import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transaction-fail',
  templateUrl: './transaction-fail.page.html',
  styleUrls: ['./transaction-fail.page.scss'],
})
export class TransactionFailPage implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
  }
  tryAgain() {
    this.router.navigate(['upgrade-choose']);
  }

}
