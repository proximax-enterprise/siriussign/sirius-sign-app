import { Component } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
import * as pdfjsLib from 'pdfjs-dist';
import * as pdfWorker from 'pdfjs-dist/build/pdf.worker';
import * as PDF from 'pdf-lib';

import { SignatureCanvas, NamePosition } from '../model/signature-canvas.model';
import { SiriusSignCosigningTask } from '../model/siriussign-task.model';

import { HelperService } from '../services/helper.service';
import { GlobalService } from './../services/global.service';
import { DrawSignatureService, SignatureInfo } from '../services/draw-signature.service';
import { SignatureService } from './../services/signature.service';
import { SigningWithoutMultisigService } from '../services/signing-without-multisig.service';
import { MonitorService } from '../services/monitor.service';
import { UploadStorageService } from '../services/upload-storage.service';
import { DocumentClassificationService } from '../services/document-classification.service';
import { DocumentStorageService } from '../services/document-strorage.service';
import { MultitaskService } from '../services/multitask.service';

@Component({
    selector: 'app-cosign-interactive',
    templateUrl: './cosign-interactive.page.html',
    styleUrls: ['./cosign-interactive.page.scss'],
})
export class CosignInteractivePage {
    signatureImgObj: HTMLImageElement = document.createElement('img');

    imgSrc: string = '/assets/sign-here-xl.png';
    imgObj: HTMLImageElement = document.createElement('img');

    inactiveImgSrc: string = '/assets/sign-here-inactive-xl.png';
    inactiveImgObj: HTMLImageElement = document.createElement('img');

    pdfDoc = null;
    docUint8Array: Uint8Array;
    pageNum: number = 1;
    pageRendering: boolean = false;
    pageNumPending: number = null;
    scale: number = 1;
    pdfCanvas: HTMLCanvasElement;
    pdfCtx: CanvasRenderingContext2D;

    signaturesInfo: SignatureInfo[];
    signerCanvases: SignatureCanvas[][] = [];
    selectedSignatureIndex: number = 1;
    selectedSignerIndex: number = 0;

    isInit: boolean = false;
    isCosigned: boolean = false;
    isFinishClicked: boolean = false;

    constructor(
        private navCtrl: NavController,
        private toastController: ToastController,
        private global: GlobalService,
        private drawSignature: DrawSignatureService,
        private signature: SignatureService,
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private monitor: MonitorService,
        private uploadStorage: UploadStorageService,
        private documentClassification: DocumentClassificationService,
        private multitask: MultitaskService,
        private documentStorage: DocumentStorageService
    ) {
        // The workerSrc property shall be specified.
        pdfjsLib.GlobalWorkerOptions.workerSrc = pdfWorker;
    }

    ngOnDestroy() {
    }

    async ionViewWillEnter() {
        await this.initView();
    }

    async initView() {
        document.documentElement.style.setProperty('--width-container', '595px');
        this.signaturesInfo = this.drawSignature.signers;
        this.selectedSignatureIndex = this.drawSignature.selectedSignerIndex;
        this.isInit = true;
        if (!this.signaturesInfo.map(signatureInfo => signatureInfo.publicKey).includes(this.global.loggedAccount.publicKey))
            this.isCosigned = true;

        this.pdfCanvas = <HTMLCanvasElement>document.getElementById('the-pdf');
        this.pdfCtx = this.pdfCanvas.getContext('2d');

        // Device Pixel Ratio
        const dpr = window.devicePixelRatio;

        // User signature image
        await this.signature.fetchFromStorage();
        const pngSignatureImg = await HelperService.getPngSignature(this.signature.signatureImg);
        this.signatureImgObj.src = pngSignatureImg;
        this.signatureImgObj.height = 88;
        this.signatureImgObj.width = 188;

        // Active image
        this.imgObj.src = this.imgSrc;
        this.imgObj.height = 88;
        this.imgObj.width = 188;

        // Inactive image
        this.inactiveImgObj.src = this.inactiveImgSrc;
        this.inactiveImgObj.height = 88;
        this.inactiveImgObj.width = 188;

        // Set pdf
        this.docUint8Array = HelperService.convertDataURIToBinary(this.drawSignature.pdfUri);
        this.pdfDoc = await pdfjsLib.getDocument(this.docUint8Array).promise;

        document.getElementById('page_count').innerHTML = this.pdfDoc.numPages;
        document.getElementById('prev').addEventListener('click', () => this.onPrevPage());
        document.getElementById('next').addEventListener('click', () => this.onNextPage());

        // Initial/first page rendering
        await this.renderPage(this.pageNum);

        // Generate signature canvases
        this.signaturesInfo.forEach((signatureInfo, idxSigner) => {
            this.signerCanvases[idxSigner] = [];
            signatureInfo.signaturePositions.forEach((signaturePosition, idxSignature) => {
                this.signerCanvases[idxSigner].push(new SignatureCanvas(signatureInfo.publicKey, idxSignature, signatureInfo.signaturePositions[idxSignature].name));
            });
        });

        const signatureImgPublicKeys = this.drawSignature.signatureImgs.map(imgInfo => imgInfo.publicKey);
        const createCanvases = async () => HelperService.asyncForEach(this.signerCanvases, async (signautrPosition: SignatureCanvas[], idxSigner: number) => {
            const subCreateCanvases = async () => HelperService.asyncForEach(signautrPosition, async (canvas: SignatureCanvas, idxSignature: number) => {
                canvas.canvasSize.width = this.pdfCanvas.width;
                canvas.canvasSize.height = this.pdfCanvas.height;

                // Check and get signed signature image
                const indexOfImg = signatureImgPublicKeys.indexOf(canvas.publicKey);
                const signatureImgSrc = (indexOfImg > -1) ? await HelperService.getPngSignature(this.drawSignature.signatureImgs[indexOfImg].signatureImg) :
                    this.imgSrc;
                const otherSignatureImgObj: HTMLImageElement = document.createElement('img');
                otherSignatureImgObj.src = signatureImgSrc;
                otherSignatureImgObj.height = 88;
                otherSignatureImgObj.width = 188;
                // Setup canvas
                canvas.create('the-signature-' + idxSigner + '-' + idxSignature, otherSignatureImgObj, this.scale * dpr);
                const idxOfSigner = this.drawSignature.signers.map(signer => signer.publicKey).indexOf(canvas.publicKey);
                const idxOfPosition = canvas.id;
                canvas.pageNumber = this.drawSignature.signers[idxOfSigner].signaturePositions[idxOfPosition].page;
                canvas.x = this.drawSignature.signers[idxOfSigner].signaturePositions[idxOfPosition].x;
                canvas.y = this.drawSignature.signers[idxOfSigner].signaturePositions[idxOfPosition].y;
                canvas.namePosition = this.drawSignature.signers[idxOfSigner].signaturePositions[idxOfPosition].namePosition;
                canvas.isNamePlace = canvas.namePosition != NamePosition.NONE;
                canvas.isInit = false;

                if (!(this.global.isBrowser) || (dpr != 1)) {
                    const canvasElement = document.getElementById('the-signature-' + idxSigner + '-' + idxSignature);
                    canvasElement.style.width = '100%';
                }
            });
            await subCreateCanvases();
        });
        await createCanvases();

        this.signerCanvases.forEach((signatureCanvases, idxSigner) => {
            signatureCanvases.forEach((canvas, idxSignature) => {
                // Draw signature image
                canvas.drawInactive(this.pageNum);

                // Set z-index for canvas
                const canvasElement = document.getElementById('the-signature-' + idxSigner + '-' + idxSignature);
                canvasElement.style.zIndex = (idxSigner == this.selectedSignerIndex) ? '2' : '1';
            });
        });

        this.setupOnSignHere();
        // document.getElementById('download').addEventListener('click', () => { this.run(); });

        // document.getElementById('apply').addEventListener('click', () => {
        //     const heightSign = <HTMLInputElement>document.getElementById('heightSign');
        //     const widthSign = <HTMLInputElement>document.getElementById('widthSign');
        //     this.imgObj.height = parseInt(heightSign.value);
        //     this.imgObj.width = parseInt(widthSign.value);
        //     this.signCanvases[this.selectedSignatureIndex].drawSignature();
        // });

        this.drawSignature.isInit = true;
    }

    /**
    * Get page info from document, resize canvas accordingly, and render page.
    * @param num Page number.
    */
    async renderPage(num) {
        this.pageRendering = true;
        // Using promise to fetch the page
        const page = await this.pdfDoc.getPage(num)

        // const pdfScreenRatio = window.innerWidth / page.view[2];
        // this.scale = pdfScreenRatio > 1 ? 1 : pdfScreenRatio;
        if (this.scale == 1) document.documentElement.style.setProperty('--width-container', page.view[2] + 'px');

        // Scale up for crystal clear image
        const dpr = window.devicePixelRatio;
        var viewport = page.getViewport({ scale: this.scale * dpr });
        this.pdfCanvas.height = viewport.height;
        this.pdfCanvas.width = viewport.width;

        // Scale down for right scale ratio
        const canvas = document.getElementById(this.pdfCanvas.id);
        if ((!this.global.isBrowser) || (dpr != 1))
            canvas.style.width = '100%';

        // Render PDF page into canvas context
        var renderContext = {
            canvasContext: this.pdfCtx,
            viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(() => {
            this.pageRendering = false;
            if (this.pageNumPending !== null) {
                // New page rendering is pending
                this.renderPage(this.pageNumPending);
                this.pageNumPending = null;
            }
        });


        // Update page counters
        document.getElementById('page_num').textContent = num;

        // Update signature
        this.signerCanvases.forEach((signatureCanvases, idxSigner) => {
            signatureCanvases.forEach((canvas, idxSignature) => {
                canvas.drawInactive(this.pageNum);
            });
        });

        this.setupOnSignHere();
    }

    /**
     * Run and refresh onSignHere method to selected signer canvases
     */
    setupOnSignHere() {
        if (this.signerCanvases[this.selectedSignerIndex]) {
            this.signerCanvases[this.selectedSignerIndex].forEach((canvas, idxSignature) => {
                canvas.onSignHere(this.pageNum, () => {
                    canvas.image = this.signatureImgObj;
                    if (!canvas.isInit)
                        canvas.drawInactiveWithAnimation(this.pageNum);
                    canvas.isInit = true;
                },
                    (x: number, y: number) => {
                        this.selectAnotherSignHere(x, y);
                    }
                );
            });
        }
    }

    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    queueRenderPage(num) {
        if (this.pageRendering) {
            this.pageNumPending = num;
        } else {
            this.renderPage(num);
        }
    }

    /**
     * Displays previous page.
     */
    onPrevPage() {
        if (this.pageNum <= 1) {
            return;
        }
        this.pageNum--;
        this.queueRenderPage(this.pageNum);
    }


    /**
     * Displays next page.
     */
    onNextPage() {
        console.log(this.pdfDoc)
        if (this.pageNum >= this.pdfDoc.numPages) {
            return;
        }
        this.pageNum++;
        this.queueRenderPage(this.pageNum);
    }

    /**
     * Alert by toast
     */
    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }

    /**
     * Embbed signatures to pdf
     */
    async getSignedFile() {
        // Load
        const pdfDoc = await PDF.PDFDocument.load(this.docUint8Array);
        const pages = pdfDoc.getPages();
        // const canvasPngImage = await pdfDoc.embedPng(canvasImageBuffer);
        const dpr = window.devicePixelRatio;

        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(PDF.StandardFonts.Helvetica);

        // Draw
        const embedImg = async () => HelperService.asyncForEach(this.signerCanvases, async (signaturePosition: SignatureCanvas[], idxSigner: number) => {
            const subEmbedImg = async () => HelperService.asyncForEach(signaturePosition, async (signCanvas: SignatureCanvas, idxSignature: number) => {
                const embedPage = pages[signCanvas.pageNumber - 1];
                const { width, height } = embedPage.getSize();
                embedPage.setFont(helveticaFont);
                embedPage.setFontSize(14);  //px

                if (signCanvas.isNamePlace) {
                    const namePosition = signCanvas.namePosition;
                    const nameRect = signCanvas.computeNameRect(namePosition);

                    const isNameCenter = (namePosition == NamePosition.TOP) || (namePosition == NamePosition.BOTTOM);
                    let nameX = nameRect.topLeftX;
                    const nameY = height / dpr - nameRect.bottomRightY + 16;

                    if (isNameCenter) {
                        this.pdfCtx.font = "14px Helvetica";
                        const textMetric = this.pdfCtx.measureText(signCanvas.name);
                        const centerNameX = (nameRect.topLeftX + nameRect.bottomRightX) / 2 - textMetric.width / 2;
                        nameX = centerNameX;
                    }

                    if (namePosition == NamePosition.LEFT) {
                        this.pdfCtx.font = "14px Helvetica";
                        const textMetric = this.pdfCtx.measureText(signCanvas.name);
                        const rightAlignNameX = nameRect.bottomRightX - textMetric.width - 5;
                        nameX = rightAlignNameX;
                    }

                    embedPage.drawText(signCanvas.name, {
                        x: nameX,
                        y: nameY,
                        color: PDF.rgb(0, 0, 0),
                        size: 14
                    });
                }

                const signatureImgBuffer = HelperService.convertDataURIToBinary(signCanvas.image.src);
                const signaturePngImg = await pdfDoc.embedPng(signatureImgBuffer);

                const position = signCanvas.getPosition();
                const embedImg = signaturePngImg;
                embedPage.drawImage(embedImg, {
                    x: position.x,
                    y: height - position.y - signCanvas.image.height,
                    height: signCanvas.image.height,
                    width: signCanvas.image.width
                });
            });
            await subEmbedImg();
        });
        await embedImg();

        // Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save();
        return pdfBytes;
    }

    /**
     * Check if network is available
     */
    async checkNetwork() {
        if (!this.global.isOnline) {
            this.presentToast('No Internet access!');
            return false;
        }

        await this.global.checkNode(this.global.apiNode);

        if (!this.global.isNodeWorking) {
            this.presentToast('API Node Failure..')
            return false;
        }

        return true;
    }

    /**
     * Sign on another sign here
     * @param canvas
     * @param signerIndex
     * @param signatureIndex
     */
    onAnotherSignHere(canvas: SignatureCanvas, signerIndex: number, signatureIndex: number) {
        const currentCanvas = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex];
        currentCanvas.isActive = false;
        if (!currentCanvas.isInit)
            currentCanvas.drawInactive(this.pageNum);
        currentCanvas.canvas.style.zIndex = '1';

        // canvas.isActive = true;
        canvas.image = this.signatureImgObj;
        if (!canvas.isInit)
            canvas.drawInactiveWithAnimation(this.pageNum);
        canvas.isInit = true;
        canvas.canvas.style.zIndex = '2';

        this.selectedSignerIndex = signerIndex;
        this.selectedSignatureIndex = signatureIndex;
    }

    /**
     * Select at another sign here
     * @param x
     * @param y
     */
    selectAnotherSignHere(x: number, y: number) {
        let isChecked = false;
        const idxSigner = this.selectedSignerIndex;
        this.signerCanvases[this.selectedSignerIndex].forEach((canvas, idxSignature) => {
            if (isChecked) return;
            const isInside = canvas.checkSelected(x, y, this.pageNum);
            if (isInside) {
                this.onAnotherSignHere(canvas, idxSigner, idxSignature);
                isChecked = true;
            }
        });
    }

    /**
     * Put signature done, launch cosign process
     */
    async onFinish() {
        if (this.isFinishClicked) return;
        this.isFinishClicked = true;
        const isAccessible = await this.checkNetwork();
        if (!isAccessible) {
            this.isFinishClicked = false;
            return;
        }

        this.isCosigned = true;
        if (this.signerCanvases[this.selectedSignerIndex]) {
            this.signerCanvases[this.selectedSignerIndex].forEach(canvas => {
                if (!canvas.isInit) this.isCosigned = false;
            });
        }

        if (this.isCosigned) {
            this.global.setIsProgressDone(false);
            await this.prepareTask();
            setTimeout(() => {
                this.goTasks();
            }, 10);
        }
        else {
            this.isFinishClicked = false;
            this.presentToast('Please put your signature');
        }
    }

    /**
     * Create cosigning taks for multitasking
     */
    async prepareTask() {
        const cFileUint8Array = await this.getSignedFile();
        const oFileUint8Array = this.docUint8Array;
        const task = new SiriusSignCosigningTask(
            this.signingWithoutMultisig,
            this.documentClassification,
            this.monitor,
            this.uploadStorage,
            this.global.loggedAccount,
            this.drawSignature.cosigner,
            this.signature.signatureImg,
            this.documentClassification.selectedDocInfo,
            cFileUint8Array,
            oFileUint8Array
        );
        const selectedDocId = task.selectedDocInfo.id;
        const selectedDocAccPublicKey = task.selectedDocInfo.documentAccount.publicKey;
        this.documentClassification.needSign = this.documentClassification.needSign.filter(doc => doc.documentAccount.publicKey != selectedDocAccPublicKey);
        task.incCosignProgress(task => this.finishCosigning(task));
        this.multitask.tasks.push(task);
    }

    /**
     * Fetch and store document after cosigning done
     * @param task
     */
    async finishCosigning(task: SiriusSignCosigningTask) {
        const isSuccess = (task.transactionStatus.status == 'confirmed');
        const oldDocId = task.selectedDocInfo.id;
        const oldDocPublicKey = task.selectedDocInfo.documentAccount.publicKey;
        if (isSuccess) {
            const newDoc = await this.documentClassification.fetchDocumentBySigningTxHash(task.selectedDocInfo.signTxHash);
            this.documentStorage.updateDocumentByPublicKey(oldDocPublicKey, oldDoc => newDoc);
            this.documentClassification.classify(newDoc);
            this.documentClassification.sortDocs('compledAndWaiting');
        }
        else {
            const docs = await this.documentStorage.fetchDocument();
            docs.forEach(doc => this.documentClassification.classify(doc));
            this.documentClassification.sortDocs('all');
        }
    }

    /**
     * Navigate to tasks tab
     */
    goTasks() {
        this.global.setIsProgressDone(true);
        this.multitask.selectedTask = null;
        this.navCtrl.navigateRoot('/app/tabs/tab-tasks');
    }
}
