import { SiriusWallet } from './../model/sirius-wallet.model';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';

import {
    Account,
    Address,
    NetworkType,
    BlockHttp,
    MosaicId,
    MosaicInfo,
    MosaicHttp,
    NamespaceId,
    NamespaceHttp,
    NodeHttp
} from 'tsjs-xpx-chain-sdk';
import { BlockchainNetworkType } from 'tsjs-chain-xipfs-sdk';
import { MD5, AES } from 'crypto-js';
import { BehaviorSubject } from 'rxjs';

import { config } from '../config';

enum NodeType {
    API = 'api',
    IPFS = 'ipfs'
}


@Injectable({
    providedIn: 'root'
})

export class GlobalService {
    /** App indentifying code name */
    readonly appCodeName = 'SiriusSign';
    readonly appVersion = config.VERSION;   // '1.01';
    readonly build = 'c96a75f.1';

    /** Network Type */
    readonly networkType: NetworkType = config.NETWORK_TYPE; //  NetworkType.PRIVATE_TEST;
    readonly blockchainNetworkType: BlockchainNetworkType = config.NETWORK_TYPE; // BlockchainNetworkType.PRIVATE_TEST;

    /** Default Node server */
    readonly API_DOMAIN_1 = 'demo-sc-api-1.ssi.xpxsirius.io';
    readonly API_NODE_1 = config.API_NODE_1;  // 'https://demo-sc-api-1.ssi.xpxsirius.io';
    readonly WS_1 = 'wss://demo-sc-api-1.ssi.xpxsirius.io';
    readonly IPFS_DOMAIN_1 = 'ipfs1-dev.xpxsirius.io';
    readonly IPFS_NODE_1 = config.IPFS_NODE_1;  // 'https://ipfs1-dev.xpxsirius.io:5443';

    readonly API_DOMAIN_2 = 'bctestnet5.xpxsirius.io';
    readonly API_NODE_2 = config.API_NODE_2;   // https://bctestnet5.xpxsirius.io';
    readonly WS_2 = 'wss://bctestnet5.xpxsirius.io';
    readonly IPFS_DOMAIN_2 = 'ipfs2-dev.xpxsirius.io';
    readonly IPFS_NODE_2 = config.IPFS_NODE_2;  // 'https://ipfs2-dev.xpxsirius.io:5443';

    /** Using Node */
    apiNode = config.API_NODE_1;                                        // 'https://demo-sc-api-1.ssi.xpxsirius.io';
    apiProtocol = this.apiNode.split('://')[0];                         // 'https';
    apiDomain = this.apiNode.split('//')[1].split(':')[0];              // 'demo-sc-api-1.ssi.xpxsirius.io';
    apiPort: number | undefined = (this.apiNode.split('//')[1].split(':')[1]) ? parseInt(this.apiNode.split('//')[1].split(':')[1], 10) : undefined; // undifined
    ws = this.apiNode.replace('https', 'wss').replace('http', 'ws');    // 'wss://demo-sc-api-1.ssi.xpxsirius.io';

    ipfsNode = config.IPFS_NODE_1;                                      // 'https://ipfs1-dev.xpxsirius.io:5443';
    ipfsProtocol = this.ipfsNode.split('://')[0];                       // 'https';
    ipfsDomain = this.ipfsNode.split('//')[1].split(':')[0];            // 'ipfs1-dev.xpxsirius.io';
    ipfsPort: number | undefined = (this.ipfsNode.split('//')[1].split(':')[1]) ? parseInt(this.ipfsNode.split('//')[1].split(':')[1], 10) : undefined; // 5443;

    /** The first block hash from this.global.node/block/1 meta.generationHash value */
    networkGenerationHash = '3D9507C8038633C0EB2658704A5E7BC983E4327A99AC14D032D67F5AACBCCF6A';

    /** Mosaic ID of the network */
    // readonly mosaicId = new MosaicId([697101613, 1007631845]); //'0DC67FBE1CAD29E3'; // XPX
    readonly namespaceId = new NamespaceId('prx.xpx');
    mosaicId: MosaicId;
    mosaicInfo: MosaicInfo;
    mosaicName = this.namespaceId.fullName;


    /** Unique device ID */
    uDid: string;

    /** Payment Feature */
    isPayment: boolean = true;

    /** Browser of Mobile app */
    isBrowser: boolean;

    /** Internet status */
    isOnline: boolean = true;
    observableIsOnline: BehaviorSubject<boolean>;

    /** Node status */
    isNodeWorking: boolean = true;

    /** Sign up state */
    isSignedUp: boolean;

    /** Log-in state of app*/
    isLoggedIn: boolean = false;

    /** Sign/Verify progress state. Use to prevent route back to progress page */
    isProgressDone: boolean = true;

    /** Sign/Verify result finish state. Use to prevent route back to result page */
    isFinished: boolean = true;

    /** Logged Wallet */
    loggedWallet: SiriusWallet;

    /** Account following Logged Wallet */
    loggedAccount: Account;

    /** Logged account fingerprint */
    isAllowFingerprint: boolean;

    /** Notarization Account Address */
    // notarizationAddress: Address;

    /** Notarization Account Account */
    // notarizationAccount: Account;

    isMenuNeedOpen: boolean = false;

    constructor(private storage: Storage, private uniqueDeviceID: UniqueDeviceID) {
        if (this.appCodeName == 'SiriusSign') this.isPayment = false;
        this.observableIsOnline = new BehaviorSubject<boolean>(this.isOnline);
        this.initGlobal();
    }

    /**
     * Store default node
     * Fetch selected node
     * Fetch Network Generation Hash
     */
    async initGlobal() {
        await this.addNodeList('api', this.API_NODE_1);
        // this.addNodeList('api', this.API_NODE_2);

        await this.addNodeList('ipfs', this.IPFS_NODE_1);
        this.addNodeList('ipfs', this.IPFS_NODE_2);

        await this.fetchSelectedNode('api');

        this.uDid = await this.uniqueDeviceID.get()
            .catch((error: any) => { console.log(error); return null; });

        this.fetchGenerationHash();

        this.fetchMosaicInfo();
    }

    /**
     * Set isSignedUp
     * @param state 
     */
    setIsSignedUp(state: boolean) {
        this.isSignedUp = state;
        this.storage.set('isSignedUp', this.isSignedUp);
    }

    /**
     * Querry sign up state from local storage.
     * @async
     */
    async fetchIsSignUp() {
        await this.storage.get('isSignedUp')
            .then((value: boolean) => {
                this.isSignedUp = value;
            })
            .catch(e => {
                this.isSignedUp = false;
                this.setIsSignedUp(this.isSignedUp);
            })
        return this.isSignedUp;
    }

    /**
     * Store log in state to local storage.
     * Be carefull when use this function, should use only when sign in/out
     * @param {boolean} state the log in state
     */
    setIsLoggedIn(state: boolean) {
        this.isLoggedIn = state;

        //Use for keeping logged state
        //this.storage.set('isLoggedIn', this.isLoggedIn);
    }

    /**
     * Querry log in state from local storage.
     * @async
     */
    async fetchIsLoggedIn() {
        await this.storage.get('isLoggedIn')
            .then((value: boolean) => {
                this.isLoggedIn = value;
            })
            .catch(e => {
                this.isLoggedIn = false;
                this.setIsLoggedIn(this.isLoggedIn);
            })
        return this.isLoggedIn;
    }

    /**
     * Get log in state without fetch from storage.
     * Let use this function carefully, should use only in case of stable log in state.
     * If you want to get the 'real' log in state, use fetchIsLoggedIn() instead.
     * @return {boolean}
     */
    getIsLoggedIn() {
        return this.isLoggedIn;
    }

    /**
     * Set value of isProgressDone
     * @param {boolean} state
     */
    setIsProgressDone(state: boolean) {
        this.isProgressDone = state;
    }

    /**
     * Get value of isProgressDone
     * @return
     */
    getIsProgressDone() {
        return this.isProgressDone;
    }

    /**
     * Set value of isFinished
     * @param {boolean} state
     */
    setIsFinished(state: boolean) {
        this.isFinished = state;
    }

    /**
     * Store logged in wallet to storage
     * @param wallet
     */
    setLoggedWallet(wallet: SiriusWallet) {
        this.loggedWallet = wallet;
        // this.storage.set('loggedWlt', this.loggedWallet);
    }

    /**
    * Store logged in account
    * @param account
    */
    setLoggedAccount(account: Account) {
        this.loggedAccount = account;
        //this.storage.set('loggedAcc', this.loggedAccount);
    }

    /**
     * Get Public Key of Logged Account
     */
    getLoggedAccountPublicKey() {
        return this.loggedAccount.publicKey;
    }


    /**
     * Fetch stored API Node List
     */
    fetchNodeList(type) {
        const key = type + 'NodeList';
        return this.storage.get(key);
    }

    /**
     * Add a node to node list
     * @param node 
     */
    async addNodeList(type, node) {
        const key = type + 'NodeList';
        return new Promise(resolve => {
            this.fetchNodeList(type)
                .then(async (nodeList) => {
                    const isExist = nodeList.includes(node);
                    if (!isExist) {
                        nodeList.push(node);
                        await this.storage.set(key, nodeList);
                        console.log('Added node:' + node);
                        resolve();
                    }
                    else {
                        // throw new Error('Node is already exist');
                        console.log('Node already exist');
                        resolve()
                    }
                })
                .catch(async (err) => {
                    await this.storage.set(key, [node]);
                    resolve();
                });
        });
    }

    /**
     * Store selected node
     * @param type 
     * @param node 
     */
    setSelectedNode(type, node) {
        const key = type + 'Node';
        this.setNode(type, node);
        this.storage.set(key, node);
    }

    /**
     * Set using node
     * @param type 
     * @param node 
     */
    setNode(type, node) {
        if (type == 'api') {
            this.apiNode = node;
            this.apiDomain = this.apiNode.split('//')[1].split(':')[0];
            const apiPort = this.apiNode.split('//')[1].split(':')[1];
            this.apiPort = (apiPort) ? parseInt(apiPort, 10) : undefined;
            this.ws = this.apiNode.replace('https', 'wss').replace('http', 'ws');

            console.log('node: ' + this.apiNode);
            console.log('domain: ' + this.apiDomain);
            console.log('ws: ' + this.ws);
        }
        if (type == 'ipfs') {
            this.ipfsNode = node;
            this.ipfsDomain = this.ipfsNode.split('//')[1].split(':')[0];
            const ipfsPort = this.ipfsNode.split('//')[1].split(':')[1];
            this.ipfsPort = (ipfsPort) ? parseInt(ipfsPort, 10) : undefined;
        }
    }

    /**
     * Fetch selected node from storage
     * @param type
     */
    async fetchSelectedNode(type) {
        const key = type + 'Node';
        const node = await this.storage.get(key);
        if (node != null) {
            this.setNode(type, node);
        }
    }

    /**
     * Check if a node is valid
     * @param node 
     */
    checkNode(node) {
        const nodeHttp = new NodeHttp(node);
        return new Promise<boolean>(resolve => {
            setTimeout(() => {
                this.isNodeWorking = false;
                resolve(false);
            }, 3000);

            nodeHttp.getNodeInfo().subscribe(nodeInfo => {
                this.isNodeWorking = true;
                resolve(true);
            });
        });
    }

    /**
     * Fetch the Network Generation Hash
     */
    fetchGenerationHash() {
        const blockHttp = new BlockHttp(this.apiNode);
        blockHttp.getBlockByHeight(1)
            .subscribe(blockInfo => {
                this.networkGenerationHash = blockInfo.generationHash;
            });
    }

    /**
     * Fetch mosaic info
     */
    fetchMosaicInfo() {
        const namespaceHttp = new NamespaceHttp(this.apiNode);
        const mosaicHttp = new MosaicHttp(this.apiNode);
        namespaceHttp.getLinkedMosaicId(this.namespaceId)
            .subscribe(mosaicId => {
                this.mosaicId = mosaicId;
                mosaicHttp.getMosaic(this.mosaicId)
                    .subscribe(mosaicInfo => {
                        this.mosaicInfo = mosaicInfo;
                    }, err => { console.log(err); });
            }, err => console.log(err));
    }

    /**
     * Create a key for internal using
     * @param account
     * @returns
     */
    private createMasterKey(account: Account) {
        let key = MD5(account.privateKey).toString();
        this.storage.set('master', key);
        return key;
    }

    /**
     * Encrypt data for internal using
     * @param data 
     * @param key 
     */
    private createSlaveKey(data: string, key: string) {
        let encrypted = AES.encrypt(data, key).toString();
        this.storage.set('slave', encrypted);
    }

    deriveSlaveKey(slaveKey: string, key: string) {
        let plain = AES.decrypt(slaveKey, key).toString();
        return plain;
    }

    /**
     * Sign out the current wallet
     */
    signOut() {
        this.setLoggedWallet(null);
        this.setIsLoggedIn(false);
    }
}
