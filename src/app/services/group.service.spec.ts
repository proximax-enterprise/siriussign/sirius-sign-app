import { TestBed, async, inject } from '@angular/core/testing';

import { GroupService, SiriusGroups, GroupElement } from './group.service';
import { IonicStorageModule } from '@ionic/storage';
import { GlobalService } from './global.service';
import { SiriusWallet } from '../model/sirius-wallet.model';
import { Address, Account, NetworkType } from 'tsjs-xpx-chain-sdk';
import { Device } from '@ionic-native/device/ngx';


describe('GroupService', () => {
  let service: GroupService;
  let global: GlobalService ;
  let account: Account;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicStorageModule.forRoot()
      ],
      providers: [
          Storage,
          GlobalService,
          Device
      ]
    });
    service = TestBed.get(GroupService);
    global = TestBed.get(GlobalService);
    account = Account.createFromPrivateKey('EC994CC2F78603D554809FC06CC587357CDE40E6577B4B069E881ECA6D901E04', NetworkType.TEST_NET);
    global.setLoggedAccount(account);
  });

  it('should be created', () => {
    const service: GroupService = TestBed.get(GroupService);
    expect(service).toBeTruthy();
  });


  
  it('add and fetch a new group', async () =>{
    

    const group:GroupElement = {
      name: 'Test Group',
      multisigAcc: account.publicAccount,
      creationTxHash: '',
      creationDate: 0,
      creator: 'Test creator',
      members: []
    }

    global.setLoggedWallet(new SiriusWallet(
      group.creator,
      Address.createFromRawAddress('SB3KUBHATFCPV7UZQLWAQ2EUR6SIHBSBEOEDDDF3'),
      '',
      '',
      '',
      '',
      0
    ));
    
    const store = await service.storeGroup(group)

    const list:Array<any> = await service.fetchGroups()

    // expect(list).toBeDefined()
      
    // expect(list.length).not.toEqual(0)


  });





  it('add, fetch and remove a new group', async ()=>{

    const group:GroupElement = {
      name: 'Test Group',
      multisigAcc: account.publicAccount,
      creationTxHash: '',
      creationDate: 0,
      creator: 'Test creator',
      members: []
    }

    global.setLoggedWallet(new SiriusWallet(
      group.creator,
      Address.createFromRawAddress('SB3KUBHATFCPV7UZQLWAQ2EUR6SIHBSBEOEDDDF3'),
      '',
      '',
      '',
      '',
      0
    ));
    
    // sync call to store new group
    const store = await service.storeGroup(group);

    // expect new group has been stored in storage
    let list:Array<any> = await service.fetchGroups();
    expect(list).toBeDefined()
    expect(list.length).not.toEqual(0)
    // remove that group from storage
    await service.removeGroup(group.name)
    list = await service.fetchGroups()
    // expect that group successfully remove from storage
    expect(list.length).toEqual(0)
    

  });
  


});