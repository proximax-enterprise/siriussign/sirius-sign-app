import { Component, OnInit } from '@angular/core';
import { BuyCryptoService } from './../services/buy-crypto.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-transaction-success',
  templateUrl: './transaction-success.page.html',
  styleUrls: ['./transaction-success.page.scss'],
})
export class TransactionSuccessPage implements OnInit {
  data: any = {};
  name: string = '';
  email: string = '';
  totalAmount: any = '';
  currency: string = '';

  constructor(
    private buyCrypto: BuyCryptoService,
    private router: Router,
    private navCtrl: NavController
  ) { }

  ionViewWillEnter() {
    this.data = this.buyCrypto.cardInfo;
    this.totalAmount = this.buyCrypto.usdAmount / 100;
    this.currency = this.buyCrypto.currency;
  }
  ngOnInit() {

  }
  done() {
    // this.router.navigate(['/app/tabs/tab-menu/home']);
    this.navCtrl.navigateRoot('/app/tabs/tab-menu/home');
  }
}
