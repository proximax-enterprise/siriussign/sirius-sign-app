import { Component } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { HelperService } from './../services/helper.service';
import { GlobalService } from './../services/global.service';
import { SignDocumentService } from '../services/sign-document.service';
import { GroupService } from '../services/group.service';
import { DrawSignatureService } from './../services/draw-signature.service';
import { SignatureService } from './../services/signature.service';

interface ToSignFile {
    name: string,
    size: string,
    uploadDate: string,
    owner: {
        publicKey: string,
        name: string,
        isRequireSignatureImg: boolean
    },
    cosigners: {
        publicKey: string,
        name: string,
        isRequireSignatureImg: boolean
    }[],
    verifiers: string[],
    hashFunc: 'SHA256',
    fileHash: string,
    storage: 'Public' | 'Private',
    fee: 'No fee' | number
}

@Component({
    selector: 'app-new-multi-sign',
    templateUrl: './new-multi-sign.page.html',
    styleUrls: ['./new-multi-sign.page.scss'],
})
export class NewMultiSignPage {

    file: ToSignFile = {
        name: '',
        size: 'Unknown',
        uploadDate: '',
        owner: {
            publicKey: '',
            name: '',
            isRequireSignatureImg: false
        },
        cosigners: [
            {
                publicKey: '',
                name: '',
                isRequireSignatureImg: false
            },
            {
                publicKey: '',
                name: '',
                isRequireSignatureImg: false
            }
        ],
        verifiers: [],
        hashFunc: 'SHA256',
        fileHash: '',
        storage: 'Public',
        fee: 'No fee'
    };

    labels = {
        name: 'File name',
        size: 'Size',
        uploadDate: 'Upload date',
        owner: 'Your information',
        cosigners: 'Cosigners',
        verifiers: 'Verifiers',
        hashFunc: 'Hash function',
        fileHash: 'File Hash (SHA256)',
        storage: 'Storage',
        fee: 'Network fee'
    };

    fields = ['name', 'size', 'uploadDate', 'owner', 'cosigners', 'verifiers', 'fileHash', 'storage', 'fee'];

    isProcessing: boolean = false;

    constructor(
        private toastController: ToastController,
        private navCtrl: NavController,
        private barcodeScanner: BarcodeScanner,
        private global: GlobalService,
        private signDoc: SignDocumentService,
        private group: GroupService,
        private drawSignature: DrawSignatureService,
        private signature: SignatureService
    ) { }

    ionViewWillEnter() {
        this.isProcessing = false;
        const doc = this.signDoc.getDocmument();
        this.file.name = doc.file.name;
        this.file.uploadDate = HelperService.dateToShortString(doc.signDate);
        this.file.fileHash = doc.fileHash;
        this.file.size = HelperService.sizeToString(this.signDoc.document.size);
        this.file.owner.publicKey = this.global.loggedAccount.publicKey;
        this.file.owner.name = this.signature.name;
        // if (this.group.selectedGroup) this.file.cosignAcc = this.group.selectedGroup.name;
        // else this.file.cosignAcc = '';
        this.drawSignature.reset();

        // Reset signing doc in case of back from interactive
        this.signDoc.document.cosigners = [];
    }

    /**
     * Naviagte to new-only-me
     */
    goOnlyMe() {
        this.navCtrl.pop();
        // this.router.navigate(['new-only-me']);
    }

    /**
     * Navigate to progress page
     */
    goProgress() {
        this.global.setIsProgressDone(false);
        this.isProcessing = false;
        this.navCtrl.navigateRoot('new-progress');
    }

    /**
     * Navigate to progress page
     */
    goInteractive() {
        this.isProcessing = false;
        this.navCtrl.navigateRoot('new-interactive');
    }

    toggleIsRequireSignatureImg(index: number) {
        if (index == -1) {
            this.file.owner.isRequireSignatureImg = !this.file.owner.isRequireSignatureImg;
        }
        else {
            this.file.cosigners[index].isRequireSignatureImg = !this.file.cosigners[index].isRequireSignatureImg;
        }
    }

    /*
     * Add one more multisig account to list
     */
    onAdd() {
        let length = this.file.cosigners.length;
        if (length < 15) this.file.cosigners.push({ publicKey: '', name: '', isRequireSignatureImg: false });
    }

    /*
     * Remove a multisig account from list
     * @param index - index of the address need be removed
     */
    onRemove(index: number, flag: 'cosigners' | 'verifiers') {
        if (flag == 'cosigners') this.file.cosigners.splice(index, 1);
        if (flag == 'verifiers') this.file.verifiers.splice(index, 1);
    }

    /**
     * Scan QR code to get cosigner information
     * @param index 
     */
    onScan(index: number, flag: 'cosigners' | 'verifiers') {
        this.barcodeScanner.scan().then(barcodeData => {
            const info = JSON.parse(barcodeData.text)
            if (flag == 'cosigners') {
                this.file.cosigners[index].name = info.name;
                this.file.cosigners[index].publicKey = info.publicKey;
            }
            if (flag == 'verifiers')
                this.file.verifiers[index] = info.publicKey;
        }).catch(err => {
            console.log('Error', err);
        });
    }

    /**
     * Check if cosigner public key is valid
     * @param index 
     */
    checkCosignerKey(index: number) {
        this.file.cosigners[index].publicKey = this.file.cosigners[index].publicKey.toUpperCase();
        const hexPattern = RegExp('^[a-fA-F0-9]+$');
        if (this.file.cosigners[index].publicKey.includes(' ')) return false;
        const isEmpty = this.file.cosigners[index].publicKey == '';
        const isHexString = hexPattern.test(this.file.cosigners[index].publicKey);
        const isRightLength = this.file.cosigners[index].publicKey.length == 64;
        return (isEmpty || (isRightLength && isHexString));
    }

    /**
     * Check if verifier public key is valid
     * @param index 
     */
    checkVerifierKey(index: number, value?) {
        if (value)
            this.file.verifiers[index] = value.toUpperCase(); //this.file.verifiers[index].toUpperCase();
        else
            this.file.verifiers[index] = this.file.verifiers[index].toUpperCase();
        const hexPattern = RegExp('^[a-fA-F0-9]+$');
        if (this.file.verifiers[index].includes(' ')) return false;
        const isEmpty = this.file.verifiers[index] == '';
        const isHexString = hexPattern.test(this.file.verifiers[index]);
        const isRightLength = this.file.verifiers[index].length == 64;
        return (isEmpty || (isRightLength && isHexString));
    }


    /**
     * This function help the template use ngFor, input and Array together.
     * Because of input-array binding, the view (ngFor) is reloaded after type
     * a character. This ngFor must track content by index instead of array content.
     * @param index
     * @param obj
     * @return index
     */
    trackByIndex(index: number, obj: any): any {
        return index;
    }

    /**
     * Alert by toast
     */
    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }

    /**
     * Launch sign document with others process
     */
    onSignDocument() {
        this.isProcessing = true;

        if (!this.global.isOnline) {
            this.presentToast('No internet access!');
            this.isProcessing = false;
            return;
        }

        if (this.file.owner.isRequireSignatureImg && this.file.owner.name == '') {
            this.presentToast('Please input your name');
            this.isProcessing = false;
            return;
        }

        this.file.cosigners = this.file.cosigners.filter(value => value.publicKey != '');
        if (this.file.cosigners.length == 0) {
            this.file.cosigners.push({ publicKey: '', name: '', isRequireSignatureImg: false });
            this.presentToast('Please input cosigner\'s information');
            this.isProcessing = false;
            return;
        }

        for (let i = 0; i < this.file.cosigners.length; i++) {
            if (!this.checkCosignerKey(i)) {
                this.presentToast('Invalid cosigner\'s public keys');
                this.isProcessing = false;
                return;
            }
            if (this.file.cosigners[i].publicKey == this.file.owner.publicKey) {
                this.presentToast('Owner cannot be set as a cosigner');
                this.isProcessing = false;
                return;
            }
            if (this.file.cosigners[i].isRequireSignatureImg && this.file.cosigners[i].name == '') {
                this.presentToast('Please input cosigner\'s names');
                this.isProcessing = false;
                return;
            }
        }

        this.file.verifiers = this.file.verifiers.filter(value => value != '');
        for (let i = 0; i < this.file.verifiers.length; i++) {
            if (!this.checkVerifierKey(i)) {
                this.presentToast('Invalid verifier\'s public key');
                this.isProcessing = false;
                return;
            }
        }

        // Check if verifiers are signers
        const signers = [this.file.owner.publicKey, ...this.file.cosigners.map(signer => signer.publicKey)];
        const intersec = HelperService.intersecArray(this.file.verifiers, signers);
        if (intersec.length > 0) {
            this.presentToast('Signer cannot be a verifier');
            this.isProcessing = false;
            return;
        }

        // Set signers to draw signature
        this.drawSignature.pdfUri = this.signDoc.document.file.dataURI;
        if (this.file.owner.isRequireSignatureImg)
            this.drawSignature.setSigner(this.file.owner.publicKey, this.file.owner.name);
        this.file.cosigners.forEach(cosigner => {
            if (cosigner.isRequireSignatureImg)
                this.drawSignature.setSigner(cosigner.publicKey, cosigner.name);
        });


        // Set cosigners and verifiers to document
        this.signDoc.document.cosigners = this.file.cosigners.map(cosigner => cosigner.publicKey);
        this.signDoc.document.verifiers = this.file.verifiers;

        console.log(this.signDoc.document.cosigners);
        console.log(this.signDoc.document.verifiers);
        console.log(this.drawSignature.signers);

        this.group.selectedGroup = null;
        this.isProcessing = false;
        this.goInteractive();
    }
}
