import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedSignPage } from './need-sign.page';

xdescribe('NeedSignPage', () => {
    let component: NeedSignPage;
    let fixture: ComponentFixture<NeedSignPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NeedSignPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NeedSignPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
