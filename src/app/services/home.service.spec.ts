import { TestBed } from '@angular/core/testing';

import { HomeService } from './home.service';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device/ngx';
import { GlobalService } from './global.service';
import { SigningService } from './signing.service';
import { Account, NetworkType } from 'tsjs-xpx-chain-sdk';

describe('HomeService', () => {
  let service:HomeService;
  let global:GlobalService;
  let signing:SigningService;
  let account:Account;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicStorageModule.forRoot()
      ],
      providers: [
          Storage,
          Device,
          GlobalService,
          SigningService
      ]
    })

    // setup account
    service = TestBed.get(HomeService);
    global = TestBed.get(GlobalService);
    signing = TestBed.get(SigningService);
    account = Account.createFromPrivateKey('EC994CC2F78603D554809FC06CC587357CDE40E6577B4B069E881ECA6D901E04', NetworkType.TEST_NET);
    global.setLoggedAccount(account);
    
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('Fetch aggregate bonded transaction', async ()=>{
    const callFetch = await  service.fetchSigningDoc();
    // Check fetch function was run
    expect(service.docs).toBeDefined();

    /**
     * To run this below EXPECT
     * Check if list is not empty (need setup an account which at least one Signing Document)
     *  */ 
    // expect(service.docs.length).toBeGreaterThan(0)
  })


});
