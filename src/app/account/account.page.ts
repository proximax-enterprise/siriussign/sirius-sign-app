import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, AlertController, ToastController, ActionSheetController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

import { Clipboard } from '@ionic-native/clipboard/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

import * as ClipboardJS from 'clipboard/dist/clipboard.min.js';

import { GlobalService } from './../services/global.service';
import { WalletService } from './../services/wallet.service';
import { MonitorService } from './../services/monitor.service';
import { SignatureService } from './../services/signature.service';
import { HelperService } from './../services/helper.service';

@Component({
    selector: 'app-account',
    templateUrl: './account.page.html',
    styleUrls: ['./account.page.scss'],
})

export class AccountPage {

    accInfo = {
        name: '',
        email: '',
        address: '',
        publicKey: '',
        privateKey: '',
        signatureImg: '',
        plan: '',
        balance: '',
        creationDate: '',
        networkType: '',
    }

    networkInfo = {
        apiNode: '',
        isApiOnl: false,
        ipfsNode: '',
        isIpfsOnl: false
    }

    currentNode = {
        apiNode: '',
        ipfsNode: ''
    }

    nodeList = {
        apiNode: [],
        ipfsNode: []
    }

    readonly fields = ['name', 'email', 'address', 'publicKey', 'privateKey', 'signatureImg', 'plan', 'balance', 'networkType'];
    readonly labels = {
        name: 'Name',
        email: 'Email',
        address: 'Address',
        publicKey: 'Public Key',
        privateKey: 'Private Key',
        signatureImg: 'Signature',
        creationDate: 'Creation Date',
        plan: 'Plan',
        balance: 'Balance',
        networkType: 'Network Type',
        apiNode: 'API Node',
        ipfsNode: 'Storage Node'
    };

    apiActionSheetOptions: any = {
        header: 'Select API Node'
    };

    ipfsActionSheetOptions: any = {
        header: 'Select Storage Node'
    };

    isAllowFingerprint = false;

    constructor(
        private router: Router,
        private menuCtrl: MenuController,
        private alertCtrl: AlertController,
        private toastController: ToastController,
        private actionSheetCtrl: ActionSheetController,
        private domSanitizer: DomSanitizer,
        private clipboard: Clipboard,
        private faio: FingerprintAIO,
        private global: GlobalService,
        private wallet: WalletService,
        private monitor: MonitorService,
        private signature: SignatureService
    ) {
        var clipboardjs = new ClipboardJS('.btnCopy');
        clipboardjs.on('success', (event: ClipboardJS.Event) => {
            setTimeout(() => { event.clearSelection() }, 200);
        });
        this.isAllowFingerprint = this.global.isAllowFingerprint;
    }

    bypassSecurityTrustUrl = this.domSanitizer.bypassSecurityTrustUrl;

    ionViewWillEnter() {
        this.setAccInfo();
    }

    /**
     * Set accInfo
     */
    async setAccInfo() {
        const networkTypeName = {
            '184': 'MAIN_NET',
            '168': 'TEST_NET',
            '200': 'PRIVATE',
            '176': 'PRIVATE_TEST',
            '96': 'MIJIN',
            '144': 'MIJIN_TEST'
        }
        let wallet = this.global.loggedWallet;
        this.isAllowFingerprint = this.global.isAllowFingerprint;
        this.accInfo.email = wallet.name;
        this.accInfo.address = wallet.address.pretty();
        // this.accInfo.creationDate = wallet.creationDate.toString();
        this.accInfo.networkType = networkTypeName[wallet.address.networkType.toString()];
        this.accInfo.publicKey = this.global.getLoggedAccountPublicKey();
        this.accInfo.privateKey = '';
        this.accInfo.plan =
            (this.global.loggedWallet.plan == 0) ? 'Not register yet' :
                (this.global.loggedWallet.plan == 1) ? 'Free' :
                    (this.global.loggedWallet.plan == 2) ? 'Premium' : 'No license';
        if (!this.global.isPayment) this.accInfo.plan = 'Free';
        this.networkInfo.apiNode = this.global.apiNode;
        this.networkInfo.ipfsNode = this.global.ipfsNode;
        this.currentNode.apiNode = this.networkInfo.apiNode;
        this.currentNode.ipfsNode = this.networkInfo.ipfsNode;
        this.accInfo.signatureImg = this.signature.pngSignatureImg;
        this.accInfo.name = this.signature.name;
        const signautureImgSub = this.signature.observableSignatureImg.subscribe(async signatureImg => {
            this.accInfo.signatureImg = await HelperService.getPngSignature(signatureImg);
        });
        this.accInfo.balance = 'Fetching..';
        this.nodeList.apiNode = await this.global.fetchNodeList('api');
        this.nodeList.ipfsNode = await this.global.fetchNodeList('ipfs');
        this.accInfo.balance = await this.wallet.fetchBalance();
        console.log(this.nodeList);
        console.log(this.networkInfo);
        console.log(this.currentNode);
        console.log('Address: ' + this.accInfo.address);
        console.log('Public: ' + this.accInfo.publicKey);

        this.networkInfo.isApiOnl = false;
        this.networkInfo.isApiOnl = await this.global.checkNode(this.networkInfo.apiNode);

        if (this.global.loggedWallet.plan == 0) {
            const planPolling = setInterval(() => {
                if (this.global.loggedWallet.plan > 0) {
                    this.accInfo.plan =
                        (this.global.loggedWallet.plan == 1) ? 'Free' :
                            (this.global.loggedWallet.plan == 2) ? 'Premium' : 'No license';
                    clearInterval(planPolling);
                }
                else {
                    this.accInfo.plan = 'No license';
                }
            }, 500);
        }
    }

    /**
     * Launch enter password alert
     */
    async onShowPrivateKey() {
        const alert = await this.alertCtrl.create({
            header: 'Please enter your password',
            inputs: [
                {
                    name: 'pass',
                    type: 'password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        this.verifyPass(data.pass);
                    }
                }
            ]
        });

        await alert.present();
    }

    async onEnterYourName() {
        const alert = await this.alertCtrl.create({
            header: 'Please enter your name',
            inputs: [
                {
                    name: 'name',
                    type: 'text'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        this.updateName(data.name);
                    }
                }
            ]
        });

        await alert.present();
    }

    /**
     * Launch verify password
     */
    async alertWrongPass() {
        const alert = await this.alertCtrl.create({
            header: 'Your password is invalid.',
            buttons: [
                {
                    text: 'Ok'
                }
            ]
        });
        await alert.present();
    }


    /**
     * Get private key
     */
    verifyPass(password: string) {
        const isPassOk = this.wallet.checkWalletPassword(this.global.loggedWallet, password);
        if (isPassOk) this.showPrivateKey();
        else this.alertWrongPass();
    }

    /**
     * Show Private Key
     */
    showPrivateKey() {
        this.accInfo.privateKey = this.global.loggedAccount.privateKey;
        console.log(this.accInfo.privateKey);
    }

    /**
     * Update name on signature
     * @param name 
     */
    async updateName(name: string) {
        await this.signature.override(
            this.global.loggedWallet.name,
            name,
            this.signature.signatureImg,
            this.signature.signatureImgTxHash
        );
        await this.signature.fetchFromStorage();
        this.accInfo.name = this.signature.name;
    }

    /**
     * Display toast that inform copied message
     */
    async presentToastCopied() {
        const toast = await this.toastController.create({
            message: 'Copied!',
            duration: 1000,
            translucent: true
        });
        toast.present();
    }

    /**
     * Copy using cordova plugin
     * @param text 
     */
    copy(text) {
        this.clipboard.copy(text);
        this.presentToastCopied();
    }

    /**
     * Alert to enter API node
     */
    async onAddApiNode() {
        const alert = await this.alertCtrl.create({
            header: 'Enter API Node',
            inputs: [
                {
                    name: 'node',
                    type: 'text'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {

                        this.networkInfo.apiNode = this.currentNode.apiNode;
                        console.log(this.networkInfo);
                    }
                }, {
                    text: 'Add',
                    handler: (data) => {
                        this.addNode('api', data.node);
                    }
                }
            ]
        });

        await alert.present();
    }

    /**
     * Alert to enter Storage node
     */
    async onAddIpfsNode() {
        const alert = await this.alertCtrl.create({
            header: 'Enter Storage Node',
            inputs: [
                {
                    name: 'node',
                    type: 'text'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                        this.networkInfo.ipfsNode = this.currentNode.ipfsNode;
                        console.log(this.networkInfo);
                    }
                }, {
                    text: 'Add',
                    handler: (data) => {
                        this.addNode('ipfs', data.node);
                    }
                }
            ]
        });

        await alert.present();
    }

    /**
     * Add node to storage
     * @param type 
     * @param node 
     */
    async addNode(type, node) {
        const urlPattern = RegExp(/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/igm);
        if (!urlPattern.test(node)) return;

        this.global.addNodeList(type, node).then(async () => {
            this.nodeList.apiNode = await this.global.fetchNodeList(type);
            this.setNode(type, node);
        });
    }

    /**
     * Change node or add node
     * @param type 
     * @param value
     */
    onNodeChange(type, value) {
        if (type == 'api') {
            if (value == 'add') { this.onAddApiNode(); }
            else {
                this.setNode('api', value);
            }
        }
        if (type == 'ipfs') {
            if (value == 'add') { this.onAddIpfsNode(); }
            else {
                this.setNode('ipfs', value);
            }
        }
    }

    /**
     * Set node
     * @param type 
     * @param node 
     */
    setNode(type, node) {
        if (type == 'api') {
            this.networkInfo.isApiOnl = false;
            this.global.checkNode(node).then(isValid => {
                this.networkInfo.isApiOnl = isValid;
            });

            this.currentNode.apiNode = node;
            this.networkInfo.apiNode = node;
        }
        if (type == 'ipfs') {
            this.currentNode.ipfsNode = node;
            this.networkInfo.ipfsNode = node;
        }
        this.global.setSelectedNode(type, node);
        this.monitor.listener.terminate();
        this.monitor.openListener();
    }

    /**
     * Select a node on view
     * @param type 
     */
    async selectNode(type) {
        let actionSheet = await this.actionSheetCtrl.create({
            header: (type == 'api') ? "Select API Node" : "Select Storage Node",
            translucent: true,
            buttons: this.buttonsGen(type)
        });
        await actionSheet.present();
    }

    /**
     * Create node selectors
     * @param type 
     */
    buttonsGen(type) {
        let buttons = [];
        let list = (type == 'api') ? this.nodeList.apiNode :
            (type == 'ipfs') ? this.nodeList.ipfsNode : [];
        let selected = (type == 'api') ? this.networkInfo.apiNode :
            (type == 'ipfs') ? this.networkInfo.ipfsNode : '';

        for (let btn of list) {
            let button = {
                text: btn,
                cssClass: (btn == selected) ? 'selected option' : 'option',
                handler: () => {
                    this.onNodeChange(type, btn);
                }
            };
            buttons.push(button);
        }
        let addButton = {
            text: '+ Add...',
            handler: () => {
                this.onNodeChange('api', 'add');
            }
        }
        if (type == 'api') buttons.push(addButton);
        return buttons;
    }

    /**
     * Fingerprint
     */
    fingerprint(fn) {
        this.faio.isAvailable()
            .then(res => {
                this.faio.show({
                    clientId: 'Fingerprint-PrivKey', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
                    clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
                    disableBackup: true,  //Only for Android(optional)
                    localizedFallbackTitle: 'Use Pin', //Only for iOS
                    localizedReason: 'Please authenticate' //Only for iOS
                } as any)
                    .then((result: any) => fn())
                    .catch((error: any) => console.log(error));
            })
            .catch(err => { });
    }

    /**
     * Use fingerprint instead of password to show private key
     */
    showPassWithFingerprint() {
        this.fingerprint(() => {
            this.showPrivateKey();
        });
    }

    /**
     * Register a signature
     */
    onRegisterSignature() {
        this.router.navigateByUrl('signature');
    }
}
