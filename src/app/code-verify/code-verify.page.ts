import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { WalletService } from './../services/wallet.service';
import { GlobalService } from './../services/global.service';

@Component({
    selector: 'app-code-verify',
    templateUrl: './code-verify.page.html',
    styleUrls: ['./code-verify.page.scss'],
})
export class CodeVerifyPage implements OnInit {

    pollingEmailVerified;

    constructor(
        private router: Router,
        private alertCtrl: AlertController,
        private toastController: ToastController,
        private firebaseAuth: AngularFireAuth,
    ) { }

    ngOnInit() {
        this.pollingEmailVerified = setInterval(() => {
            this.firebaseAuth.auth.currentUser.reload()
                .then(() => {
                    let isVerified = this.firebaseAuth.auth.currentUser.emailVerified;
                    if (isVerified) {
                        clearInterval(this.pollingEmailVerified);
                        this.firebaseAuth.auth.currentUser.delete()
                            .then(res => {
                                console.log(res);
                                this.router.navigate(['signature']);
                            })
                            .catch(err => { console.log(err); });

                    }
                })
                .catch(() => { });
        }, 1000);
    }

    /**
     * Navigate to sign-up page
     */
    goBack() {
        clearInterval(this.pollingEmailVerified);
        this.firebaseAuth.auth.currentUser.delete()
            .then(res => { console.log(res); })
            .catch(err => { console.log(err); })
            .finally(() => { this.router.navigate(['sign-up']); });
    }

    /**
      * Launch verify phone number process
      */
    onResend() {
        this.firebaseAuth.auth.currentUser.sendEmailVerification()
            .then(res => {
                console.log(res);
                this.presentToastResent()
            })
            .catch(err => console.log(err));
    }

    /**
     * Alert resent
     */
    async presentToastResent() {
        const toast = await this.toastController.create({
            message: 'Verify email is resent',
            duration: 1000
        });
        toast.present();
    }
}
