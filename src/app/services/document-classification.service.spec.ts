import { TestBed } from '@angular/core/testing';

import { DocumentClassificationService } from './document-classification.service';

describe('DocumentClassificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentClassificationService = TestBed.get(DocumentClassificationService);
    expect(service).toBeTruthy();
  });
});
