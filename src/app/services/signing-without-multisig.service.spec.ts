import { TestBed } from '@angular/core/testing';

import { SigningWithoutMultisigService } from './signing-without-multisig.service';

describe('SigningWithoutMultisigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SigningWithoutMultisigService = TestBed.get(SigningWithoutMultisigService);
    expect(service).toBeTruthy();
  });
});
