import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareInfoPage } from './share-info.page';

describe('ShareInfoPage', () => {
    let component: ShareInfoPage;
    let fixture: ComponentFixture<ShareInfoPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ShareInfoPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ShareInfoPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
