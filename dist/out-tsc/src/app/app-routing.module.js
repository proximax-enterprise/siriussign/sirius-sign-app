import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
var routes = [
    { path: '', redirectTo: 'sign-up', pathMatch: 'full' },
    { path: 'app', loadChildren: './tabs/tabs.module#TabsPageModule' },
    { path: 'sign-in', loadChildren: './sign-in/sign-in.module#SignInPageModule' },
    { path: 'sign-up', loadChildren: './sign-up/sign-up.module#SignUpPageModule' },
    { path: 'code-verify', loadChildren: './code-verify/code-verify.module#CodeVerifyPageModule' },
    { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
    { path: 'need-sign', loadChildren: './need-sign/need-sign.module#NeedSignPageModule' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
            ],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map