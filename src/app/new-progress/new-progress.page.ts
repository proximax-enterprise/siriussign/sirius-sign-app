import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

import { SiriusSignSigningTask } from './../model/siriussign-task.model';

import { GlobalService } from './../services/global.service';
import { SignDocumentService } from './../services/sign-document.service';
import { MonitorService } from '../services/monitor.service';
import { SigningWithoutMultisigService } from './../services/signing-without-multisig.service';
import { SignatureService } from './../services/signature.service';
import { UploadStorageService } from './../services/upload-storage.service';
import { DrawSignatureService } from './../services/draw-signature.service';
import { MultitaskService } from './../services/multitask.service';
import { DocumentClassificationService } from './../services/document-classification.service';
import { DocumentStorageService } from './../services/document-strorage.service';

@Component({
    selector: 'app-new-progress',
    templateUrl: './new-progress.page.html',
    styleUrls: ['./new-progress.page.scss'],
})
export class NewProgressPage {

    constructor(
        private navCtrl: NavController,
        private global: GlobalService,
        private signDoc: SignDocumentService,
        private uploadStorage: UploadStorageService,
        private monitor: MonitorService,
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private signature: SignatureService,
        private drawSignature: DrawSignatureService,
        public multitask: MultitaskService,
        private documentClassification: DocumentClassificationService,
        private documentStorage: DocumentStorageService
    ) {
        if (!this.multitask.selectedTask) {
            const task = new SiriusSignSigningTask(
                this.signingWithoutMultisig,
                this.monitor,
                this.uploadStorage,
                this.signature,
                this.global.loggedAccount,
                this.signDoc.document,
                this.drawSignature.signers,
                null
            );
            const isMultisig = task.document.cosigners.length > 0;
            if (isMultisig) task.incMultiSign(task => this.finishSigning(task));
            else task.incSingleSign(task => this.finishSigning(task));
            this.multitask.tasks.push(task);
        }
    }

    ionViewWillEnter() {
        this.goTasks();
    }

    /**
     * Calculate stroke-dashoffset value from percent
     * @returns
     */
    calStrokeDashOffset() {
        let c = Math.PI * (60 * 2);

        if (this.multitask.selectedTask.percent < 0) { this.multitask.selectedTask.percent = 0; }
        if (this.multitask.selectedTask.percent > 100) { this.multitask.selectedTask.percent = 100; }

        let offset = ((100 - this.multitask.selectedTask.percent) / 100) * c;
        return offset + 190;
    }

    /**
     * Fetch and store document after signing done successfully
     * @param task 
     */
    async finishSigning(task: SiriusSignSigningTask) {
        const isSuccess = (task.document.status == 'Confirmed') || (task.document.status == 'Waiting for cosignatures');
        if (isSuccess) {
            const doc = await this.documentClassification.fetchDocumentBySigningTxHash(task.document.signTxHash);
            doc.localUrl = task.document.file.uri;
            this.documentStorage.storeDocument(doc);
            this.documentClassification.classify(doc);
            this.documentClassification.sortDocs('compledAndWaiting');
        }
    }

    /**
     * Navigate to sign result page and lock route back to this page
     */
    goDone() {
        this.global.setIsProgressDone(true);
        this.multitask.selectedTask = null;
        this.navCtrl.navigateRoot('new-done');
    }

    /**
     * Navigate to tasks tab
     */
    goTasks() {
        this.global.setIsProgressDone(true);
        this.multitask.selectedTask = null;
        this.navCtrl.navigateRoot('/app/tabs/tab-tasks');
    }
}