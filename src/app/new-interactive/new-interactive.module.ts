import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProgressGuardService, ProgressGuardCanDeactiveService } from '../services/guards/progress-guard.service';

import { NewInteractivePage } from './new-interactive.page';
import { ComponentModule } from './../components/component.module';

const routes: Routes = [
    {
        path: '',
        component: NewInteractivePage
    },
    {
        path: 'new-multi-sign',
        loadChildren: '../new-multi-sign/new-multi-sign.module#NewMultiSignPageModule'
    },
    {
        path: 'new-only-me',
        loadChildren: '../new-only-me/new-only-me.module#NewOnlyMePageModule'
    },
    {
        path: 'new-progress', loadChildren: '../new-progress/new-progress.module#NewProgressPageModule',
        canActivate: [ProgressGuardService],
        canDeactivate: [ProgressGuardCanDeactiveService]
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ComponentModule,
        RouterModule.forChild(routes)
    ],
    declarations: [NewInteractivePage]
})
export class NewInteractivePageModule { }
