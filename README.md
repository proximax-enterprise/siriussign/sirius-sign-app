# Install
## Install Ionic
Follow instruction at `https://ionicframework.com/docs/installation/cli`
## Install dependencies
Run ```npm install```  
If there's any packages update, run ```node patch.js``` to fix crypto.js errors on browser, stream-http on Chrome 77 before building app. This is the postscript, so it will run follow npm install automatically.
# Custom
## Use your Firebase instance
### Create Firebase project
Go to `https://console.firebase.google.com/` and create a new project.
### Enable Sign in
From the Firebase Console, select **Authentication**. Then, go to **SIGN-IN METHOD** and enable **Email/Password**.
### Connect your Firebase project with the SiriusSign
From the Firebse Console, select **Project Overview**. Then choose **Add Firebase to your web app**.  
Enter the app information and **Register app**. Copy **firebaseConfig** JSON and paste it to **environment** constant in `./src/environments/environment.ts` (replace the existing one).
# Run app
Follow instruction from Ionic document. Recommend using Ionic's Cordova running app cli.
## Android
Follow `https://ionicframework.com/docs/building/android`
## iOS
Follow `https://ionicframework.com/docs/building/ios`