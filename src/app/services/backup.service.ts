import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';

import * as CryptoJS from 'crypto-js';
import { QRCode, ErrorCorrectLevel, QR8BitByte } from 'qrcode-generator-ts/js'
import * as zlib from 'zlib';

import { GlobalService } from './global.service';
// import { GroupService } from './group.service';
import { SignatureService } from './signature.service';
import { HelperService } from './helper.service';
import { Scrambler } from './../lib/scrambler';

export interface GroupShortInfo {
    name: string,
    creationTxHash: string
}

export interface BackupInfo {
    name: string,
    privateKey: string,
    // notaPrivateKey: string,
    // groupsShortInfo: GroupShortInfo[],
    // signatureUploadTx: string
    signatureImg: string,
    plan: number
}

@Injectable({
    providedIn: 'root'
})
export class BackupService {

    constructor(
        private platform: Platform,
        private file: File,
        private fileOpener: FileOpener,
        private global: GlobalService,
        // private group: GroupService,
        private signature: SignatureService
    ) { }

    async compress(data: string, enc: 'utf8' | 'base64') {
        return new Promise<string>((resolve, reject) => {
            zlib.deflate(data, (err: any, buffer: Buffer) => {
                if (!err) {
                    resolve(buffer.toString(enc));
                } else {
                    console.log(err);
                    reject(err);
                }
            });
        });
    }

    async decompress(data: string, enc: 'utf8' | 'base64') {
        const buffer = Buffer.from(data, enc);
        return new Promise<string>((resolve, reject) => {
            zlib.inflate(buffer, (err: any, buffer: Buffer) => {
                if (!err) {
                    console.log(buffer.toString());
                    resolve(buffer.toString());
                } else {
                    reject(err);
                }
            });
        });
    }

    private async getString() {
        const key = 'fjAr96DiZX81BCJnBnWf4y97vUaYPMjFP8WIRmPLE7KRlajbfzKLBLeDa3sg7aHi';
        // const groups = await this.group.fetchGroups();
        // const groupsCreationTx: GroupShortInfo[] = groups.map(group => {
        //     return { name: group.name, creationTxHash: group.creationTxHash };
        // });
        await this.signature.fetchFromStorage();
        const signatureImg = HelperService.base64toUtf8(this.signature.signatureImg.split(';')[1].split(',')[1]).toString();
        const backupInfo: BackupInfo = {
            name: this.global.loggedWallet.name,
            privateKey: Scrambler.scramble(this.global.loggedAccount.privateKey),
            // notaPrivateKey: Scrambler.scramble(this.global.notarizationAccount.privateKey),
            signatureImg: await this.compress(signatureImg, 'base64'),
            plan: this.global.loggedWallet.plan
        }
        console.log(backupInfo);
        const encBackupString = CryptoJS.AES.encrypt(JSON.stringify(backupInfo), key);
        const compressedBackupString = await this.compress(JSON.stringify(backupInfo), 'base64');
        return compressedBackupString;
    }

    async getQr() {
        const backupString = await this.getString();
        const types = [1940, 2050, 2170, 2290, 2420, 2550, 2680, 2790, 2953];
        let typeNumber = 32;
        for (let i = 0; i < types.length - 1; i += 1) {
            if ((backupString.length - types[i]) * (backupString.length - types[i + 1]) <= 0)
                typeNumber += (i + 1);
        }
        console.log(typeNumber);
        let qr = new QRCode();
        qr.setTypeNumber(typeNumber);
        qr.setErrorCorrectLevel(ErrorCorrectLevel.L);
        qr.addData(new QR8BitByte(backupString));
        qr.make();
        return qr.toDataURL();
    }

    async revert(compressedBackupString: string) {
        const backupString = await this.decompress(compressedBackupString, 'base64');
        try {
            // const key = 'fjAr96DiZX81BCJnBnWf4y97vUaYPMjFP8WIRmPLE7KRlajbfzKLBLeDa3sg7aHi';
            // const encBackupString = HelperService.hexToBase64(backupString);
            // const plainBackupString = CryptoJS.AES.decrypt(encBackupString, key).toString(CryptoJS.enc.Utf8);
            // const scrambledBackupInfo: BackupInfo = JSON.parse(plainBackupString);
            const scrambledBackupInfo: BackupInfo = JSON.parse(backupString);
            const backupInfo: BackupInfo = {
                name: scrambledBackupInfo.name,
                privateKey: Scrambler.scramble(scrambledBackupInfo.privateKey),
                // notaPrivateKey: Scrambler.scramble(scrambledBackupInfo.notaPrivateKey),
                // groups: scrambledBackupInfo.groups
                // groupsShortInfo: scrambledBackupInfo.groupsShortInfo,
                // signatureUploadTx: scrambledBackupInfo.signatureUploadTx
                signatureImg: 'data:image/svg+xml;base64,'
                    + HelperService.utf8toBase64(
                        await this.decompress(scrambledBackupInfo.signatureImg, 'base64')
                    ),
                plan: scrambledBackupInfo.plan
            }
            return backupInfo;
        }
        catch (err) {
            throw new Error('Invalid backup string');
        }
    }

    async saveToFile() {
        const backupString = await this.getString();
        console.log(backupString);
        let path = '';
        const option = {
            replace: true
        }
        const userNameParts = this.global.loggedWallet.name.split('@');
        const fileName = userNameParts[0] + '-' + userNameParts[1] + '-backup.ssb';

        if (this.platform.is('ios')) {
            path = this.file.syncedDataDirectory;
            this.file.checkDir(path, '')
                .then(res => {
                    this.file.writeFile(path, fileName, backupString, option)
                        .then(response => {
                            this.fileOpener.open(path + fileName, 'text/plain')
                                .then(() => console.log('Opened'))
                                .catch(err => {
                                    console.log('open file error');
                                    console.log(err);
                                });
                        })
                        .catch(err => {
                            console.log('wr file error');
                            console.log(err);
                        });
                }).then(err => {
                    console.log('check dir error');
                    console.log(err);
                });
        }
        else if (this.platform.is('android')) {
            path = this.file.externalDataDirectory;
            await this.file.writeFile(path, fileName, backupString, option);
            return path + fileName;
        }

        if (this.global.isBrowser) {
            this.downloadFileWithDataUri(backupString, fileName);
        }
    }

    /**
     * Download pdf file with data uri
     * @param data 
     * @param fileName 
     */
    downloadFileWithDataUri(data, fileName) {
        // Construct the <a> element
        var link = document.createElement("a");
        link.download = fileName;
        // Construct the uri
        var uri = 'data:text/plain;charset=utf-8,' + encodeURIComponent(data);
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        // Cleanup the DOM
        document.body.removeChild(link);
    }
}
