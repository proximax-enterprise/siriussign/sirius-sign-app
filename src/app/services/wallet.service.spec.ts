import { TestBed } from '@angular/core/testing';

import { WalletService } from './wallet.service';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device/ngx';

describe('WalletService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            IonicStorageModule.forRoot()
          ],
          providers: [
              Storage,
              Device
          ]
    }));

    it('should be created', () => {
        const service: WalletService = TestBed.get(WalletService);
        expect(service).toBeTruthy();
    });
});
