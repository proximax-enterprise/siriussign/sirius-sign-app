import { HelperService } from './helper.service';
import { Injectable } from '@angular/core';

import * as CryptoJS from 'crypto-js';

import {
    BlockchainNetworkConnection,
    IpfsConnection,
    UploadParameter,
    Uploader,
    ConnectionConfig,
    Protocol,
    Uint8ArrayParameterData,
    Downloader,
    DownloadParameter,
    DownloadResult
} from 'tsjs-chain-xipfs-sdk';
import { PublicAccount } from 'tsjs-xpx-chain-sdk';

import { GlobalService } from './global.service';
import { SignDocument } from './sign-document.service';
import { SiriusSignDocument } from '../model/siriussign-document.model';

@Injectable({
    providedIn: 'root'
})
export class UploadStorageService {
    // Creates ipfs connection
    ipfsConnection = new IpfsConnection(
        this.global.ipfsDomain, // the host or multi address
        this.global.ipfsPort, // the port number
        (this.global.ipfsProtocol == 'https') ? { protocol: 'https' } : { protocol: 'http' } // the optional protocol
    );

    // Creates Proximax blockchain network connection
    blockchainConnection = new BlockchainNetworkConnection(
        this.global.blockchainNetworkType, // the network type
        this.global.apiDomain, // the rest api base endpoint
        this.global.apiPort, // the optional websocket end point
        (this.global.apiProtocol == 'https') ? Protocol.HTTPS : Protocol.HTTP
    );

    // Connection Config
    conectionConfig = ConnectionConfig.createWithLocalIpfsConnection(this.blockchainConnection, this.ipfsConnection);

    //
    uploader = new Uploader(this.conectionConfig);
    dowloader = new Downloader(this.conectionConfig);

    constructor(private global: GlobalService) { }

    /**
     * Create upload params builder
     * @param document
     */
    private createParamsBuilder(document: SignDocument) {
        const file = document.file;
        console.log(file);
        const fileContent = file.data;
        const fileName = file.name;
        const fileType = file.mediaType;

        const documentAccountPriv = CryptoJS.AES.encrypt(document.documentAcc.privateKey, this.global.loggedAccount.privateKey);
        const documentAccountEncPriv = HelperService.base64toHex(documentAccountPriv.toString());

        const metaData = new Map<string, string>([
            ['SFH', document.fileHash],
            ['SDA', documentAccountEncPriv]
        ]);

        const privateKey = this.global.loggedAccount.privateKey;

        const metaParams = Uint8ArrayParameterData.create(fileContent, fileName, '', fileType, metaData);
        console.log(metaParams);
        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        console.log(uploadParamsBuilder);
        return uploadParamsBuilder;
    }

    /**
     * Upload file from signDoc
     * @param document
     */
    uploadFile(document: SignDocument, isEncrypt: boolean) {
        const privateKey = this.global.loggedAccount.privateKey;
        const publicKey = this.global.loggedAccount.publicKey;

        try {
            const uploadParamsBuilder = this.createParamsBuilder(document);
            const uploadParams = isEncrypt ?
                uploadParamsBuilder
                    .withRecipientPublicKey(document.documentAcc.publicKey)
                    .withTransactionMosaics([])
                    .withNemKeysPrivacy(privateKey, publicKey)
                    .build() :
                uploadParamsBuilder
                    .withRecipientPublicKey(document.documentAcc.publicKey)
                    .withTransactionMosaics([])
                    .build();
            console.log(uploadParams);

            // call upload services
            return this.uploader.upload(uploadParams);
            // console.log('Result');
            // console.log(result);
        }
        catch (err) {
            console.log(err);
            throw new Error('Failed to upload file');
        }
    }

    /**
     * Download file from file hash
     * @param hash
     */
    downloadFile(hash: string, isEncrypt: boolean) {
        const privateKey = this.global.loggedAccount.privateKey;
        const publicKey = this.global.loggedAccount.publicKey;

        const downloadParamsBuilder = DownloadParameter.create(hash);
        console.log(downloadParamsBuilder);
        const downloadParams = isEncrypt ? downloadParamsBuilder.withNemKeysPrivacy(privateKey, publicKey).build() :
            downloadParamsBuilder.build();
        console.log(downloadParams);

        return this.dowloader.download(downloadParams);
    }

    /**
     * Download file by hash and convert to data URI
     * @param hash
     * @param isEncrypt
     * @param privateKey
     */
    async downloadFileToDataUri(hash: string, isEncrypt: boolean = false) {
        let downloadResult: DownloadResult
        try {
            downloadResult = await this.downloadFile(hash, isEncrypt);
        }
        catch (err) {
            throw err;
        }
        const buffer: Buffer = await downloadResult.data.getContentAsBuffer()
            .catch(err => {
                throw err;
            });
        const dataString = await downloadResult.data.getContentsAsString();
        const data = buffer.toString('base64');
        const fileType = downloadResult.data.contentType;
        return 'data:' + fileType + ';base64,' + encodeURI(data);
    }

    /**
     * Upload signature image
     * @param signatureImg
     */
    uploadSignature(signatureImg: string) {
        const fileContent = HelperService.convertDataURIToBinary(signatureImg);
        const userNameParts = this.global.loggedWallet.name.split('@');
        const fileName = userNameParts[0] + '-' + userNameParts[1] + '-signature.svg';
        const fileType = 'image/svg+xml';
        const fileDescription = 'SiriusSign Signature Image';

        const fileHash = CryptoJS.SHA256(signatureImg).toString();
        const metaData = new Map<string, string>([
            ['SSH', fileHash]
        ]);

        const privateKey = this.global.loggedAccount.privateKey;

        const metaParams = Uint8ArrayParameterData.create(
            fileContent,
            fileName,
            fileDescription,
            fileType,
            metaData
        );
        console.log(metaParams);

        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        console.log(uploadParamsBuilder);

        const uploadParams = uploadParamsBuilder.withTransactionMosaics([]).build();
        console.log(uploadParams);

        return this.uploader.upload(uploadParams);
    }

    /**
     * Upload signature image
     * @param signatureImg
     */
    uploadSignatureForDocSigning(signatureImg: string, documentPublicAccount: PublicAccount) {
        const fileContent = HelperService.convertDataURIToBinary(signatureImg);
        const userNameParts = this.global.loggedWallet.name.split('@');
        const fileName = userNameParts[0] + '-' + userNameParts[1] + '-signature.svg';
        const fileType = 'image/svg+xml';
        const fileDescription = 'SiriusSign Signature Image';

        const fileHash = CryptoJS.SHA256(signatureImg).toString();
        const metaData = new Map<string, string>([
            ['SSH', fileHash]
        ]);

        const privateKey = this.global.loggedAccount.privateKey;

        const metaParams = Uint8ArrayParameterData.create(
            fileContent,
            fileName,
            fileDescription,
            fileType,
            metaData
        );
        console.log(metaParams);

        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        console.log(uploadParamsBuilder);

        const uploadParams = uploadParamsBuilder
            .withRecipientPublicKey(documentPublicAccount.publicKey)
            .withTransactionMosaics([])
            .build();
        console.log(uploadParams);

        return this.uploader.upload(uploadParams);
    }

    /**
     * Upload signature image
     * @param signatureImg
     */
    uploadCompletedDoc(completedDoc: Uint8Array, filename: string, documentPublicAccount: PublicAccount, isEncrypt: boolean, document: SignDocument | SiriusSignDocument) {
        console.log('[Upload Storage] Upload Completed Document');

        const fileContent = completedDoc;
        const fileName = filename + '-' + 'signed.pdf';
        const fileType = 'application/pdf';
        const fileDescription = 'SiriusSign Signed Document';

        const completedDocDataUrl = 'data:' + fileType + ';base64,' + HelperService.bytesToBase64(completedDoc);
        const cFileHash = CryptoJS.SHA256(completedDocDataUrl).toString();
        let oFileHash: string;
        if (document instanceof SiriusSignDocument) {
            oFileHash = (<SiriusSignDocument>document).fileHash.sha256Hash;
        }
        else {
            oFileHash = (<SignDocument>document).fileHash;
        }
        const metaData = new Map<string, string>([
            ['SCH', cFileHash],
            ['SFH', oFileHash]
        ]);

        const privateKey = this.global.loggedAccount.privateKey;
        const publicKey = this.global.loggedAccount.publicKey;

        const metaParams = Uint8ArrayParameterData.create(
            fileContent,
            fileName,
            fileDescription,
            fileType,
            metaData
        );
        console.log(metaParams);

        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        console.log(uploadParamsBuilder);

        const uploadParams = isEncrypt ?
            uploadParamsBuilder
                .withRecipientPublicKey(documentPublicAccount.publicKey)
                .withTransactionMosaics([])
                .withNemKeysPrivacy(privateKey, publicKey)
                .build() :
            uploadParamsBuilder
                .withRecipientPublicKey(documentPublicAccount.publicKey)
                .withTransactionMosaics([])
                .build();
        console.log(uploadParams);

        return this.uploader.upload(uploadParams);
    }
}
