import { TestBed } from '@angular/core/testing';

import { MultitaskService } from './multitask.service';

describe('MultitaskService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: MultitaskService = TestBed.get(MultitaskService);
        expect(service).toBeTruthy();
    });
});
