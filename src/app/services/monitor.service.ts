import { Injectable } from '@angular/core';

import { Listener, Address, Account, PublicAccount, Transaction, TransactionHttp } from 'tsjs-xpx-chain-sdk'

import { GlobalService } from './global.service';

@Injectable({
    providedIn: 'root'
})
export class MonitorService {

    listener: Listener;

    constructor(private global: GlobalService) { }

    /**
     * Open websocket to listen events
     */
    openListener() {
        this.listener = new Listener(this.global.ws, WebSocket);
        return new Promise((resolve, reject) => {
            this.listener.open()
                .then(() => { resolve(); })
                .catch(err => { reject(err) });
        });
    }

    /**
     * Check if the connection is still open
     */
    checkListener() {
        if (!this.listener) return false;
        const listenerAsAny = this.listener as any;
        if (!listenerAsAny.webSocket) return false;
        return listenerAsAny.webSocket.readyState === 1;
    }

    /**
     * Check if the connection is still open, if not then reopen
     */
    async checkAndOpenListener() {
        if (!this.checkListener()) await this.openListener();
    }

    /**
     * Close websocket
     */
    closeListener() {
        this.listener.close();
    }

    /**
     * Listen for confirmed transaction
     * @param address 
     * @param fn1 
     * @param fn2 
     */
    async listenConfirmed(address: string, fn1?, fn2?) {
        const addr = Address.createFromRawAddress(address);

        await this.checkAndOpenListener();
        const confirmedTxchannel = this.listener.confirmed(addr)
            .subscribe(transaction => {
                console.log('From Listener:')
                console.log(transaction);
                if (fn2) fn2(transaction);
            }, err => {
                console.log('[Listener] listenConfirmedTx Error at address: ' + addr.plain())
                console.log(err)
            }, () => {
                console.log('[Listener] listenConfirmedTx Done at address: ' + addr.plain())
            });
        if (fn1) fn1();
        return confirmedTxchannel;
    }

    /**
     * Open websocket to listen status of transaction
     * @param senderAcc 
     * @param signedTx 
     * @async
     */
    async listenTransaction(senderAcc: Account | PublicAccount, signedTxHash: string, fnAnnounce, fnConfirmed, fnError) {
        await this.checkAndOpenListener();
        const statusSub = this.listener.status(senderAcc.address)
            .subscribe(status => {
                console.log('[Listener] Transaction get a failure');
                console.log(status);
                fnError(status);
            }, error => {
                console.log('[Listener] Status Listener Error');
                console.log(error);
            }, () => {
                console.log('[Listener] Status Listener Done');
            });

        const confirmSub = this.listener.confirmed(senderAcc.address).subscribe(successTx => {
            if (successTx && successTx.transactionInfo && successTx.transactionInfo.hash === signedTxHash) {
                console.log('[Listener] Transaction is on the network now');
                console.log(successTx);
                fnConfirmed(successTx);
                confirmSub.unsubscribe();
                statusSub.unsubscribe();
            }
        }, error => {
            console.log('[Listener] Confirm Listener Error');
            console.error(error);
        }, () => {
            console.log('[Listener] Confirmed Listener Done.');
        });
        fnAnnounce();
    }

    /**
     * Get aggregate bonded transaction
     * @param fn1
     * @param fn2
     */
    async listenAggregateBondedTransaction(address, fn1?, fn2?) {
        let addr = address;
        if (typeof address == 'string') addr = Address.createFromRawAddress(address);

        await this.checkAndOpenListener();
        const aggBondedTxchannel = this.listener.aggregateBondedAdded(addr)
            .subscribe(transaction => {
                console.log('From Listener:')
                console.log(transaction);
                if (fn2) fn2(transaction);
            }, err => {
                console.log('[Listener] listenAggregateBondedTx Error at address: ' + addr.plain())
                console.log(err)
            }, () => {
                console.log('[Listener] listenAggregateBondedTx Done at address: ' + addr.plain())
            });
        if (fn1) fn1();
        return aggBondedTxchannel;
    }

    /**
     * Listen Aggregate Bonded Cosignature
     * @param address 
     * @param fn1 Callback function work after listener subcribe the channel
     * @param fn2 Callback function with cosignSignedTransaction returned
     */
    async listenAggregateBondedCosignature(address, fn1?, fn2?) {
        let addr = address;
        if (typeof address == 'string') addr = Address.createFromRawAddress(address);

        await this.checkAndOpenListener();
        const cosignature = this.listener.cosignatureAdded(addr)
            .subscribe(cosignSignedTx => {
                console.log(cosignSignedTx);
                fn2(cosignSignedTx);
            }, err => {
                console.log('[Listener] listenAggregateBondedCosignature Error at address: ' + addr.plain());
                console.log(err);
            }, () => {
                console.log('[Listener] listenAggregateBondedCosignature Done at address: ' + addr.plain());
            });

        fn1();

        return cosignature;
    }

    async waitForTransactionConfirmed(address: Address, signedTxHash: string, fnAnnounce) {
        await this.checkAndOpenListener();
        return new Promise<Transaction>((resolve, reject) => {
            const statusSub = this.listener.status(address)
                .subscribe(
                    status => {
                        clearTimeout(timeoutListener);
                        reject(status);
                    },
                    error => { reject(error.status); },
                    () => { console.log('[Listener] Status Listener Done'); }
                );

            const confirmSub = this.listener.confirmed(address)
                .subscribe(
                    successTx => {
                        if (successTx && successTx.transactionInfo && successTx.transactionInfo.hash === signedTxHash) {
                            console.log('[Listener] Transaction is on the network now');
                            console.log(successTx);
                            clearTimeout(timeoutListener);
                            resolve(successTx);
                            confirmSub.unsubscribe();
                            statusSub.unsubscribe();
                        }
                    },
                    error => { reject(error.status); },
                    () => { console.log('[Listener] Confirmed Listener Done.'); }
                );
            fnAnnounce();

            const timeoutListener = setTimeout(async () => {
                const transactionHttp = new TransactionHttp(this.global.apiNode);
                const status = await transactionHttp.getTransactionStatus(signedTxHash).toPromise();

                if (status.group == 'unconfirmed') {
                    const statusPolling = setInterval(async () => {
                        const statusReget = await transactionHttp.getTransactionStatus(signedTxHash).toPromise();
                        if (statusReget.group == 'confirmed') {
                            const tx = await transactionHttp.getTransaction(signedTxHash).toPromise();
                            resolve(tx);
                            clearInterval(statusPolling);
                        }
                        if (statusReget.group == 'failed') {
                            reject(statusReget.status);
                            clearInterval(statusPolling);
                        }
                    }, 500);
                }

                if (status.group == 'confirmed') {
                    const tx = await transactionHttp.getTransaction(signedTxHash).toPromise();
                    resolve(tx);
                }

                if (status.group == 'failed') {
                    reject(status.status);
                }
            }, 30 * 1000);
        });
    }
}
