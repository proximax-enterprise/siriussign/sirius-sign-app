import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

import { GlobalService } from './../services/global.service';
import { WalletService } from './../services/wallet.service';
import { BackupService } from './../services/backup.service';

@Component({
    selector: 'app-backup',
    templateUrl: './backup.page.html',
    styleUrls: ['./backup.page.scss'],
})
export class BackupPage implements OnInit {

    accountName = '';
    address = '';
    qrCode = '';
    isShow = false;
    isQr = false;
    savedPath = '';

    constructor(
        private router: Router,
        private alertCtrl: AlertController,
        private faio: FingerprintAIO,
        private global: GlobalService,
        private wallet: WalletService,
        private backup: BackupService
    ) { }

    ngOnInit() {
        this.accountName = this.global.loggedWallet.name;
        this.address = this.global.loggedWallet.address.pretty();
        this.inputPassword();
    }

    /**
     * Show backup QR code
     */
    async showBackup() {
        this.qrCode = await this.backup.getQr().catch(err => null);
        if (this.qrCode) {
            this.isQr = true;
            this.isShow = true;
        }
        else {
            this.isQr = false;
            this.isShow = true;
        }
    }

    /**
     * Launch enter password alert
     */
    async inputPassword() {
        let buttons = [
            {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: () => {
                    this.router.navigateByUrl('/app/tabs/tab-menu/home');
                }
            }, {
                text: 'Ok',
                handler: (data) => {
                    this.verifyPass(data.pass);
                }
            }
        ];
        console.log(this.global.isAllowFingerprint);
        if (this.global.isAllowFingerprint) {
            buttons.push(
                {
                    text: 'Fingerprint',
                    handler: () => {
                        this.fingerprint(() => this.showBackup());
                    }
                }
            )
        }
        const alert = await this.alertCtrl.create({
            header: 'Please enter your password',
            inputs: [
                {
                    name: 'pass',
                    type: 'password'
                }
            ],
            buttons: buttons
        });

        await alert.present();
    }

    /**
     * Show backup
     */
    verifyPass(password: string) {
        const isPassOk = this.wallet.checkWalletPassword(this.global.loggedWallet, password);
        if (isPassOk) this.showBackup();
        else this.alertWrongPass();
    }

    /**
     * Launch verify password
     */
    async alertWrongPass() {
        const alert = await this.alertCtrl.create({
            header: 'Your password is invalid.',
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        this.router.navigateByUrl('/app/tabs/tab-menu/home');
                    }
                }
            ]
        });
        await alert.present();
    }

    /**
     * Alert backup file saved
     */
    async alertSaved() {
        const alert = await this.alertCtrl.create({
            header: 'SiriusSign Backup file is saved.',
            subHeader: 'Please keep your backup file secret.',
            message: (this.savedPath) ? 'File path: ' + this.savedPath : '',
            buttons: ['OK']
        });
        await alert.present();
    }

    /**
     * Save backup string to file
     */
    async onSave() {

        this.backup.saveToFile().then(path => {
            this.savedPath = path;
            this.alertSaved();
        })
    }

    /**
     * Fingerprint
     */
    fingerprint(fn) {
        this.faio.isAvailable()
            .then(res => {
                this.faio.show({
                    clientId: 'Fingerprint-Backup', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
                    clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
                    disableBackup: true,  //Only for Android(optional)
                    localizedFallbackTitle: 'Use Pin', //Only for iOS
                    localizedReason: 'Please authenticate' //Only for iOS
                } as any)
                    .then((result: any) => fn())
                    .catch((error: any) => console.log(error));
            })
            .catch(err => { });
    }
}
