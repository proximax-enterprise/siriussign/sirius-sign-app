import { Injectable } from '@angular/core';

import { NamePosition } from '../model/signature-canvas.model';
import { Account } from 'tsjs-xpx-chain-sdk';
export interface SignaturePosition {
    page: number,
    x: number,
    y: number,
    name: string,
    namePosition: NamePosition
}

export interface SignatureInfo {
    publicKey: string,
    name: string,
    signaturePositions: SignaturePosition[]
}

export interface VerifyInfo {
    publicKey: string,
    publicAcc: Account
    // signaturePosition: SignaturePosition
}

export interface SignatureImageInfo {
    publicKey: string,
    signatureImg: string
}

@Injectable({
    providedIn: 'root'
})
export class DrawSignatureService {

    name: string;
    pdfUri: string;

    isInit: boolean = false;
    signers: SignatureInfo[] = [];
    signatureImgs: SignatureImageInfo[] = [];
    selectedSignerIndex: number = 0;

    cosigner: SignatureInfo;

    constructor() {
        this.selectedSignerIndex = 0;
    }

    /**
     * Reset draw state
     */
    reset() {
        this.selectedSignerIndex = 0;
        this.signers = [];
        this.isInit = false;
    }

    /**
     * Init signer with signature
     * @param publicKey 
     * @param name 
     */
    setSigner(publicKey: string, name: string) {
        const signatureInfo: SignatureInfo = {
            publicKey: publicKey,
            name: name,
            signaturePositions: [{
                name: name,
                namePosition: NamePosition.NONE,
                page: 1,
                x: 0,
                y: 0
            }]
        }
        this.signers.push(signatureInfo);
    }

    /**
     * Add a new signature position
     * @param publicKey 
     */
    addSignaturePosition(publicKey: string) {
        const idxOfCurrentSigner = this.signers.map(signer => signer.publicKey).indexOf(publicKey);
        this.signers[idxOfCurrentSigner].signaturePositions.push({
            name: this.signers[idxOfCurrentSigner].name,
            namePosition: NamePosition.NONE,
            page: 1,
            x: 0,
            y: 0
        });
        return this.signers[idxOfCurrentSigner].signaturePositions.length - 1;
    }

    /**
     * Update position when user drag and drop signature
     * @param publicKey 
     * @param page 
     * @param x 
     * @param y 
     * @param namePosition 
     */
    updateSignaturePosition(publicKey: string, id: number, page: number, x: number, y: number, namePosition: NamePosition) {
        const publicKeys = this.signers.map(signer => signer.publicKey);
        const index = publicKeys.indexOf(publicKey);
        this.signers[index].signaturePositions[id].page = page;
        this.signers[index].signaturePositions[id].x = x;
        this.signers[index].signaturePositions[id].y = y;
        this.signers[index].signaturePositions[id].namePosition = namePosition;
    }

    /**
     * Remove a position of a signer signature
     * @param publicKey
     * @param id
     */
    removeSignaturePosition(publicKey: string, id: number) {
        console.log(id);
        const publicKeys = this.signers.map(signer => signer.publicKey);
        const index = publicKeys.indexOf(publicKey);
        console.log(this.signers[index].signaturePositions);
        this.signers[index].signaturePositions.splice(id, 1);
        console.log(this.signers[index].signaturePositions);
    }
}
