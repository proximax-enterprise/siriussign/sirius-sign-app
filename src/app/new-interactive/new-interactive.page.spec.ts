import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewInteractivePage } from './new-interactive.page';

describe('NewInteractivePage', () => {
  let component: NewInteractivePage;
  let fixture: ComponentFixture<NewInteractivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewInteractivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewInteractivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
