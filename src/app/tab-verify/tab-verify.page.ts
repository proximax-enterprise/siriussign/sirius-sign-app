import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, Platform, ToastController } from '@ionic/angular';

import { Device } from '@ionic-native/device/ngx';
import { Chooser, ChooserResult } from '@ionic-native/chooser/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { IOSFilePicker } from '@ionic-native/file-picker/ngx';
import { File } from '@ionic-native/file/ngx';

import { SiriusSignDocument } from '../model/siriussign-document.model';

import { GlobalService } from './../services/global.service';
import { HelperService } from '../services/helper.service';
import { VerifyService } from './../services/verify.service';
import { DocumentClassificationService, DocumentVerifyingStatus } from '../services/document-classification.service';
import { DocumentStorageService } from './../services/document-strorage.service';

@Component({
    selector: 'app-tab-verify',
    templateUrl: 'tab-verify.page.html',
    styleUrls: ['tab-verify.page.scss']
})
export class TabVerifyPage {

    isSelected = false;
    onChooser = false;

    isLoading: boolean = false;
    owner: string;
    isFileLoading: boolean = false;

    flag = {
        COMPLETED: DocumentVerifyingStatus.VERIFIED,
        WAITING: DocumentVerifyingStatus.VERIFYING,
        NEEDSIGN: DocumentVerifyingStatus.NEEDVERIFY
    }


    constructor(
        private platform: Platform,
        private device: Device,
        private alertCtrl: AlertController,
        private toastController: ToastController,
        private router: Router,
        private chooser: Chooser,
        private fileOpener: FileOpener,
        private filePath: FilePath,
        private fileChooser: FileChooser,
        private filePicker: IOSFilePicker,
        private file: File,
        private global: GlobalService,
        public verify: VerifyService,
        private barcodeScanner: BarcodeScanner,
        public documentClassification: DocumentClassificationService,
        private documentStorage: DocumentStorageService
    ) {
        this.isSelected = false;
        this.onChooser = this.device.platform != 'browser' && this.device.platform != null;
        this.owner = this.global.loggedAccount.publicKey;
        this.verify.observableIsNeedReload.subscribe(isNeedReload => {
            if (isNeedReload) this.fetchFromChain();
        })
        //!this.platform.is('desktop') || !this.platform.is('mobileweb');
    }

    dateToShortString = HelperService.dateToShortString;

    /**
     * Navigate to verify file view
     */
    goVerify() {
        this.router.navigate(['verify-file-view']);
    }

    /**
     * Alert no Internet access
     */
    async presentToastNoInternet() {
        const toast = await this.toastController.create({
            message: 'No internet access!',
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }

    /**
     * Navigate to audit progress
     */
    goProgress() {
        if (!this.global.isOnline) {
            this.presentToastNoInternet();
            return;
        }
        this.verify.isRealVerify = false;
        this.isSelected = false;
        this.global.setIsProgressDone(false);
        this.router.navigate(['verify-progress']);
    }

    /**
     * Open file chooser then return file uri, switch view to file list
     * @async
     */
    async browseFile() {
        if (!this.onChooser) {
            document.getElementById('browseVerify').click();
            return;
        }

        // const file: ChooserResult = await this.chooser.getFile('application/pdf')
        //     .catch(err => { console.log(err); return null; });

        // if (file) {
        //     if (file.mediaType != 'application/pdf') {
        //         this.alertInvalidFileType();
        //         return;
        //     }
        //     this.verify.setDocument(file);
        //     this.isSelected = true;
        // }
        // else if (this.verify.document.file.name == '') {
        //     this.isSelected = false;
        // }
        this.browseFileByChooser();
    }

    /**
     * Open file chooser then return file uri, switch view to file list
     * @async
     */
    async browseFileByChooser() {
        this.isFileLoading = true;
        let fileUri: string;
        if (this.platform.is('android')) {
            fileUri = await this.fileChooser.open()
                .catch(err => {
                    console.log(err);
                    this.isFileLoading = false;
                    return null;
                });
        }
        else if (this.platform.is('ios')) {
            fileUri = 'file://' + await this.filePicker.pickFile()
                .catch(err => {
                    console.log(err);
                    this.isFileLoading = false;
                    return null;
                });
        }

        if (fileUri) {
            console.log(fileUri);
            let nativeFileUri = fileUri;
            if (this.platform.is('android'))
                nativeFileUri = await this.filePath.resolveNativePath(fileUri);
            const seperatorIdx = nativeFileUri.lastIndexOf('/');
            const filePath = nativeFileUri.substring(0, seperatorIdx + 1);
            const fileName = nativeFileUri.substring(seperatorIdx + 1);
            const fileDataUri = await this.file.readAsDataURL(filePath, fileName)
                .catch(err => {
                    console.log(err);
                    this.isFileLoading = false;
                    return null;
                });
            if (!fileDataUri) return;

            const fileType = fileDataUri.split(';')[0].split(':')[1];
            if (fileType != 'application/pdf') {
                this.alertInvalidFileType();
                this.isFileLoading = false;
                return;
            }

            const fileData = HelperService.convertDataURIToBinary(fileDataUri);
            const fileSize = fileData.length;
            const file: ChooserResult = {
                data: fileData,
                dataURI: fileDataUri,
                mediaType: fileType,
                name: fileName,
                uri: fileUri
            }
            this.verify.setDocument(file, fileSize);
            this.isSelected = true;
            this.isFileLoading = false;
        }
        else if (this.verify.document.file.name == '') {
            this.isSelected = false;
        }
    }

    /**
     * Browse file for web browser
     * @param files
     */
    onFile(files) {
        this.isFileLoading = true;
        console.log(files);
        const reader = new FileReader();
        reader.onload = (event) => {
            console.log(event.target);
            const target = <any>event.target;
            const file = {
                data: new Uint8Array(0),
                dataURI: target.result,
                mediaType: target.result.split(';')[0].split(':')[1],
                name: files[0].name,
                uri: ''
            }

            if (file.mediaType != 'application/pdf') {
                this.alertInvalidFileType();
                this.isFileLoading = false;
                return;
            }

            this.verify.setDocument(file, files[0].size);
            this.isSelected = true;
            let input = <any>document.getElementById("browseVerify");
            input.value = '';
            this.isFileLoading = false;
            //this.goVerify();
        }
        reader.readAsDataURL(files[0]);
    }

    /**
     * Call a program to view file
     */
    onView() {
        if (this.platform.is('android')) {
            this.filePath.resolveNativePath(this.verify.document.file.uri)
                .then(filePath => {
                    console.log(filePath);
                    this.fileOpener.open(filePath, 'application/pdf')
                        .then(() => console.log('File is opened'))
                        .catch(e => console.log('Error opening file', e));
                })
                .catch(err => console.log(err));
        }

        if (this.platform.is('ios')) {
            this.fileOpener.open(this.verify.document.file.uri, 'application/pdf')
                .then(() => console.log('File is opened'))
                .catch(e => console.log('Error opening file', e));
        }

        if (this.platform.is('desktop')) {
            let pdfWindow = window.open('');
            pdfWindow.document.write("<iframe width='100%' height='100%' src='" + encodeURI(this.verify.document.file.dataURI) + "'></iframe>")
            pdfWindow.document.close();
        }
    }

    /**
     * Launch enter hash alert
     */
    async onAddHash() {
        const alert = await this.alertCtrl.create({
            header: 'Please enter the sign transaction hash of the document',
            inputs: [
                {
                    name: 'hash',
                    type: 'text'
                }
            ],
            buttons: [
                {
                    text: 'Scan QR code',
                    handler: () => {
                        this.scanHashQrCode();
                    }
                },
                {
                    text: 'Ok',
                    handler: (data) => {
                        this.checkHash(data.hash);
                    }
                }
            ]
        });

        await alert.present();
    }

    /**
     * Cancel audit file
     */
    onCancel() {
        this.isSelected = false;
        const browseVerifyInput = <any>document.getElementById('browseVerify');
        browseVerifyInput.value = '';
        this.verify.clearDocument();
    }

    /**
     * Launch verify password
     */
    async alertInvalidHash() {
        const alert = await this.alertCtrl.create({
            header: 'Your transaction hash is invalid.',
            buttons: [
                {
                    text: 'Ok'
                }
            ]
        });
        await alert.present();
    }

    /**
     * Alert invalid file type
     */
    async alertInvalidFileType() {
        const alert = await this.alertCtrl.create({
            header: 'Invalid File Type',
            message: 'SirisuSign supports .pdf file only.',
            buttons: ['OK']
        });

        await alert.present();
    }

    /**
     * Valid the hash format
     * @param hash 
     */
    checkHash(hash: string) {
        if (hash.includes(' ') || (hash.length != 64)) this.alertInvalidHash();
        else this.verify.document.signTxHash = hash;
    }

    /**
     * Scan QR code to get hash
     */
    scanHashQrCode() {
        this.barcodeScanner.scan().then(barcodeData => {
            this.checkHash(barcodeData.text);
        }).catch(err => {
            console.log('Error', err);
        });
    }

    nowFromDate = HelperService.nowFromDate;

    loadData(event) {
        setTimeout(() => {
            console.log('Load more history');
            // if (!this.history.isLast) this.history.fetchTransactions();
            event.target.complete();
            // App logic to determine if all data is loaded
            // and disable the infinite scroll
            // if (this.history.docs.length == 1000) {
            //     event.target.disabled = true;
            // }
        }, 500);
    }

    /**
     * View document info
     * @param index
     */
    onDocument(index: number, flag: DocumentVerifyingStatus) {
        this.documentClassification.selectedDocInfo = this.documentClassification.getVerifyDocByIndex(index, flag);
        this.router.navigate(['sign-info']);
    }

    /**
     * Reload home and history
     * @async
     */
    async fetchFromChain() {
        // this.documentClassification.needSign = [];
        this.fetchAndUpdateNeedSignAndNeedVerifyFromChain();
        this.documentClassification.sortDocs('needSign');
        await this.documentClassification.fetchCompletedAndWaiting();
        this.documentClassification.sortDocs('compledAndWaiting');
        let storedDocs = await this.documentStorage.fetchDocument();
        const storedDocAccounts = storedDocs.map(doc => doc.documentAccount.publicKey);
        const allDocs = [
            ...this.documentClassification.completed,
            ...this.documentClassification.waiting,
            ...this.documentClassification.verified,
            ...this.documentClassification.verifying
        ];
        const updateDocStorage = async () => HelperService.asyncForEach(allDocs, async (doc: SiriusSignDocument) => {
            if (!storedDocAccounts.includes(doc.documentAccount.publicKey)) {
                await this.documentStorage.storeDocument(doc);
                return;
            }
            const focusedDoc = storedDocs[storedDocAccounts.indexOf(doc.documentAccount.publicKey)];
            if (focusedDoc.cosignatures.length != doc.cosignatures.length) {
                await this.documentStorage.updateDocument(focusedDoc.id, oldDoc => doc);
            }
        });
        await updateDocStorage();
    }

    /**
     * Fetch from storage
     */
    async fetchFromStorage() {
        this.documentClassification.clearDocs();
        const docs = await this.documentStorage.fetchDocument();
        console.log(this.documentClassification.needSign)
        if (docs) docs.forEach((doc, i) => this.documentClassification.classify(doc));
        this.documentClassification.sortDocs('all');
    }

    /**
     * Fetch need-sign document from chain to update local
     */
    async fetchAndUpdateNeedSignAndNeedVerifyFromChain() {
        const [newNeedSign, newNeedVerify] = await this.documentClassification.fetchNeedSignDocs();

        const oldNeedSign = this.documentClassification.needSign;
        const storeNeedSign = async () => HelperService.asyncForEach(newNeedSign, async (newDoc: SiriusSignDocument) => {
            const isStored = oldNeedSign.map(oldDoc => oldDoc.documentAccount.publicKey).includes(newDoc.documentAccount.publicKey);
            if (!isStored) await this.documentStorage.storeDocument(newDoc);
        });
        const updateNeedSign = async () => HelperService.asyncForEach(oldNeedSign, async (oldDoc: SiriusSignDocument) => {
            const isUpdated = !newNeedSign.map(newDoc => newDoc.documentAccount.publicKey).includes(oldDoc.documentAccount.publicKey);
            if (isUpdated) await this.documentStorage.removeDocument(oldDoc.id);
        });

        const oldNeedVerify = this.documentClassification.needVerify;
        const storeNeedVerify = async () => HelperService.asyncForEach(newNeedVerify, async (newDoc: SiriusSignDocument) => {
            const isStored = oldNeedVerify.map(oldDoc => oldDoc.documentAccount.publicKey).includes(newDoc.documentAccount.publicKey);
            if (!isStored) await this.documentStorage.storeDocument(newDoc);
        });
        const updateNeedVerify = async () => HelperService.asyncForEach(oldNeedVerify, async (oldDoc: SiriusSignDocument) => {
            const isUpdated = !newNeedVerify.map(newDoc => newDoc.documentAccount.publicKey).includes(oldDoc.documentAccount.publicKey);
            if (isUpdated) await this.documentStorage.removeDocument(oldDoc.id);
        });

        await updateNeedSign();
        await storeNeedSign();
        await storeNeedVerify();
        await updateNeedVerify();
        this.documentClassification.needSign = newNeedSign;
        this.documentClassification.needVerify = newNeedVerify;
    }

    /**
     * Refresher
     * @param event 
     */
    async doRefresh(event) {
        this.documentClassification.clearDocs();
        await this.fetchFromStorage();
        event.target.complete();
        await this.fetchFromChain();
    }
}
