import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import { IonicStorageModule } from '@ionic/storage';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { GlobalService } from '../global.service';
import { Device } from '@ionic-native/device/ngx';

describe('AuthGuardService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            IonicStorageModule.forRoot()
        ],
        providers: [
            Storage,
            { provide: Router, useClass: RouterTestingModule },
            GlobalService,
            Device
        ]
    }));

    it('should be created', () => {
        const service: AuthGuardService = TestBed.get(AuthGuardService);
        expect(service).toBeTruthy();
    });

    
});
