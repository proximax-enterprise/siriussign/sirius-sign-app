export enum NamePosition {
    NONE,
    TOP,
    BOTTOM,
    LEFT,
    RIGHT
}

export class SignatureCanvas {
    x: number;
    y: number;
    canvas: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;
    scale: number = 1;
    image: HTMLImageElement;
    offsetX: number;
    offsetY: number;
    canvasSize = {
        width: 0,
        height: 0
    }

    publicKey: string;
    id: number;
    name: string;
    isInit: boolean = false;
    isNamePlace: boolean = false;
    namePosition: NamePosition = NamePosition.NONE;
    pageNumber: number;
    isActive: boolean = false;

    constructor(publicKey: string, id: number, name: string) {
        this.publicKey = publicKey;
        this.id = id;
        this.name = name;
    }

    /**
     * Create canvas object and link to the canvas via html id
     * @param canvasId
     * @param imageObj
     * @param scale
     */
    create(canvasId: string, imageObj: HTMLImageElement, scale: number) {
        // Initial settings
        this.pageNumber = 1;
        this.x = 0;
        this.y = 0;
        this.scale = scale;

        // Canvas settings
        this.canvas = <HTMLCanvasElement>document.getElementById(canvasId);
        this.ctx = this.canvas.getContext('2d');

        this.canvas.width = this.canvasSize.width;
        this.canvas.height = this.canvasSize.height;

        this.ctx.scale(this.scale, this.scale);

        // Image setting
        this.image = imageObj
        this.calOffset();
    }

    /**
     * Fix scale
     */
    fixScale() {
        this.ctx.scale(this.scale, this.scale);
    }

    /**
     * Calculate offset of the canvas to screen
     */
    calOffset() {
        const rect = this.canvas.getBoundingClientRect();
        this.offsetX = rect.left;
        this.offsetY = rect.top;
    }


    /**
     * Get signature position
     */
    getPosition() {
        return {
            x: this.x,
            y: this.y
        }
    }

    /**
     * Compute the name bounding rect
     * @param namePosition
     */
    computeNameRect(namePosition: NamePosition) {
        switch (namePosition) {
            case NamePosition.TOP:
                return {
                    topLeftX: this.x,
                    topLeftY: this.y - 44,
                    bottomRightX: this.x + this.image.width,
                    bottomRightY: this.y
                }
            case NamePosition.BOTTOM:
                return {
                    topLeftX: this.x,
                    topLeftY: this.y + this.image.height,
                    bottomRightX: this.x + this.image.width,
                    bottomRightY: this.y + this.image.height + 44
                }
            case NamePosition.LEFT:
                return {
                    topLeftX: this.x - this.image.width,
                    topLeftY: this.y + this.image.height / 2 - 22,
                    bottomRightX: this.x,
                    bottomRightY: this.y + this.image.height / 2 + 22
                }
            case NamePosition.RIGHT:
                return {
                    topLeftX: this.x + this.image.width,
                    topLeftY: this.y + this.image.height / 2 - 22,
                    bottomRightX: this.x + 2 * this.image.width,
                    bottomRightY: this.y + this.image.height / 2 + 22
                }
            case NamePosition.NONE:
                return {
                    topLeftX: 0,
                    topLeftY: 0,
                    bottomRightX: 0,
                    bottomRightY: 0
                }
            default: throw new Error('Invalid Name Position');
        }
    }

    /**
     * Get the real touch position on full canvas from touch position on scaled canvas
     * @param x
     * @param y
     */
    computeTouchPosition(event) {
        this.calOffset();
        const dpr = window.devicePixelRatio;

        // Click or touch
        const touchedX = event.pageX ? event.pageX : event.touches[0].clientX;
        const touchedY = event.pageY ? event.pageY : event.touches[0].clientY;

        // Moved by canvas offset to page
        const pageX = touchedX - this.offsetX;
        const pageY = touchedY - this.offsetY;

        // Scaled by canvas view width
        const realX = (pageX / this.scale) * dpr;
        const realY = (pageY / this.scale) * dpr;
        return [realX, realY];
    }

    /**
     * Get the real touch position on full canvas from touch position on scaled canvas
     * @param x
     * @param y
     */
    computeTwoPointTouchPosition(event) {
        this.calOffset();
        const dpr = window.devicePixelRatio;

        // Click or touch
        const touchedX0 = event.touches[0].clientX;
        const touchedY0 = event.touches[0].clientY;
        const touchedX1 = event.touches[1].clientX;
        const touchedY1 = event.touches[1].clientY;

        // Moved by canvas offset to page
        const pageX0 = touchedX0 - this.offsetX;
        const pageY0 = touchedY0 - this.offsetY;
        const pageX1 = touchedX1 - this.offsetX;
        const pageY1 = touchedY1 - this.offsetY;

        // Scaled by canvas view width
        const realX0 = (pageX0 / this.scale) * dpr;
        const realY0 = (pageY0 / this.scale) * dpr;
        const realX1 = (pageX1 / this.scale) * dpr;
        const realY1 = (pageY1 / this.scale) * dpr;

        return [realX0, realY0, realX1, realY1];
    }

    /**
     * Get the real canvas size
     */
    computeCanvasSize() {
        return [this.canvas.width / this.scale, this.canvas.height / this.scale];
    }

    /**
     * Clear canvas
     */
    clearCanvas() {
        const [realCanvasW, realCanvasH] = this.computeCanvasSize();
        this.ctx.clearRect(0, 0, realCanvasW, realCanvasH);
    }

    /**
     * Draw the rounded rect
     * @param x
     * @param y
     * @param width
     * @param height
     * @param radius
     */
    drawRoundRect(x: number, y: number, width: number, height: number, radius: number) {
        if (width < 2 * radius) radius = width / 2;
        if (height < 2 * radius) radius = height / 2;
        this.ctx.beginPath();
        this.ctx.moveTo(x + radius, y);
        this.ctx.arcTo(x + width, y, x + width, y + height, radius);
        this.ctx.arcTo(x + width, y + height, x, y + height, radius);
        this.ctx.arcTo(x, y + height, x, y, radius);
        this.ctx.arcTo(x, y, x + width, y, radius);
        this.ctx.closePath();
        this.ctx.stroke();
        return this;
    }

    /**
     * Draw name by name position
     * @param namePosition
     * @param isButton
     */
    drawNameAtPosition(namePosition: NamePosition, isButton: boolean) {
        const rect = this.computeNameRect(namePosition);
        this.ctx.font = "14px Helvetica";
        if (namePosition == NamePosition.TOP || namePosition == NamePosition.BOTTOM) {
            this.ctx.textAlign = "center";
            this.ctx.fillText(this.name, (rect.topLeftX + rect.bottomRightX) / 2, rect.bottomRightY - 16);
        }
        else if (namePosition == NamePosition.RIGHT) {
            this.ctx.textAlign = "start";
            this.ctx.fillText(this.name, rect.topLeftX + 5, rect.bottomRightY - 16);
        }
        else if (namePosition == NamePosition.LEFT) {
            this.ctx.textAlign = "end";
            this.ctx.fillText(this.name, rect.bottomRightX - 5, rect.bottomRightY - 16);
        }

        if (isButton) this.drawRoundRect(rect.topLeftX, rect.topLeftY, (rect.bottomRightX - rect.topLeftX), (rect.bottomRightY - rect.topLeftY), 10);
    }

    /**
     * Draw name button
     */
    drawNameButton() {
        const isButton = true;
        this.ctx.fillStyle = "#979797";
        this.ctx.strokeStyle = "#979797";
        this.drawNameAtPosition(NamePosition.TOP, isButton);
        this.drawNameAtPosition(NamePosition.RIGHT, isButton);
        this.drawNameAtPosition(NamePosition.BOTTOM, isButton);
        this.drawNameAtPosition(NamePosition.LEFT, isButton);
    }

    /**
     * Draw placed name
     */
    drawName() {
        this.ctx.fillStyle = "#000000";
        const isButton = false;
        this.drawNameAtPosition(this.namePosition, isButton);
    }

    /**
     * Draw remove button
     */
    drawRemove() {
        // this.ctx.fillStyle = "#f53d3d";
        // this.ctx.strokeStyle = "#f53d3d";
        // this.drawRoundRect(this.x + this.image.width, this.y, 20, 20, 10);
    }

    /**
     * Draw selected box
     */
    drawSelected() {
        if (!this.isActive) return;
        this.ctx.fillStyle = "#3880ff";
        this.ctx.strokeStyle = "#3880ff";
        this.drawRoundRect(this.x, this.y, this.image.width, this.image.height, 10);
    }

    /**
     * Initiate signature
     * @param fnEndDrawAfterInit
     */
    init(pageNumber: number, fnEndDrawAfterInit, fnEndVoidAfterInit) {
        let isMoved = false;
        const startInit = (e) => {
            isMoved = false
            const move = (e) => {
                isMoved = true;
            }
            this.canvas.onmousemove = move;
            this.canvas.ontouchmove = move;
        }

        const endInit = (e) => {
            if (!isMoved) {
                this.isInit = true;
                this.pageNumber = pageNumber;
                this.canvas.width = this.canvasSize.width
                this.canvas.height = this.canvasSize.height

                const [pageX, pageY] = this.computeTouchPosition(e);
                this.x = pageX;
                this.y = pageY;

                this.drawSignature(fnEndDrawAfterInit, fnEndVoidAfterInit);
            }

            this.canvas.onmousemove = null;
            this.canvas.ontouchmove = null;
        }

        this.canvas.onmousedown = startInit;
        this.canvas.ontouchstart = startInit;

        this.canvas.onmouseup = endInit;
        this.canvas.ontouchend = endInit;
    }

    /**
     * Draw the interactive signature
     * @param fnFinish
     */
    drawSignature(fnFinish?, fnVoid?) {
        // init
        let start = {
            x: 0,
            y: 0
        };
        let end = {
            x: 0,
            y: 0
        };
        var completed = false;

        this.canvas.width = this.canvasSize.width
        this.canvas.height = this.canvasSize.height

        // default signature
        this.fixScale();
        this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
        this.drawRemove();
        this.drawSelected();
        if (!this.isNamePlace) this.drawNameButton();
        else this.drawName();
        if (fnFinish) fnFinish(this);

        let isNeedRedraw = false;
        let holdFunction;

        const startDraw = (e) => {
            if (!this.isActive) return;
            const [pageX, pageY] = this.computeTouchPosition(e);

            // console.log(this.offsetX, this.offsetY);
            // console.log(pageX, pageY);
            // console.log(this.x, this.y);
            // console.log(this.offsetX + this.x, this.offsetX + this.x + this.image.width, this.offsetY + this.y, this.offsetY + this.y + this.image.height);
            // console.log('-----');

            // Check if user start click on image
            if (
                pageX > this.x &&
                pageX < this.x + this.image.width &&
                pageY > this.y &&
                pageY < this.y + this.image.height
            ) {
                e.preventDefault();
                start.x = pageX;
                start.y = pageY;
                completed = true;

                holdFunction = setTimeout(() => {
                    console.log('Replace name')
                    if (this.isNamePlace) {
                        this.isNamePlace = false;
                        this.clearCanvas();
                        this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
                        this.drawRemove();
                        this.drawSelected();
                        this.drawNameButton();
                    }
                }, 1000);

                const moveDraw = (e) => {
                    clearTimeout(holdFunction);

                    const [pageX, pageY] = this.computeTouchPosition(e);
                    if (completed) {
                        end.x = pageX;
                        end.y = pageY;

                        const newX = this.x + (end.x - start.x);
                        const newY = this.y + (end.y - start.y);

                        this.x = newX >= 0 ? newX : 0;
                        this.y = newY >= 0 ? newY : 0;

                        const [realCanvasW, realCanvasH] = this.computeCanvasSize();
                        this.x = (newX + this.image.width) <= realCanvasW ? this.x : (realCanvasW - this.image.width);
                        this.y = (newY + this.image.height) <= realCanvasH ? this.y : (realCanvasH - this.image.height);

                        this.clearCanvas();
                        this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
                        this.drawRemove();
                        this.drawSelected();
                        start.x = end.x;
                        start.y = end.y;

                        if (this.isNamePlace) {
                            this.drawName();
                            isNeedRedraw = false;
                        }
                        else isNeedRedraw = true;
                    }
                }

                this.canvas.onmousemove = moveDraw;
                this.canvas.ontouchmove = moveDraw;
            }
            else {
                if (!this.isNamePlace) {
                    let flag = {
                        isNeedRedraw: isNeedRedraw,
                        completed: completed
                    }
                    this.selectNamePosition(pageX, pageY, e, start, flag);
                    isNeedRedraw = flag.isNeedRedraw;
                    completed = flag.completed;
                }
                if (fnVoid) fnVoid(pageX, pageY);
            }
        }

        const endDraw = () => {
            clearTimeout(holdFunction);
            if (isNeedRedraw) {
                if (!this.isNamePlace) this.drawNameButton();
                else this.drawName();
                isNeedRedraw = false;
            }
            if (fnFinish) fnFinish(this);
            completed = false;
            this.canvas.onmousemove = null;
            this.canvas.ontouchmove = null;
        }

        this.canvas.onmousedown = startDraw;
        this.canvas.ontouchstart = startDraw;

        this.canvas.onmouseup = endDraw;
        this.canvas.ontouchend = endDraw;
    }

    /**
     * Draw static signature
     * @param page
     */
    drawInactive(page: number, isDrawNameButton: boolean = false) {
        const [realCanvasW, realCanvasH] = this.computeCanvasSize();
        if (page != this.pageNumber) this.clearCanvas();
        else {
            this.clearCanvas();
            setTimeout(() => {
                this.ctx.beginPath();
                this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
                this.drawSelected();
                this.ctx.closePath();
            }, 100);
            if (this.isNamePlace) this.drawName();
            else if (isDrawNameButton) this.drawNameButton();
        }
    }

    /**
     * Draw static signature with appearing animation
     * @param page
     */
    drawInactiveWithAnimation(page: number) {
        const [realCanvasW, realCanvasH] = this.computeCanvasSize();
        if (page != this.pageNumber) this.clearCanvas();
        else {
            let width = 0;
            let height = 0;
            const draw = () => {
                this.clearCanvas();
                this.ctx.beginPath();
                this.ctx.drawImage(this.image, this.x, this.y, width, height);
                this.ctx.closePath();
                if (this.isNamePlace) this.drawName();
                if (width < this.image.width || height < this.image.height) {
                    width += 10;
                    height = (width / this.image.width) * this.image.height;
                    requestAnimationFrame(() => draw());
                }
            }
            requestAnimationFrame(() => draw());
            if (this.isNamePlace) this.drawName();
        }
    }

    /**
     * Check if name button is clicked, select name position by name position
     * @param pageX
     * @param pageY
     * @param e
     * @param namePosition
     * @param flag
     */
    checkAndSelectNamePosition(
        pageX: number,
        pageY: number,
        e,
        namePosition: NamePosition,
        flag: { isNeedRedraw: boolean, completed: boolean }
    ) {
        const nameRect = this.computeNameRect(namePosition);
        if (
            pageX > nameRect.topLeftX &&
            pageX < nameRect.bottomRightX &&
            pageY > nameRect.topLeftY &&
            pageY < nameRect.bottomRightY
        ) {
            e.preventDefault();

            this.namePosition = namePosition;
            this.isNamePlace = true;

            this.clearCanvas();
            this.ctx.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
            this.drawRemove();
            this.drawSelected();

            flag.isNeedRedraw = true;
            flag.completed = true;
        }
    }

    /**
     * Check if name button is clicked, select name position for all name positions
     * @param pageX 
     * @param pageY 
     * @param e 
     * @param start 
     * @param flag 
     */
    selectNamePosition(pageX: number, pageY: number, e, start: { x: number, y: number }, flag: { isNeedRedraw: boolean, completed: boolean }) {
        this.checkAndSelectNamePosition(pageX, pageY, e, NamePosition.TOP, flag);
        this.checkAndSelectNamePosition(pageX, pageY, e, NamePosition.BOTTOM, flag);
        this.checkAndSelectNamePosition(pageX, pageY, e, NamePosition.LEFT, flag);
        this.checkAndSelectNamePosition(pageX, pageY, e, NamePosition.RIGHT, flag);
    }

    /**
     * Check if sign here button is clicked
     * @param fnFinish
     */
    onSignHere(pageNumber, fnFinish?, fnVoid?) {
        let onSignHere = false;
        const startDraw = (e) => {
            const [pageX, pageY] = this.computeTouchPosition(e);

            // Check if user start click on image
            if (
                pageNumber == this.pageNumber &&
                pageX > this.x &&
                pageX < this.x + this.image.width &&
                pageY > this.y &&
                pageY < this.y + this.image.height
            ) {
                e.preventDefault();
                onSignHere = true;
            }
            else {
                if (fnVoid) fnVoid(pageX, pageY);
            }
        }

        const endDraw = () => {
            if (fnFinish && onSignHere) {
                fnFinish(this);
                onSignHere = false;
            }
        }

        this.canvas.onmousedown = startDraw;
        this.canvas.ontouchstart = startDraw;

        this.canvas.onmouseup = endDraw;
        this.canvas.ontouchend = endDraw;
    }

    /**
     * Check if an aligned position is inside this signature
     * @param x
     * @param y
     */
    checkSelected(x: number, y: number, page: number) {
        return (
            page == this.pageNumber &&
            x > this.x &&
            x < this.x + this.image.width &&
            y > this.y &&
            y < this.y + this.image.height
        )
    }
}
