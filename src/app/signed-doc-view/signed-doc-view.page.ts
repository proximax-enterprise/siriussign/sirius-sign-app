import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
import * as pdfjsLib from 'pdfjs-dist';
import * as pdfWorker from 'pdfjs-dist/build/pdf.worker';
import * as PDF from 'pdf-lib';
import { rgb } from 'pdf-lib';

import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';

import { SignatureCanvas, NamePosition } from '../model/signature-canvas.model';

import { HelperService } from '../services/helper.service';
import { DrawSignatureService, SignatureInfo } from '../services/draw-signature.service';
import { GlobalService } from './../services/global.service';
import { DocumentStorageService } from '../services/document-strorage.service';
import { DocumentClassificationService } from '../services/document-classification.service';
import { SiriusSignDocument } from '../model/siriussign-document.model';

@Component({
    selector: 'app-signed-doc-view',
    templateUrl: './signed-doc-view.page.html',
    styleUrls: ['./signed-doc-view.page.scss'],
})
export class SignedDocViewPage {

    signatureImgObj: HTMLImageElement = document.createElement('img');

    pdfDoc = null;
    docUint8Array: Uint8Array;
    pageNum: number = 1;
    pageRendering: boolean = false;
    pageNumPending: number = null;
    scale: number = 1;
    pdfCanvas: HTMLCanvasElement;
    pdfCtx: CanvasRenderingContext2D;

    signaturesInfo: SignatureInfo[];
    signerCanvases: SignatureCanvas[][] = [];
    selectedSignatureIndex: number = 1;
    selectedSignerIndex: number = 0;

    isInit: boolean = false;
    isProcessing: boolean = false;

    constructor(
        private platform: Platform,
        private file: File,
        private fileOpener: FileOpener,
        private drawSignature: DrawSignatureService,
        private global: GlobalService,
        private documentStorage: DocumentStorageService,
        private documentClassification: DocumentClassificationService
    ) {
        // The workerSrc property shall be specified.
        pdfjsLib.GlobalWorkerOptions.workerSrc = pdfWorker;
    }

    ngOnDestroy() {
    }

    async ionViewWillEnter() {
        document.documentElement.style.setProperty('--opacity-container', '0');
        await this.initView();
        setTimeout(() => {
            document.documentElement.style.setProperty('--opacity-container', '1');
        }, 50);
    }

    async initView() {
        document.documentElement.style.setProperty('--width-container', '595px');
        this.signaturesInfo = this.drawSignature.signers;
        this.selectedSignatureIndex = this.drawSignature.selectedSignerIndex;
        this.isInit = true;

        this.pdfCanvas = <HTMLCanvasElement>document.getElementById('the-pdf');
        this.pdfCtx = this.pdfCanvas.getContext('2d');

        // Device Pixel Ratio
        const dpr = window.devicePixelRatio;

        // Set pdf
        this.docUint8Array = HelperService.convertDataURIToBinary(this.drawSignature.pdfUri);
        this.pdfDoc = await pdfjsLib.getDocument(this.docUint8Array).promise;

        document.getElementById('page_count').innerHTML = this.pdfDoc.numPages;
        document.getElementById('prev').addEventListener('click', () => this.onPrevPage());
        document.getElementById('next').addEventListener('click', () => this.onNextPage());

        // Initial/first page rendering
        await this.renderPage(this.pageNum);

        // Generate signature canvases
        this.signaturesInfo.forEach((signatureInfo, idxSigner) => {
            this.signerCanvases[idxSigner] = [];
            signatureInfo.signaturePositions.forEach((signaturePosition, idxSignature) => {
                this.signerCanvases[idxSigner].push(new SignatureCanvas(signatureInfo.publicKey, idxSignature, signatureInfo.signaturePositions[idxSignature].name));
            });
        });

        const signatureImgPublicKeys = this.drawSignature.signatureImgs.map(imgInfo => imgInfo.publicKey);
        const createCanvases = async () => HelperService.asyncForEach(this.signerCanvases, async (signautrPosition: SignatureCanvas[], idxSigner: number) => {
            const subCreateCanvases = async () => HelperService.asyncForEach(signautrPosition, async (canvas: SignatureCanvas, idxSignature: number) => {
                canvas.canvasSize.width = this.pdfCanvas.width;
                canvas.canvasSize.height = this.pdfCanvas.height;

                // Check and get signed signature image
                const indexOfImg = signatureImgPublicKeys.indexOf(canvas.publicKey);
                const signatureImgSrc = await HelperService.getPngSignature(this.drawSignature.signatureImgs[indexOfImg].signatureImg);
                const otherSignatureImgObj: HTMLImageElement = document.createElement('img');
                otherSignatureImgObj.src = signatureImgSrc;
                otherSignatureImgObj.height = 88;
                otherSignatureImgObj.width = 188;
                // Setup canvas
                canvas.create('the-signature-' + idxSigner + '-' + idxSignature, otherSignatureImgObj, this.scale * dpr);
                const idxOfSigner = this.drawSignature.signers.map(signer => signer.publicKey).indexOf(canvas.publicKey);
                const idxOfPosition = canvas.id;
                canvas.pageNumber = this.drawSignature.signers[idxOfSigner].signaturePositions[idxOfPosition].page;
                canvas.x = this.drawSignature.signers[idxOfSigner].signaturePositions[idxOfPosition].x;
                canvas.y = this.drawSignature.signers[idxOfSigner].signaturePositions[idxOfPosition].y;
                canvas.namePosition = this.drawSignature.signers[idxOfSigner].signaturePositions[idxOfPosition].namePosition;
                canvas.isNamePlace = canvas.namePosition != NamePosition.NONE;
                canvas.isInit = true;

                if (!(this.global.isBrowser) || (dpr != 1)) {
                    const canvasElement = document.getElementById('the-signature-' + idxSigner + '-' + idxSignature);
                    canvasElement.style.width = '100%';
                }
            });
            await subCreateCanvases();
        });
        await createCanvases();

        this.signerCanvases.forEach((signatureCanvases, idxSigner) => {
            signatureCanvases.forEach((canvas, idxSignature) => {
                // Draw signature image
                canvas.drawInactive(this.pageNum);

                // Set z-index for canvas
                const canvasElement = document.getElementById('the-signature-' + idxSigner + '-' + idxSignature);
                canvasElement.style.zIndex = (idxSigner == this.selectedSignerIndex) ? '2' : '1';
            });
        });

        // document.getElementById('download').addEventListener('click', () => { this.run(); });

        // document.getElementById('apply').addEventListener('click', () => {
        //     const heightSign = <HTMLInputElement>document.getElementById('heightSign');
        //     const widthSign = <HTMLInputElement>document.getElementById('widthSign');
        //     this.imgObj.height = parseInt(heightSign.value);
        //     this.imgObj.width = parseInt(widthSign.value);
        //     this.signCanvases[this.selectedSignatureIndex].drawSignature();
        // });

        this.drawSignature.isInit = true;
    }

    /**
    * Get page info from document, resize canvas accordingly, and render page.
    * @param num Page number.
    */
    async renderPage(num) {
        this.pageRendering = true;
        // Using promise to fetch the page
        const page = await this.pdfDoc.getPage(num)

        // const pdfScreenRatio = window.innerWidth / page.view[2];
        // this.scale = pdfScreenRatio > 1 ? 1 : pdfScreenRatio;
        if (this.scale == 1) document.documentElement.style.setProperty('--width-container', page.view[2] + 'px');

        // Scale up for crystal clear image
        const dpr = window.devicePixelRatio;
        var viewport = page.getViewport({ scale: this.scale * dpr });
        this.pdfCanvas.height = viewport.height;
        this.pdfCanvas.width = viewport.width;

        // Scale down for right scale ratio
        const canvas = document.getElementById(this.pdfCanvas.id);
        if ((!this.global.isBrowser) || (dpr != 1))
            canvas.style.width = '100%';

        // Render PDF page into canvas context
        var renderContext = {
            canvasContext: this.pdfCtx,
            viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(() => {
            this.pageRendering = false;
            if (this.pageNumPending !== null) {
                // New page rendering is pending
                this.renderPage(this.pageNumPending);
                this.pageNumPending = null;
            }
        });


        // Update page counters
        document.getElementById('page_num').textContent = num;

        // Update signature
        this.signerCanvases.forEach((signatureCanvases, idxSigner) => {
            signatureCanvases.forEach((canvas, idxSignature) => {
                canvas.drawInactive(this.pageNum)
            });
        });
    }

    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    queueRenderPage(num) {
        if (this.pageRendering) {
            this.pageNumPending = num;
        } else {
            this.renderPage(num);
        }
    }

    /**
     * Displays previous page.
     */
    onPrevPage() {
        if (this.pageNum <= 1) {
            return;
        }
        this.pageNum--;
        this.queueRenderPage(this.pageNum);
    }


    /**
     * Displays next page.
     */
    onNextPage() {
        if (this.pageNum >= this.pdfDoc.numPages) {
            return;
        }
        this.pageNum++;
        this.queueRenderPage(this.pageNum);
    }

    async export() {
        this.isProcessing = true;
        // Load
        const pdfDoc = await PDF.PDFDocument.load(this.docUint8Array);
        const pages = pdfDoc.getPages();
        // const canvasPngImage = await pdfDoc.embedPng(canvasImageBuffer);
        const dpr = window.devicePixelRatio;

        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(PDF.StandardFonts.Helvetica);

        // Draw
        const embedImg = async () => HelperService.asyncForEach(this.signerCanvases, async (signautrPosition: SignatureCanvas[], idxSigner: number) => {
            const subEmbedImg = async () => HelperService.asyncForEach(signautrPosition, async (signCanvas: SignatureCanvas, idxSignature: number) => {
                const embedPage = pages[signCanvas.pageNumber - 1];
                const { width, height } = embedPage.getSize();
                embedPage.setFont(helveticaFont);
                embedPage.setFontSize(14);  //px

                if (signCanvas.isNamePlace) {
                    const namePosition = signCanvas.namePosition;
                    const nameRect = signCanvas.computeNameRect(namePosition);

                    const isNameCenter = (namePosition == NamePosition.TOP) || (namePosition == NamePosition.BOTTOM);
                    let nameX = nameRect.topLeftX;
                    const nameY = height / dpr - nameRect.bottomRightY + 16;

                    if (isNameCenter) {
                        this.pdfCtx.font = "14px Helvetica";
                        const textMetric = this.pdfCtx.measureText(signCanvas.name);
                        const centerNameX = (nameRect.topLeftX + nameRect.bottomRightX) / 2 - textMetric.width / 2;
                        nameX = centerNameX;
                    }

                    if (namePosition == NamePosition.LEFT) {
                        this.pdfCtx.font = "14px Helvetica";
                        const textMetric = this.pdfCtx.measureText(signCanvas.name);
                        const rightAlignNameX = nameRect.bottomRightX - textMetric.width - 5;
                        nameX = rightAlignNameX;
                    }
                    embedPage.drawText(signCanvas.name, {
                        x: nameX,
                        y: nameY,
                        color: rgb(0, 0, 0),
                        size: 14
                    });
                }

                const signatureImgBuffer = HelperService.convertDataURIToBinary(signCanvas.image.src);
                const signaturePngImg = await pdfDoc.embedPng(signatureImgBuffer);

                const position = signCanvas.getPosition();
                const embedImg = signaturePngImg;
                embedPage.drawImage(embedImg, {
                    x: position.x,
                    y: height - position.y - signCanvas.image.height,
                    height: signCanvas.image.height,
                    width: signCanvas.image.width
                });
            });
            await subEmbedImg();
        });
        await embedImg();

        // Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save();


        // var blob = new Blob([pdfBytes], { type: "application/pdf" });
        // var link = document.createElement("a");
        // link.href = window.URL.createObjectURL(blob);
        // link.download = "output.pdf";
        // link.click();

        const signedDocBlob = new Blob([pdfBytes], { type: "application/pdf" });
        const data = window.URL.createObjectURL(signedDocBlob);

        const fileName = this.drawSignature.name.substring(0, this.drawSignature.name.lastIndexOf('.'));
        const fileType = this.drawSignature.name.substr(this.drawSignature.name.lastIndexOf('.'));
        const writeFileName = fileName + '-signed' + fileType;
        console.log(writeFileName);
        this.createDocumentView(writeFileName, data, signedDocBlob);
        this.isProcessing = false;
    }

    /**
     * Create HTML web view or write file and call opner on mobile to view pdf
     */
    createDocumentView(writeFileName: string, dataUri: string, blob: Blob) {
        if (this.platform.is('desktop')) {
            let pdfWindow = window.open('');
            setTimeout(() => {
                pdfWindow.document.write(`
                            <!DOCTYPE html>
                            <html>
                            <head>
                            <style>
                            .button {
                                background-color: blue;
                                border: none;
                                color: white;
                                padding: 5px 15px;
                                margin-right: 10px;
                                text-align: center;
                                font-size: 14px;
                                cursor: pointer;
                            }

                            .button:hover {
                                background-color: #4CAF50;
                            }

                            a {
                                margin-bottom: 15px;
                                text-decoration: none;
                            }

                            iframe {
                                margin-top: 10px;
                                height: 90vh;
                            }
                            </style>
                            </head>
                            <body>
                            <a 
                                download='` + writeFileName + `'
                                href='` + dataUri + `' >
                                    <button class="button">Download</button>` + writeFileName + ` 
                            </a>
                            <iframe 
                                width='100%' height='100%' 
                                src='` + dataUri + `'></iframe>
                            </body>
                            </html>
                        `);
                // pdfWindow.document.write('<html><body><object width="100%" height="100%" data="data:application/pdf;base64,' + encodeURI(data) + '" type = "application/pdf" ><embed src="data:application/pdf;base64,' + encodeURI(data) + '" type = "application/pdf" /></object></body></html>');
                pdfWindow.document.title = writeFileName;
            }, 100);
            pdfWindow.document.close();

        }
        else {
            let path = '';
            if (this.platform.is('ios')) path = this.file.documentsDirectory;
            else if (this.platform.is('android')) path = this.file.externalDataDirectory;
            const option = {
                replace: true
            }
            this.file.checkFile(path, writeFileName)
                .then(isExist => { if (isExist) console.log('File is exist') });

            this.file.writeFile(path, writeFileName, blob, option)
                .then(async res => {
                    console.log(res);
                    this.documentClassification.selectedDocInfo.localUrl = res.nativeURL;
                    await this.documentStorage.updateDocument(this.documentClassification.selectedDocInfo.id, (oldDoc: SiriusSignDocument) => {
                        const newDoc = oldDoc;
                        newDoc.localUrl = res.nativeURL;
                        return newDoc;
                    });
                    this.fileOpener.open(res.nativeURL, 'application/pdf')
                        .then(() => console.log('File is opened'))
                        .catch(err => console.log('Error opening file', err));
                })
                .catch(err => console.log(err));
        }
    }
}
