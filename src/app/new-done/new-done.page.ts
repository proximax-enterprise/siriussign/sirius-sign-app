import { Component } from '@angular/core';
import { Platform, ToastController, NavController } from '@ionic/angular';
import { HelperService } from '../services/helper.service';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import * as ClipboardJS from 'clipboard/dist/clipboard.min.js';

import { SiriusSignSigningTask } from './../model/siriussign-task.model';

import { SignDocumentService } from './../services/sign-document.service';
import { DocumentClassificationService } from '../services/document-classification.service';
import { DocumentStorageService } from './../services/document-strorage.service';
import { MultitaskService } from './../services/multitask.service';

@Component({
    selector: 'app-new-done',
    templateUrl: './new-done.page.html',
    styleUrls: ['./new-done.page.scss'],
})
export class NewDonePage {

    file = {
        name: '',
        size: 'unknow',
        uploadDate: '',
        hashFunc: 'SHA256',
        storage: 'Public',
        fee: 'No fee',
        status: '',
        owner: '',
        fileHash: '',
        transactionHash: '',
        transactionHashQr: ''
    };

    isSuccess = true;

    uniLink = '';

    constructor(
        private navCtrl: NavController,
        private toastController: ToastController,
        private platform: Platform,
        private socialSharing: SocialSharing,
        private clipboard: Clipboard,
        public signDoc: SignDocumentService,
        private documentClassification: DocumentClassificationService,
        private documentStorage: DocumentStorageService,
        private multitask: MultitaskService
    ) { }

    async ionViewWillEnter() {
        console.log(this.signDoc.document);
        const selectedTask          = <SiriusSignSigningTask>this.multitask.selectedTask;
        this.file.name              = selectedTask.document.file.name;
        this.file.uploadDate        = HelperService.dateToShortString(selectedTask.document.signDate);
        this.file.storage           = selectedTask.document.isEncrypt ? 'Private' : 'Public';
        this.file.owner             = selectedTask.account.address.pretty();
        this.file.fileHash          = selectedTask.document.fileHash;
        this.file.transactionHash   = selectedTask.document.signTxHash;
        this.file.transactionHashQr = HelperService.getQr(this.file.transactionHash, 4);
        this.file.status            = selectedTask.document.status;
        this.isSuccess              = (this.file.status == 'Confirmed') || (this.file.status == 'Waiting for cosignatures');
        this.uniLink                = this.getUniLink();
        var clipboardjs = new ClipboardJS('.btnCopy');
        clipboardjs.on('success', (event: ClipboardJS.Event) => {
            setTimeout(() => { event.clearSelection() }, 200);
        });
    }

    /*
     * Naviagte to home
     */
    goHome() {
        // this.reloadAll();
        const taskIndex = this.multitask.tasks.indexOf(this.multitask.selectedTask);
        this.multitask.tasks.splice(taskIndex, 1);
        this.multitask.selectedTask = null;
        this.navCtrl.navigateRoot('app/tabs/tab-menu/home');
    }

    /**
     * Fetch transaction to get uniLink
     */
    getUniLink() {
        const docAccountPublicKey = (<SiriusSignSigningTask>this.multitask.selectedTask).document.documentAcc.publicKey;
        const uniLink = 'siriussign://siriussign.com/app/sign-info/' + docAccountPublicKey;
        return uniLink;
    }

    /**
     * Invaite cosigner to sign via social apps
     */
    regularShare() {
        let message = 'I invite you to be a cosigner of ' + this.file.name + '. Please open this link in your web browser to sign it.\n';
        this.socialSharing.share(message, 'SiriusSign cosign invitation', null, this.uniLink);
        if (this.platform.is('desktop')) alert('Copied cosigner invitation link!\n' + this.uniLink);
    }

    /**
     * Display toast that inform copied message
     */
    async presentToastCopied() {
        const toast = await this.toastController.create({
            message: 'Copied!',
            duration: 1000,
            translucent: true
        });
        toast.present();
    }

    /**
     * Copy transaction hash using cordova plugin
     */
    copyTxHash() {
        this.clipboard.copy(this.file.transactionHash);
        this.presentToastCopied();
    }
}
