import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Transaction } from 'tsjs-xpx-chain-sdk';

import { GlobalService } from './../services/global.service';
import { VerifyService } from './../services/verify.service';
import { DocumentClassificationService } from '../services/document-classification.service';
import { SigningWithoutMultisigService } from '../services/signing-without-multisig.service';
import { MonitorService } from './../services/monitor.service';

@Component({
    selector: 'app-verify-progress',
    templateUrl: './verify-progress.page.html',
    styleUrls: ['./verify-progress.page.scss'],
})

export class VerifyProgressPage implements OnInit {
    percent = 64;
    isDone = false;

    constructor(
        private router: Router,
        private global: GlobalService,
        private verify: VerifyService,
        private documentClassification: DocumentClassificationService,
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private monitor: MonitorService
    ) { }

    ngOnInit() {
        this.percent = 0;
        if (this.verify.isRealVerify) this.incVerify();
        else this.incAudit();
    }

    /**
     * Calculate stroke-dashoffset value from percent
     * @returns
     */
    calStrokeDashOffset() {
        let c = Math.PI * (60 * 2);

        if (this.percent < 0) { this.percent = 0; }
        if (this.percent > 100) { this.percent = 100; }

        let offset = ((100 - this.percent) / 100) * c;
        return offset + 190;
    }

    /**
     * Increase the value of percent until progress done
     * Status: temporarily use to navigate to next ui in sign new doc process
     * @async
     */
    async incPercent() {
        await new Promise((resolve, reject) => {
            let func = setInterval(() => {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(func);
                    resolve('done');
                }
            }, 100)
        }).then(() => {
            this.global.setIsProgressDone(true);
            this.router.navigate(['verify-done']);
        });
    }

    /**
     * Navigate to verify result
     */
    goDone() {
        this.global.setIsProgressDone(true);
        this.router.navigate(['verify-done']);
    }

    /**
     * Lauch audit progress
     */
    async incAudit() {
        const incProgress = setInterval(() => {
            if (this.percent < 75) this.percent += 1;
            if (this.isDone) clearInterval(incProgress);
        }, 100);

        const toDone = setInterval(() => {
            if (this.isDone) {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(toDone);
                    this.goDone();
                }
            }
        }, 40);

        const doc = await this.documentClassification.fetchDocumentBySigningTxHash(this.verify.document.signTxHash);
        this.verify.docResult = doc;
        if (doc) {
            const isValid = this.verify.document.fileHash == doc.fileHash.sha256Hash;
            this.verify.auditResult.isValid = isValid;
            if (!isValid)
                this.verify.auditResult.message = 1;
            this.isDone = true;
        }
        else {
            this.verify.auditResult.isValid = false;
            this.verify.auditResult.message = 2;
            this.isDone = true;
        }
    }

    /**
     * Launch verify progress
     */
    async incVerify() {
        const incProgress = setInterval(() => {
            if (this.percent < 75) this.percent += 1;
            if (this.isDone) clearInterval(incProgress);
        }, 250);

        const toDone = setInterval(() => {
            if (this.isDone) {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(toDone);
                    this.goDone();
                }
            }
        }, 40);

        const signedVerifyTx = this.verify.createDocVerifyTransaction(this.documentClassification.selectedDocInfo)
        const docVerifyTx: Transaction = await this.monitor.waitForTransactionConfirmed(this.global.loggedAccount.address, signedVerifyTx.hash, () => {
            this.signingWithoutMultisig.announceTransaction(signedVerifyTx);
        }).catch(err => {
            this.verify.verifyResult.status = err.message;
            return null;
        });

        const doc = await this.documentClassification.fetchDocumentByDocumentAccount(this.documentClassification.selectedDocInfo.documentAccount.publicKey);
        this.verify.docResult = doc;
        if (docVerifyTx) {
            this.verify.verifyResult.status = 'Verified';
            this.verify.verifyResult.isValid = true;
            this.isDone = true;
        }
        else {
            this.verify.verifyResult.isValid = false;
            this.isDone = true;
        }
    }
}
