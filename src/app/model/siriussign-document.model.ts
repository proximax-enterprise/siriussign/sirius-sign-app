import { PublicAccount } from 'tsjs-xpx-chain-sdk';
import { SignaturePosition } from './../services/draw-signature.service';

export interface SignerSignature {
    publicKey: string,
    signaturePositions: SignaturePosition[]
    signatureUploadTxHash: string
}

/**
 * SiriusDocument is a model of signing document used in SiriusSign.
 * It contains file and its notarization transaction infomation.
 */
export class SiriusSignDocument {
    /** Document ID (= transaction ID) */
    id: string;
    /** File name */
    name: string;
    /** File hash */
    fileHash: {
        sha256Hash: string,
        md5Hash: string
    }
    /** File upload and sign date */
    uploadDate: Date;
    /** File owner address, who sign this document first */
    owner: string;
    /**
     * Sign status of current account
     */
    isSigned: boolean;
    /** Signing date */
    signDate: Date;
    /** Document cosigners */
    cosigners: string[];
    cosignatures: string[];
    /** Document Account that file belongs to */
    documentAccount: PublicAccount;
    /** Sign transaction hash */
    signTxHash: string;
    /** Upload transaction hash */
    uploadTxHash: string;
    /** File is encrypt */
    isEncrypt: boolean
    /** Signature images information */
    // signatures: SignatureInfo[];
    // signaturesUploadTxHash: string[];
    signerSignatures: SignerSignature[];
    /** Document cosigners */
    verifiers: string[];
    verifierSignatures: string[];
    /** Local url */
    localUrl: string;

    constructor() {
        this.id = null;
        this.name = null;
        this.fileHash = {
            sha256Hash: null,
            md5Hash: null
        }
        this.uploadDate = null;
        this.owner = null;
        this.isSigned = null;
        this.signDate = null;
        this.cosigners = [];
        this.cosignatures = [];
        this.documentAccount = null;
        this.signTxHash = null;
        this.uploadTxHash = null;
        this.isEncrypt = null;
        // this.signatures = [];
        // this.signaturesUploadTxHash = [];
        this.signerSignatures = [];
        this.verifiers = [];
        this.verifierSignatures = [];
        this.localUrl = null;
    }

    /**
     * Create a SiriusDocument from document info
     * @param name
     * @param fileHash
     * @param uploadDate
     * @param status
     * @param owner
     * @param isSigned
     * @param cosigners
     * @param transactionHash
     */
    static create(
        id: string,
        name: string,
        fileHash: string,
        uploadDate: Date,
        owner: string,
        isSigned: boolean,
        signDate: Date,
        cosigners: string[],
        cosignatures: string[],
        documentAccount: PublicAccount,
        signTxHash: string,
        uploadTxHash: string,
        isEncrypt: boolean,
        // signatures: SignatureInfo[],
        // signaturesUploadTxHash: string[],
        signerSignatures: SignerSignature[],
        verifiers: string[],
        verifierSignatures: string[],
        localUrl: string
    ) {
        let doc = new SiriusSignDocument();
        doc.id = id;    // transactionHash.substr(transactionHash.length-5);
        doc.name = name;
        doc.fileHash = {
            sha256Hash: fileHash,
            md5Hash: null
        }
        doc.uploadDate = uploadDate;
        doc.owner = owner;
        doc.isSigned = isSigned;
        doc.signDate = signDate;
        doc.cosigners = cosigners;
        doc.cosignatures = cosignatures;
        doc.documentAccount = documentAccount;
        doc.signTxHash = signTxHash;
        doc.uploadTxHash = uploadTxHash;
        doc.isEncrypt = isEncrypt;
        // doc.signatures = signatures;
        // doc.signaturesUploadTxHash = signaturesUploadTxHash;
        doc.signerSignatures = signerSignatures;
        doc.verifiers = verifiers;
        doc.verifierSignatures = verifierSignatures;
        doc.localUrl = localUrl;
        return doc;
    }
}
