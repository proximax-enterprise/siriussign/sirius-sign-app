import { Component } from '@angular/core';
import { trigger, style, transition, animate } from '@angular/animations';
import { ToastController, ActionSheetController, NavController, Platform } from '@ionic/angular';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
import * as pdfjsLib from 'pdfjs-dist';
import * as pdfWorker from 'pdfjs-dist/build/pdf.worker';
import * as PDF from 'pdf-lib';

import { SignatureCanvas, NamePosition } from '../model/signature-canvas.model';

import { HelperService } from '../services/helper.service';
import { DrawSignatureService, SignatureInfo } from '../services/draw-signature.service';
import { SignatureService } from './../services/signature.service';
import { GlobalService } from './../services/global.service';
import { MultitaskService } from '../services/multitask.service';
import { SigningWithoutMultisigService } from '../services/signing-without-multisig.service';
import { SiriusSignSigningTask } from '../model/siriussign-task.model';
import { MonitorService } from '../services/monitor.service';
import { UploadStorageService } from '../services/upload-storage.service';
import { SignDocumentService } from '../services/sign-document.service';
import { DocumentClassificationService } from '../services/document-classification.service';
import { DocumentStorageService } from '../services/document-strorage.service';

@Component({
    selector: 'app-new-interactive',
    templateUrl: 'new-interactive.page.html',
    styleUrls: ['new-interactive.page.scss'],
    animations: [
        trigger(
            'inOutAnimation',
            [
                transition(
                    ':leave',
                    [
                        style({ opacity: 1 }),
                        animate('0.3s ease-in',
                            style({ height: 0, opacity: 0 }))
                    ]
                )
            ]
        )
    ]
})
export class NewInteractivePage {

    signatureImgObj: HTMLImageElement = document.createElement('img');

    imgSrc: string = '/assets/sign-here-xl.png';
    imgObj: HTMLImageElement = document.createElement('img');

    inactiveImgSrc: string = '/assets/sign-here-inactive-xl.png';
    inactiveImgObj: HTMLImageElement = document.createElement('img');

    pdfDoc = null;
    docUint8Array: Uint8Array;
    pageNum: number = 1;
    pageRendering: boolean = false;
    pageNumPending: number = null;
    scale: number = 1;
    pdfCanvas: HTMLCanvasElement;
    dpr: number = 1;
    pdfCtx: CanvasRenderingContext2D;

    signaturesInfo: SignatureInfo[];
    signerCanvases: SignatureCanvas[][] = [];
    selectedSignatureIndex: number = 1;
    selectedSignerIndex: number = 0;
    doneCanvas: SignatureCanvas;

    isInit: boolean = false;
    isPutAllSignatures: boolean = false;
    isMultiSign: boolean = true;
    isDone: boolean = false;
    backLink: string;
    isZooming: boolean = false;
    isFinishClicked: boolean = false;

    constructor(
        private toastController: ToastController,
        public actionSheetController: ActionSheetController,
        private navCtrl: NavController,
        private platform: Platform,
        private drawSignature: DrawSignatureService,
        private signature: SignatureService,
        private global: GlobalService,
        public multitask: MultitaskService,
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private monitor: MonitorService,
        private uploadStorage: UploadStorageService,
        private signDoc: SignDocumentService,
        private documentClassification: DocumentClassificationService,
        private documentStorage: DocumentStorageService
    ) {
        // The workerSrc property shall be specified.
        pdfjsLib.GlobalWorkerOptions.workerSrc = pdfWorker;
        document.documentElement.style.setProperty('--width-container', '595px');

        this.signaturesInfo = this.drawSignature.signers;
        this.selectedSignatureIndex = this.drawSignature.selectedSignerIndex;
        this.isMultiSign = this.signDoc.document.cosigners.length > 0;
        this.backLink = this.isMultiSign ? 'new-multi-sign' : 'new-only-me';
        this.dpr = window.devicePixelRatio; // Device Pixel Ratio
    }

    ngOnDestroy() {
        console.log('Destroy new-interactive');
    }

    async ionViewWillEnter() {
        this.selectedSignerIndex = this.drawSignature.selectedSignerIndex;
        this.selectedSignatureIndex = (this.drawSignature.signers.length > 0) ? this.drawSignature.signers[this.selectedSignerIndex].signaturePositions.length - 1 : 0;

        this.isInit = true;
        if (this.signerCanvases[this.selectedSignerIndex] && this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex]) {
            const checkInit = setInterval(() => {
                this.isInit = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex].isInit;
                if (this.isInit) clearInterval(checkInit);
            }, 100);
        }

        if (this.drawSignature.isInit) {
            this.signerCanvases.forEach((signatureCanvases, idxSigner) => {
                signatureCanvases.forEach((canvas, idxSignature) => {
                    const canvasElement = document.getElementById('the-signature-' + idxSigner + '-' + idxSignature);
                    if ((idxSigner == this.drawSignature.selectedSignerIndex) && (idxSignature == this.selectedSignatureIndex) && !canvas.isInit) {
                        canvasElement.style.zIndex = '2';
                        canvas.isActive = true;
                        if (canvas.publicKey != this.global.loggedAccount.publicKey) canvas.image = this.imgObj;
                    }
                    else {
                        canvasElement.style.zIndex = '1';
                        canvas.isActive = false;
                        if (canvas.publicKey != this.global.loggedAccount.publicKey) canvas.image = this.inactiveImgObj;
                    }
                    if (canvas.isInit) canvas.drawInactive(this.pageNum);
                });
            });
        }
        else {
            this.pdfCanvas = <HTMLCanvasElement>document.getElementById('the-pdf');
            this.pdfCtx = this.pdfCanvas.getContext('2d');

            await this.signature.fetchFromStorage();
            const pngSignatureImg = await HelperService.getPngSignature(this.signature.signatureImg);
            this.signatureImgObj.src = pngSignatureImg;
            this.signatureImgObj.height = 88;
            this.signatureImgObj.width = 188;

            this.imgObj.src = this.imgSrc;
            this.imgObj.height = 88;
            this.imgObj.width = 188;

            this.inactiveImgObj.src = this.inactiveImgSrc;
            this.inactiveImgObj.height = 88;
            this.inactiveImgObj.width = 188;

            /**
             * Asynchronously downloads PDF.
             */
            this.docUint8Array = HelperService.convertDataURIToBinary(this.drawSignature.pdfUri);
            this.pdfDoc = await pdfjsLib.getDocument(this.docUint8Array).promise;

            document.getElementById('page_count').innerHTML = this.pdfDoc.numPages;
            document.getElementById('prev').addEventListener('click', () => this.onPrevPage());
            document.getElementById('next').addEventListener('click', () => this.onNextPage());

            // Initial/first page rendering
            await this.renderPage(this.pageNum);

            // Create signature canvas object for each signature
            this.signaturesInfo.forEach((signatureInfo, idxSigner) => {
                this.signerCanvases[idxSigner] = [];
                signatureInfo.signaturePositions.forEach((signaturePosition, idxSignature) => {
                    this.signerCanvases[idxSigner].push(new SignatureCanvas(signatureInfo.publicKey, idxSignature, signatureInfo.signaturePositions[idxSignature].name));
                });
            });

            this.signerCanvases.forEach((signatureCanvases, idxSigner) => {
                signatureCanvases.forEach((canvas, idxSignature) => {
                    canvas.canvasSize.width = this.pdfCanvas.width;
                    canvas.canvasSize.height = this.pdfCanvas.height;
                    if (canvas.publicKey == this.global.loggedAccount.publicKey)
                        canvas.create('the-signature-' + idxSigner + '-' + idxSignature, this.signatureImgObj, this.scale * this.dpr);
                    else
                        canvas.create('the-signature-' + idxSigner + '-' + idxSignature, this.imgObj, this.scale * this.dpr);

                    canvas.init(this.pageNum, (canvas: SignatureCanvas) => {
                        this.isDone = false;
                        canvas.pageNumber = this.pageNum;
                        this.drawSignature.updateSignaturePosition(
                            canvas.publicKey,
                            canvas.id,
                            this.pageNum,
                            canvas.x,
                            canvas.y,
                            canvas.namePosition
                        );
                    },
                        (x: number, y: number) => {
                            this.selectAnotherSignature(x, y);
                        }
                    );
                    canvas.isActive = true;

                    if ((!this.global.isBrowser) || (this.dpr != 1)) {
                        const canvasElement = <HTMLCanvasElement>document.getElementById('the-signature-' + idxSigner + '-' + idxSignature);
                        const ctx = canvasElement.getContext('2d');
                        canvasElement.style.width = this.pdfCanvas.width / this.dpr + 'px';
                    }

                    const canvasElement = document.getElementById('the-signature-' + idxSigner + '-' + idxSignature);
                    canvasElement.style.zIndex = (idxSigner == this.drawSignature.selectedSignerIndex) ? '2' : '1';
                });
            });

            const checkInit = setInterval(() => {
                if (this.signerCanvases[this.selectedSignerIndex])
                    this.isInit = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex].isInit;
                else
                    this.isInit = true;
                if (this.isInit) clearInterval(checkInit);
            }, 100);

            this.initDoneCanvas();
            // document.getElementById('download').addEventListener('click', () => { this.run(); });

            // document.getElementById('apply').addEventListener('click', () => {
            //     const heightSign = <HTMLInputElement>document.getElementById('heightSign');
            //     const widthSign = <HTMLInputElement>document.getElementById('widthSign');
            //     this.imgObj.height = parseInt(heightSign.value);
            //     this.imgObj.width = parseInt(widthSign.value);
            //     this.signCanvases[this.selectedSignatureIndex].drawSignature();
            // });

            this.drawSignature.isInit = true;
        }
    }

    /**
     * Setup done canvas
     */
    initDoneCanvas() {
        this.doneCanvas = new SignatureCanvas('DONE', 0, '');
        this.doneCanvas.canvasSize.width = this.pdfCanvas.width;
        this.doneCanvas.canvasSize.height = this.pdfCanvas.height;
        this.doneCanvas.create('done-canvas', null, this.scale * this.dpr);
        const canvasElement = <HTMLCanvasElement>document.getElementById('done-canvas');
        const ctx = canvasElement.getContext('2d');
        const getPos = (e) => {
            const [x, y] = this.doneCanvas.computeTouchPosition(e);
            this.selectAnotherSignature(x, y);
        }
        this.doneCanvas.canvas.onmousedown = getPos;
        this.doneCanvas.canvas.ontouchstart = getPos;
        this.doneCanvas.canvas.style.width = this.pdfCanvas.width / this.dpr + 'px';
        this.doneCanvas.canvas.style.zIndex = '0';
    }

    /**
    * Get page info from document, resize canvas accordingly, and render page.
    * @param num Page number.
    */
    async renderPage(num) {
        this.pageRendering = true;
        // Using promise to fetch the page
        const page = await this.pdfDoc.getPage(num)
        // const pdfScreenRatio = window.innerWidth / page.view[2];
        // this.scale = pdfScreenRatio > 1 ? 1 : pdfScreenRatio;

        // Scale up for crystal clear image
        var viewport = page.getViewport({ scale: this.scale * this.dpr });
        this.pdfCanvas.height = viewport.height;
        this.pdfCanvas.width = viewport.width;
        document.documentElement.style.setProperty('--width-container', this.pdfCanvas.width / this.dpr + 'px');

        // Scale down for right scale ratio
        const canvas = document.getElementById(this.pdfCanvas.id);
        if ((!this.global.isBrowser) || (this.dpr != 1))
            canvas.style.width = viewport.width / this.dpr + 'px';

        // Render PDF page into canvas context
        var renderContext = {
            canvasContext: this.pdfCtx,
            viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(() => {
            this.pageRendering = false;
            if (this.pageNumPending !== null) {
                // New page rendering is pending
                this.renderPage(this.pageNumPending);
                this.pageNumPending = null;
            }
        });


        // Update page counters
        document.getElementById('page_num').textContent = num;

        // Update done canvas
        if (this.doneCanvas) {
            this.doneCanvas.canvasSize.width = this.pdfCanvas.width;
            this.doneCanvas.canvasSize.height = this.pdfCanvas.height;
            this.doneCanvas.canvas.width = this.pdfCanvas.width;
            this.doneCanvas.canvas.height = this.pdfCanvas.height;
            this.doneCanvas.scale = this.scale * this.dpr;
            this.doneCanvas.fixScale();
            this.doneCanvas.canvas.style.width = viewport.width / this.dpr + 'px';
        }

        // Update signature
        if (this.drawSignature.isInit)
            if (this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex].isActive)
                this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex].pageNumber = this.pageNum;
        this.signerCanvases.forEach((signatureCanvases, idxSigner) => {
            signatureCanvases.forEach((canvas, idxSignature) => {
                canvas.canvasSize.width = this.pdfCanvas.width;
                canvas.canvasSize.height = this.pdfCanvas.height;
                canvas.canvas.width = this.pdfCanvas.width;
                canvas.canvas.height = this.pdfCanvas.height;
                canvas.scale = this.scale * this.dpr;
                canvas.fixScale();
                canvas.canvas.style.width = viewport.width / this.dpr + 'px';
                if (canvas.isInit) canvas.drawInactive(this.pageNum);
            });
        });
    }

    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    queueRenderPage(num) {
        if (this.pageRendering) {
            this.pageNumPending = num;
        } else {
            this.renderPage(num);
        }
    }

    /**
     * Displays previous page.
     */
    onPrevPage() {
        if (this.pageNum <= 1) {
            return;
        }
        this.pageNum--;
        this.queueRenderPage(this.pageNum);
    }


    /**
     * Displays next page.
     */
    onNextPage() {
        console.log(this.pdfDoc)
        if (this.pageNum >= this.pdfDoc.numPages) {
            return;
        }
        this.pageNum++;
        this.queueRenderPage(this.pageNum);
    }

    /**
     * Embbed signatures to pdf
     */
    async getSignedFile() {
        // Load
        const pdfDoc = await PDF.PDFDocument.load(this.docUint8Array);
        const pages = pdfDoc.getPages();
        // const canvasPngImage = await pdfDoc.embedPng(canvasImageBuffer);

        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(PDF.StandardFonts.Helvetica);

        // Draw
        const embedImg = async () => HelperService.asyncForEach(this.signerCanvases, async (signautrPosition: SignatureCanvas[], idxSigner: number) => {
            const subEmbedImg = async () => HelperService.asyncForEach(signautrPosition, async (signCanvas: SignatureCanvas, idxSignature: number) => {
                const embedPage = pages[signCanvas.pageNumber - 1];
                const { width, height } = embedPage.getSize();
                embedPage.setFont(helveticaFont);
                embedPage.setFontSize(14);  //px

                if (signCanvas.isNamePlace) {
                    const namePosition = signCanvas.namePosition;
                    const nameRect = signCanvas.computeNameRect(namePosition);

                    const isNameCenter = (namePosition == NamePosition.TOP) || (namePosition == NamePosition.BOTTOM);
                    let nameX = nameRect.topLeftX;
                    const nameY = height - nameRect.bottomRightY + 16;

                    if (isNameCenter) {
                        this.pdfCtx.font = "14px Helvetica";
                        const textMetric = this.pdfCtx.measureText(signCanvas.name);
                        const centerNameX = (nameRect.topLeftX + nameRect.bottomRightX) / 2 - textMetric.width / 2;
                        nameX = centerNameX;
                    }

                    if (namePosition == NamePosition.LEFT) {
                        this.pdfCtx.font = "14px Helvetica";
                        const textMetric = this.pdfCtx.measureText(signCanvas.name);
                        const rightAlignNameX = nameRect.bottomRightX - textMetric.width - 5;
                        nameX = rightAlignNameX;
                    }

                    embedPage.drawText(signCanvas.name, {
                        x: nameX,
                        y: nameY,
                        color: PDF.rgb(0, 0, 0),
                        size: 14
                    });
                }

                const signatureImgBuffer = HelperService.convertDataURIToBinary(signCanvas.image.src);
                const signaturePngImg = await pdfDoc.embedPng(signatureImgBuffer);

                const position = signCanvas.getPosition();
                const embedImg = signaturePngImg;
                embedPage.drawImage(embedImg, {
                    x: position.x,
                    y: height - position.y - signCanvas.image.height,
                    height: signCanvas.image.height,
                    width: signCanvas.image.width
                });
            });
            await subEmbedImg();
        });
        await embedImg();

        // Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save();

        // const signedDocBlob = new Blob([pdfBytes], { type: "application/pdf" });
        // const data = window.URL.createObjectURL(signedDocBlob);

        // const fileName = this.drawSignature.name.substring(0, this.drawSignature.name.lastIndexOf('.'));
        // const fileType = this.drawSignature.name.substr(this.drawSignature.name.lastIndexOf('.'));
        // const writeFileName = fileName + '-signed' + fileType;

        // return data;
        return pdfBytes;
    }

    onChange() {
        const selectedCanvas = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex];
        if (!selectedCanvas.isInit && (this.signerCanvases[this.selectedSignerIndex].length > 1)) this.onRemove();
        this.onDone();
        this.navCtrl.navigateForward('select-signer');
    }

    /**
     * Alert by toast
     */
    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }

    /**
     * Check if network is available
     */
    async checkNetwork() {
        if (!this.global.isOnline) {
            this.presentToast('No Internet access!');
            return false;
        }

        await this.global.checkNode(this.global.apiNode);

        if (!this.global.isNodeWorking) {
            this.presentToast('API Node Failure..')
            return false;
        }

        return true;
    }

    /**
     * Put signature done, launch sign proccess
     */
    async onFinish() {
        if (this.isFinishClicked) return;
        this.isFinishClicked = true;
        this.isPutAllSignatures = true;
        this.signerCanvases.forEach((signatureCanvases, idxSigner) => {
            signatureCanvases.forEach((canvas, idxSignature) => {
                if (!canvas.isInit) this.isPutAllSignatures = false;
            });
        })

        const isAccessible = await this.checkNetwork();
        if (!isAccessible) {
            this.isFinishClicked = false;
            return;
        }

        if (this.isPutAllSignatures) {
            this.global.setIsProgressDone(false);
            await this.prepareTask();
            // this.navCtrl.navigateRoot('new-progress');
            this.goTasks();
        }
        else {
            this.isFinishClicked = false;
            this.presentToast('Please put all signature positions');
        }
    }

    /**
     * Create task for multitasking
     */
    async prepareTask() {
        if (!this.multitask.selectedTask) {
            const cFileUint8Array = this.isMultiSign ? null : await this.getSignedFile();
            const task = new SiriusSignSigningTask(
                this.signingWithoutMultisig,
                this.monitor,
                this.uploadStorage,
                this.signature,
                this.global.loggedAccount,
                this.signDoc.document,
                this.drawSignature.signers,
                cFileUint8Array
            );
            const isMultisig = task.document.cosigners.length > 0;
            if (isMultisig) task.incMultiSign(task => this.finishSigning(task));
            else task.incSingleSign(task => this.finishSigning(task));
            this.multitask.tasks.push(task);
        }
    }

    /**
     * Fetch and store document after signing done successfully
     * @param task
     */
    async finishSigning(task: SiriusSignSigningTask) {
        const isSuccess = (task.document.status == 'Confirmed') || (task.document.status == 'Waiting for cosignatures');
        if (isSuccess) {
            const doc = await this.documentClassification.fetchDocumentBySigningTxHash(task.document.signTxHash);
            // doc.localUrl = task.document.file.uri;
            this.documentStorage.storeDocument(doc);
            this.documentClassification.classify(doc);
            this.documentClassification.sortDocs('compledAndWaiting');
        }
    }

    /**
     * Navigate to tasks tab
     */
    goTasks() {
        this.global.setIsProgressDone(true);
        this.multitask.selectedTask = null;
        this.navCtrl.navigateRoot('/app/tabs/tab-tasks');
    }

    /**
     * Add a new signature of current signer
     */
    onAdd() {
        this.isDone = false;
        const currentCanvas = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex];
        let currentPublicKey = this.drawSignature.signers[this.selectedSignerIndex].publicKey;
        let currentName = this.drawSignature.signers[this.selectedSignerIndex].name;
        if (currentCanvas) {
            currentCanvas.isActive = false;
            currentCanvas.image = (currentCanvas.publicKey == this.global.loggedAccount.publicKey) ? this.signatureImgObj : this.inactiveImgObj;
            currentCanvas.drawInactive(this.pageNum);
            currentCanvas.canvas.style.zIndex = '1';
            currentPublicKey = currentCanvas.publicKey;
            currentName = currentCanvas.name;
        }

        const id = this.drawSignature.addSignaturePosition(currentPublicKey);
        const idxOfNewCanvas = this.signerCanvases[this.selectedSignerIndex].length;
        const canvas = new SignatureCanvas(currentPublicKey, id, currentName);
        this.signerCanvases[this.selectedSignerIndex][idxOfNewCanvas] = canvas;
        canvas.canvasSize.width = this.pdfCanvas.width;
        canvas.canvasSize.height = this.pdfCanvas.height;
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (canvas.publicKey == this.global.loggedAccount.publicKey)
                    canvas.create('the-signature-' + this.selectedSignerIndex + '-' + idxOfNewCanvas, this.signatureImgObj, this.scale * this.dpr);
                else
                    canvas.create('the-signature-' + this.selectedSignerIndex + '-' + idxOfNewCanvas, this.imgObj, this.scale * this.dpr);

                canvas.init(this.pageNum, (canvas: SignatureCanvas) => {
                    this.isDone = false;
                    canvas.pageNumber = this.pageNum;
                    this.drawSignature.updateSignaturePosition(
                        canvas.publicKey,
                        canvas.id,
                        this.pageNum,
                        canvas.x,
                        canvas.y,
                        canvas.namePosition
                    );
                },
                    (x: number, y: number) => {
                        this.selectAnotherSignature(x, y);
                    }
                );
                canvas.isActive = true;

                if ((!this.global.isBrowser) || (this.dpr != 1)) {
                    const canvasElement = <HTMLCanvasElement>document.getElementById('the-signature-' + this.selectedSignerIndex + '-' + idxOfNewCanvas);
                    const ctx = canvasElement.getContext('2d');
                    canvasElement.style.width = this.pdfCanvas.width / this.dpr + 'px';
                }

                this.selectedSignatureIndex = idxOfNewCanvas;
                const canvasElement = document.getElementById('the-signature-' + this.selectedSignerIndex + '-' + idxOfNewCanvas);
                canvasElement.style.zIndex = (this.selectedSignerIndex == this.drawSignature.selectedSignerIndex) ? '2' : '1';

                if (this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex]) {
                    const checkInit = setInterval(() => {
                        this.isInit = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex].isInit;
                        if (this.isInit) clearInterval(checkInit);
                    }, 100);
                }
                resolve();
            }, 50);
        });
    }

    /**
     * Remove current signer signature position
     */
    async onRemove() {
        const toBeRemoveCanvas = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex];
        const isLastPosition = this.signerCanvases[this.selectedSignerIndex].length == 1;
        // if (isLastPosition) await this.onAdd();
        // if (isLastPosition) return;
        this.drawSignature.removeSignaturePosition(toBeRemoveCanvas.publicKey, toBeRemoveCanvas.id);
        this.signerCanvases[this.selectedSignerIndex].splice(this.selectedSignatureIndex, 1);
        this.signerCanvases[this.selectedSignerIndex].forEach((canvas, idxSignature) => {
            canvas.id = idxSignature;
            canvas.canvas.id = 'the-signature-' + this.selectedSignerIndex + '-' + idxSignature;
        });
        this.selectedSignatureIndex = this.signerCanvases[this.selectedSignerIndex].length - 1;
        const selectedCanvas = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex];

        if (isLastPosition) {
            this.onAdd();
        }
        else {
            this.onDone();
        }
    }

    /**
     * Done put signature to review
     */
    onDone() {
        const selectedCanvas = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex];
        if (selectedCanvas.isInit) {
            if (selectedCanvas.publicKey != this.global.loggedAccount.publicKey) selectedCanvas.image = this.inactiveImgObj;
            selectedCanvas.isActive = false;
            selectedCanvas.canvas.style.zIndex = '1';
            selectedCanvas.drawInactive(this.pageNum);
            this.doneCanvas.canvas.style.zIndex = '2';
            this.isDone = true;
        }
    }

    /**
     * Re-draw all signature when selected on another signature
     * @param canvas
     * @param signerIndex
     * @param signatureIndex
     */
    onAnotherSiganture(canvas: SignatureCanvas, signerIndex: number, signatureIndex: number) {
        const currentCanvas = this.signerCanvases[this.selectedSignerIndex][this.selectedSignatureIndex];
        currentCanvas.isActive = false;
        currentCanvas.image = (currentCanvas.publicKey == this.global.loggedAccount.publicKey) ? this.signatureImgObj : this.inactiveImgObj;
        currentCanvas.drawInactive(this.pageNum);
        currentCanvas.canvas.style.zIndex = '1';

        canvas.isActive = true;
        canvas.image = (canvas.publicKey == this.global.loggedAccount.publicKey) ? this.signatureImgObj : this.imgObj;
        canvas.canvas.style.zIndex = '2';
        canvas.drawInactive(this.pageNum, true);
        this.drawSignature.selectedSignerIndex = signerIndex;
        this.selectedSignerIndex = signerIndex;
        this.selectedSignatureIndex = signatureIndex;
        this.isDone = false;
    }

    /**
     * Select on another signature
     * @param x
     * @param y
     */
    selectAnotherSignature(x: number, y: number) {
        let isChecked = false;
        this.signerCanvases.forEach((signatureCanvases, idxSigner) => {
            if (isChecked) return;
            signatureCanvases.forEach((canvas, idxSignature) => {
                if (isChecked) return;
                const isInside = canvas.checkSelected(x, y, this.pageNum);
                if (isInside) {
                    this.onAnotherSiganture(canvas, idxSigner, idxSignature);
                    isChecked = true;
                }
            });
        });
        if (!isChecked && !this.isDone) this.onDone();
    }

    /**
     * Option menu
     */
    async presentActionSheet() {
        const doneButtons = [{
            text: 'Add',
            handler: () => {
                this.onAdd();
            }
        }];
        const activeButtons = [{
            text: 'Remove',
            role: 'destructive',
            handler: () => {
                this.onRemove()
            }
        }, {
            text: 'Add',
            handler: () => {
                this.onAdd();
            }
        }, {
            text: 'Done',
            handler: () => {
                this.onDone();
            }
        }];
        const buttons = this.isDone ? doneButtons : activeButtons;

        const actionSheet = await this.actionSheetController.create({
            header: this.signaturesInfo[this.selectedSignerIndex].name,
            mode: 'ios',
            buttons: buttons
        });
        await actionSheet.present();
    }

    /**
     * Zoom In
     */
    async onZoomIn() {
        if (this.isZooming) return;
        this.isZooming = true;
        if (this.pdfCanvas.width / this.dpr > 2 * this.platform.width()) {
            this.isZooming = false;
            return;
        }
        this.scale += 0.1;
        await this.renderPage(this.pageNum);
        this.isZooming = false;
    }

    /**
     * Zoom out
     */
    async onZoomOut() {
        if (this.isZooming) return;
        this.isZooming = true;
        if (this.pdfCanvas.height / this.dpr < this.platform.height()) {
            this.isZooming = false;
            return;
        }
        this.scale -= 0.1;
        await this.renderPage(this.pageNum);
        this.isZooming = false;
    }
}
