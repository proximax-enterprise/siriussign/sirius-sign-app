import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

import { GlobalService } from '../services/global.service';
import { SigningService } from './../services/signing.service';
import { GroupService, GroupElement } from '../services/group.service';

@Component({
    selector: 'app-group-create',
    templateUrl: './group-create.page.html',
    styleUrls: ['./group-create.page.scss'],
})
export class GroupCreatePage implements OnInit {
    groupName: string = '';
    numOfMember: number = 0;
    creator: string;
    cosigners = [''];

    constructor(
        private router: Router,
        private toastController: ToastController,
        private global: GlobalService,
        private siging: SigningService,
        private group: GroupService
    ) { }

    ngOnInit() {
        this.creator = this.global.loggedAccount.publicKey;
    }

    /**
     * Add one more multisig account to list
     */
    onAdd() {
        let length = this.cosigners.length;
        if (length < 4) this.cosigners.push('');
    }

    /**
     * Remove a multisig account from list
     * @param {number} index - index of the address need be removed
     */
    onRemove(index: number) {
        this.cosigners.splice(index, 1);
    }

    /**
     * This function help the template use ngFor, input and Array together.
     * Because of input-array binding, the view (ngFor) is reloaded after type
     * a character. This ngFor must track content by index instead of array content.
     * @param index
     * @param obj
     * @return index
     */
    trackByIndex(index: number, obj: any): any {
        return index;
    }

    /**
     * Check if cosigner public key is valid
     * @param index 
     */
    checkKey(index) {
        if (this.cosigners[index].includes(' ')) return false;
        return (this.cosigners[index].length == 0) || (this.cosigners[index].length == 64);
    }

    /**
     * Alert no Internet access
     */
    async presentToast(msg, time) {
        const toast = await this.toastController.create({
            message: msg,
            duration: time
        });
        toast.present();
    }


    /**
     * Create group
     */
    async createGroup() {
        if (!this.global.isOnline) {
            this.presentToast('No Internet access!', 2000);
            return;
        }

        if (this.groupName == '') {
            this.presentToast('Please enter Group name', 1000);
            return;
        }

        this.cosigners = this.cosigners.filter(value => value != '');
        for (let i = 0; i < this.cosigners.length; i++) {
            if (!this.checkKey(i)) return;
        }

        // Create Multisig Account
        const cosigners = [this.creator, ...this.cosigners];
        const multisigInfo = this.siging.createMultisigAccount(cosigners, this.groupName);
        // const preMultisigAcc = {
        //     publicAccount: 'TEST Account'
        // }

        // Add group
        const group: GroupElement = {
            name: this.groupName,
            multisigAcc: multisigInfo.multisigAccount.publicAccount,
            creationTxHash: multisigInfo.creationTxHash,
            creationDate: Date.now(),
            creator: this.global.loggedAccount.publicKey,
            members: cosigners
        }

        await this.group.storeGroup(group);

        await this.group.setGroups(()=>{});

        this.router.navigateByUrl('/app/tabs/tab-menu/group-manage');
    }
}
