import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionFailPage } from './transaction-fail.page';

describe('TransactionFailPage', () => {
  let component: TransactionFailPage;
  let fixture: ComponentFixture<TransactionFailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionFailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionFailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
