import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var TabHistoryPage = /** @class */ (function () {
    function TabHistoryPage() {
        this.docs = [
            {
                name: 'Demo-waiting.pdf',
                signed: 1,
                actionDate: new Date(2019, 5, 25, 6, 24, 21, 0),
                lastModifiedDate: new Date(2019, 5, 2),
                numCoSign: 5,
                numSigned: 3
            },
            {
                name: 'Demo-signed.pdf',
                signed: -1,
                actionDate: new Date(2019, 5, 27, 12, 15, 21, 0),
                lastModifiedDate: new Date(2019, 6, 27),
                numCoSign: 5,
                numSigned: 3
            }
        ];
    }
    /*
     * Get MM dd, YYYY from Date
     */
    TabHistoryPage.prototype.dateToShortString = function (date) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        //return  date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
        return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
    };
    TabHistoryPage.prototype.nowFromDate = function (date) {
        var now = Date.now();
        var millis = now - date.getTime();
        var min = Math.floor(millis / 60000);
        if (min < 60)
            return min + 'm';
        else if (min < 1440)
            return Math.floor(min / 60) + 'h ' + min % 60 + 'm';
        else if (min < 10080)
            return Math.floor(min / 1440) + ' days';
        else
            return this.dateToShortString(date);
    };
    TabHistoryPage = tslib_1.__decorate([
        Component({
            selector: 'app-tab-history',
            templateUrl: 'tab-history.page.html',
            styleUrls: ['tab-history.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], TabHistoryPage);
    return TabHistoryPage;
}());
export { TabHistoryPage };
//# sourceMappingURL=tab-history.page.js.map