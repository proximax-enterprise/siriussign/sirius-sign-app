import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFileViewPage } from './new-file-view.page';

xdescribe('NewFileViewPage', () => {
    let component: NewFileViewPage;
    let fixture: ComponentFixture<NewFileViewPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NewFileViewPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NewFileViewPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
