import { TestBed } from '@angular/core/testing';

import { LoadingProcessService } from './loading-process.service';

describe('LoadingProcessService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoadingProcessService = TestBed.get(LoadingProcessService);
    expect(service).toBeTruthy();
  });
});
