import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { HelperService } from '../services/helper.service';
import { SigningService } from '../services/signing.service';
import { HistoryService } from '../services/history.service';
import { GroupService, SiriusGroups } from './../services/group.service';
import { HomeService } from './../services/home.service';
import { GlobalService } from './../services/global.service';
import { MonitorService } from '../services/monitor.service';
import { DocumentHistoryService } from './../services/document-history.service';

@Component({
    selector: 'app-tab-home',
    templateUrl: 'tab-home.page.html',
    styleUrls: ['tab-home.page.scss']
})

export class TabHomePage {

    initConnection: boolean = true;

    constructor(
        private router: Router,
        public alertController: AlertController,
        private global: GlobalService,
        private signing: SigningService,
        public home: HomeService,
        public history: HistoryService,
        public group: GroupService,
        private monitor: MonitorService,
        public documentHistory: DocumentHistoryService
    ) {
     }

    ngOnInit() {
        console.log('adfadsf');
        
        this.presentUpgrade();
        const initConnectionSub = this.global.observableIsOnline.subscribe(isOnline => {
            this.initConnection = isOnline;
            if (this.initConnection) {
                this.loadApp();
                if (initConnectionSub) initConnectionSub.unsubscribe();
            }
        });
    }

    dateToShortString = HelperService.dateToShortString;

    nowFromDate = HelperService.nowFromDate;

    /**
     * App starting
     */
    async loadApp() {
        this.home.docs = [];
        this.signing.documentsInfo = [];
        this.home.isLoading = false;
        await this.reloadAll();
        console.log('[Debug] Load done')

        if (this.signing.universalId != '') {
            this.signing.setSelectedDocInfoFromUniversal()
            this.router.navigate(['sign-info']);
        }

        await this.monitor.openListener()
        console.log(this.monitor.listener);

        await this.group.setGroups();
        console.log('[Listner] Listen for Aggregate Bondded Transaction from Groups');
        this.listenMultisigDoc();

        await this.group.fetchJoinGroup();
        console.log('[Listner] Listen for Aggregate Bondded Transaction related to my account');
        this.listenJoinGroup();

        // This Listener channel not working
        // console.log('[Listner] Listen for Aggregate Bondded Cosignature');
        // this.monitor.listenAggregateBondedCosignature(
        //     this.global.loggedAccount.address,
        //     () => { },
        //     () => this.reloadAll()
        // );


        setInterval(async () => {
            if (!this.monitor.checkListener()) {
                await this.monitor.openListener();
                this.listenMultisigDoc();
                this.listenJoinGroup();
            }
        }, 60 * 1000);
    }

    /**
     * View document info
     * @param index
     */
    onDocument(index: number) {
        this.signing.setSelectedDocInfoById(this.home.docs[index].id);
        this.router.navigate(['sign-info']);
    }

    /**
     * View document info
     */
    onHistoryDocument(index: number) {
        this.signing.setSelectedDocInfo(this.documentHistory.getDocById(this.documentHistory.docs[index].id));
        this.router.navigate(['sign-info']);
    }

    /**
     * View group info
     * @param index 
     */
    onGroup(index) {
        this.group.isInvite = true;
        this.group.selectedGroup = this.group.waitingGroups[index];
        this.router.navigate(['group-info']);
    }

    /**
     * Reload home and history
     * @async
     */
    async reloadAll() {
        this.documentHistory.reloadHistory();
        this.group.fetchJoinGroup();
        await this.home.reload();
    }

    /**
     * Refresher
     * @param event 
     */
    async doRefresh(event) {
        await this.reloadAll();
        if (!this.home.isLoading) event.target.complete();
        setTimeout(() => {
            this.listenMultisigDoc();
            this.listenJoinGroup();
            event.target.complete();
        }, 5000);
    }

    async presentAlertJoinGroup() {
        const alert = await this.alertController.create({
            header: 'Join group',
            message: 'There is an invitation for you to join a group. You can view later.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel');
                    }
                },
                {
                    text: 'View',
                    handler: () => {
                        this.group.isInvite = true;
                        this.router.navigate(['group-info']);
                    }
                }
            ]
        });

        await alert.present();
    }

    async presentUpgrade() {
        const alert = await this.alertController.create({
            header: 'Upgrade',
            message: 'There is an invitation for you to join a group. You can view later.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel');
                    }
                },
                {
                    text: 'View',
                    handler: () => {
                        this.group.isInvite = true;
                        this.router.navigate(['group-info']);
                    }
                }
            ]
        });

        await alert.present();
    }

    /**
     * Listner to get aggregate bonded transaction contain create multisig account tx
     */
    listenJoinGroup() {
        this.monitor.listenAggregateBondedTransaction(this.global.loggedAccount.address, () => { }, tx => {
            const isCreateMultisig = this.signing.checkJoinMultisigAcc(tx);
            const isNotCreator = tx.signer.publicKey != this.global.loggedAccount.publicKey;
            const signedCosigners = tx.cosignatures.map(cosignature => cosignature.signer.publicKey);
            const isNotSigned = !signedCosigners.includes(this.global.loggedAccount.publicKey);
            if (isCreateMultisig && isNotCreator && isNotSigned) {
                console.log('[Listener] New group invitation recieved');
                let cosigners = [];
                cosigners.push(tx.signer.publicKey);
                tx.innerTransactions.forEach((tx, index) => {
                    if (index != 0) {
                        cosigners.push(tx.signer.publicKey);
                    }
                });
                // Add group
                const messageInfo = HelperService.parseMessage(tx.innerTransactions[1].message.payload);
                const createdGroupName = messageInfo.groupName;
                const existingGroupName = this.group.groups.map(group => group.name);
                const isNameExist = existingGroupName.includes(createdGroupName);
                let groupName = createdGroupName;
                if (isNameExist) groupName = createdGroupName + '-' + tx.signer.publicKey.slice(-4);
                const group: SiriusGroups = {
                    name: groupName,
                    multisigAcc: tx.innerTransactions[0].signer,
                    creator: tx.signer.publicKey,
                    members: cosigners,
                    isActive: false,
                    numOfAccept: tx.cosignatures.length + 1,
                    transaction: tx
                }
                this.group.waitingGroups.push(group);
                this.group.selectedGroup = group;
                this.presentAlertJoinGroup();
            }
        });
    }

    /**
     * Listen to groups for multisig document
     */
    listenMultisigDoc() {
        this.group.groups.forEach(group => {
            this.signing.getMultisigAccountInfo(group.multisigAcc.address.address)
                .subscribe(accInfo => {
                    console.log(accInfo);
                    group.isActive = true;
                    group.numOfAccept = group.members.length;

                    this.monitor.listenAggregateBondedTransaction(group.multisigAcc.address.address,
                        () => { console.log('Listen to group: ' + group.name); },
                        async tx => {
                            console.log('[Listener] New multi sign document added')
                            await this.home.reload();
                            this.listenMultisigDoc();
                            this.listenJoinGroup();
                        });
                }, err => {
                    console.log(err);
                    group.isActive = false;
                });
        });
    }
}
