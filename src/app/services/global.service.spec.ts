import { TestBed, async } from '@angular/core/testing';

import { GlobalService } from './global.service';
import { IonicStorageModule } from '@ionic/storage';
import { doesNotReject } from 'assert';
import { Device } from '@ionic-native/device/ngx';


describe('GlobalService', () => {
    let service: GlobalService;
    beforeEach(() => {

        TestBed.configureTestingModule({
            imports: [
                IonicStorageModule.forRoot()
            ],
            providers: [
                Storage,
                Device
            ]
        });

        service = TestBed.get(GlobalService);

    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('addNodeList', ()=>{

        it('add new api node to nodelist', async ()=>{
            
            const type = 'api';
            const node = 'https://example.com';
            const add = await service.addNodeList(type, node)
            const list:String[] = await service.fetchNodeList(type)
            expect(list.filter(x=>x === node).length).toBeGreaterThan(0)
            
        })

        it('add new ipfs node to nodelist', async ()=>{
            
            const type = 'ipfs';
            const node = 'https://example.com';
            const add = await service.addNodeList(type, node)
            const list:String[] = await service.fetchNodeList(type)
            
            expect(list.filter(x=>x === node).length).toBeGreaterThan(0)
        })
    })

    describe('setNode', ()=>{
        it('set api node', async ()=>{
            
            const type = 'api';
            const node = 'https://bctestnet2.brimstone.xpxsirius.io';


            service.setNode(type, node)

            expect(service.apiNode).toEqual(node);
            expect(service.apiDomain).toEqual('bctestnet2.brimstone.xpxsirius.io');
            expect(service.apiPort).toBeUndefined();
            expect(service.ws).toEqual('wss://bctestnet2.brimstone.xpxsirius.io');
        })

        it('set ipfs node', async ()=>{
            
            const type = 'ipfs';
            const node = 'https://bctestnet2.brimstone.xpxsirius.io';


            service.setNode(type, node)

            expect(service.ipfsNode).toEqual(node);
            expect(service.ipfsDomain).toEqual('bctestnet2.brimstone.xpxsirius.io');
            expect(service.ipfsPort).toBeUndefined();

        })
    })

    

});
