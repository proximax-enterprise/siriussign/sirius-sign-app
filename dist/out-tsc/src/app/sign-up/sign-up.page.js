import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
var SignUpPage = /** @class */ (function () {
    function SignUpPage(route) {
        this.route = route;
        this.showForm = false;
        this.errorMess = '';
        this.inputDat = {
            email: '',
            inputPass: '',
            verifPass: ''
        };
    }
    SignUpPage.prototype.ngOnInit = function () {
    };
    /**
      * Navigate to sign-in page
      */
    SignUpPage.prototype.goSignIn = function () {
        this.route.navigate(['sign-in']);
    };
    /**
      * Navigate to code-verify page
      */
    SignUpPage.prototype.goCodeVerify = function () {
        this.route.navigate(['code-verify']);
    };
    /**
      * Validate email input
      */
    SignUpPage.prototype.checkEmail = function () {
        var emailPattern = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm);
        var isMatch = emailPattern.test(this.inputDat.email.toLowerCase());
        if (isMatch) {
            this.errorMess = '';
            return true;
        }
        else {
            this.errorMess = 'Your email is invalid.';
            return false;
        }
    };
    /**
      * Validate password input
      */
    SignUpPage.prototype.checkPass = function () {
        var isMatch = (this.inputDat.verifPass === this.inputDat.inputPass) || (this.inputDat.verifPass === '');
        if (isMatch) {
            this.errorMess = '';
            return true;
        }
        else {
            this.errorMess = 'Your password and confirmation password do not match.';
            return false;
        }
    };
    /*
     * Validate form input
     */
    SignUpPage.prototype.checkInput = function () {
        if (this.checkEmail())
            this.checkPass();
    };
    /*
     * Launch sign up process
     */
    SignUpPage.prototype.signUp = function () {
        var isNoEmail = this.inputDat.email == '';
        var isNoInputPass = this.inputDat.inputPass == '';
        var isNoVerifPass = this.inputDat.verifPass == '';
        if (isNoEmail)
            this.errorMess = 'Email is required.';
        else if (isNoInputPass)
            this.errorMess = 'Password is required.';
        else if (isNoVerifPass)
            this.errorMess = 'Confirmation password is required.';
        else {
            this.goCodeVerify();
        }
    };
    SignUpPage = tslib_1.__decorate([
        Component({
            selector: 'app-sign-up',
            templateUrl: './sign-up.page.html',
            styleUrls: ['./sign-up.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], SignUpPage);
    return SignUpPage;
}());
export { SignUpPage };
//# sourceMappingURL=sign-up.page.js.map