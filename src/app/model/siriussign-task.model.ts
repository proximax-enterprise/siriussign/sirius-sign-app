import { Transaction, Account, SignedTransaction, TransferTransaction } from 'tsjs-xpx-chain-sdk';
import { UploadResult } from 'tsjs-chain-xipfs-sdk';

import * as PDF from 'pdf-lib';

import { SiriusSignDocument, SignerSignature } from './siriussign-document.model';
import { NamePosition, SignatureCanvas } from './signature-canvas.model';

import { SignDocument } from '../services/sign-document.service';
import { SigningWithoutMultisigService } from './../services/signing-without-multisig.service';
import { MonitorService } from './../services/monitor.service';
import { UploadStorageService } from './../services/upload-storage.service';
import { SignatureService } from './../services/signature.service';
import { SignatureInfo, SignaturePosition } from '../services/draw-signature.service';
import { DocumentClassificationService } from '../services/document-classification.service';
import { VerifyService } from '../services/verify.service';
import { HelperService } from '../services/helper.service';

export class SiriusSignTask {
    percent: number;
    transactionStatus: {
        isDone: boolean,
        status: string
    }
    waitVal: number;

    name: string;
    type: 'Single signing' | 'Multi signing' | 'Cosigning' | 'Verifying';

    constructor() {
        this.percent = 0;
        this.waitVal = Math.floor(Math.random() * 10) + 74;
        this.transactionStatus = {
            isDone: false,
            status: 'undefined'
        }
    }
}

export class SiriusSignSigningTask extends SiriusSignTask {
    account: Account;
    document: SignDocument;
    signers: SignatureInfo[];
    percent: number;
    transactionStatus: {
        isDone: boolean,
        status: string
    }
    completedFileUint8Array: Uint8Array;

    processStage: number = 0;
    waitVal: number;

    constructor(
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private monitor: MonitorService,
        private uploadStorage: UploadStorageService,
        private signature: SignatureService,

        account: Account,
        document: SignDocument,
        signers: SignatureInfo[],
        completedFileUint8Array: Uint8Array
    ) {
        super();
        this.processStage = 0;

        this.account = Object.create(account);
        this.document = {
            file: document.file,
            size: document.size,
            signDate: document.signDate,
            cosigners: document.cosigners,
            verifiers: document.verifiers,
            multisigAcc: document.multisigAcc,     // phase 1
            documentAcc: document.documentAcc,     // phase 2
            fileHash: document.fileHash,
            signTxHash: document.signTxHash,
            uploadTxHash: document.uploadTxHash,
            isEncrypt: document.isEncrypt,
            signatureUploadTxHash: document.signatureUploadTxHash,
            status: document.status
        };
        this.signers = signers.map(ins => ins);
        this.completedFileUint8Array = completedFileUint8Array;
        this.name = this.document.file.name;
        this.type = (this.document.cosigners.length > 0) ? 'Multi signing' : 'Single signing';
    }

    /**
     * Increase progress percentage of new process
     * @param fnFinish
     */
    runNewProgress(fnFinish?) {
        let reGetStatus = setInterval(() => {
            const stage = this.processStage + 1;
            if (this.percent < (this.waitVal * stage / 4)) this.percent += 1;
            if (this.transactionStatus.isDone) clearInterval(reGetStatus);
        }, 250);

        let toDone = setInterval(() => {
            if (this.transactionStatus.isDone) {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(toDone);
                    if (fnFinish) fnFinish(this);
                }
            }
        }, 100);
    }

    /**
     * Upload file
     *
     * If failed, set process done, status failed by error
     */
    async uploadSignDoc() {
        const isEncrypt = this.document.isEncrypt;
        let uploadRes: UploadResult;
        try {
            uploadRes = await this.uploadStorage.uploadFile(this.document, isEncrypt);

            if (this.completedFileUint8Array)
                this.uploadStorage.uploadCompletedDoc(this.completedFileUint8Array, this.name, this.document.documentAcc.publicAccount, isEncrypt, this.document);

            return uploadRes;
        }
        catch (err) {
            console.log(err);
            this.transactionStatus.isDone = true;
            this.transactionStatus.status = 'Upload failed';

            this.document.status = 'Failed to upload the document: ' + err.message;
        }
    }

    /**
     * Single sign with new process
     */
    async incSingleSign(fnFinish?) {
        // Increase progress percentage
        this.runNewProgress(fnFinish);

        // Create document account
        this.document.documentAcc = this.signingWithoutMultisig.createDocumentAccount();

        // Set Restriction
        const signedRestrictionTx = this.signingWithoutMultisig.createRestriction(this.document);
        const restrictTx: Transaction = await this.monitor.waitForTransactionConfirmed(
            this.document.documentAcc.address,
            signedRestrictionTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedRestrictionTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = err;
                return null;
            });
        if (!restrictTx) return;
        this.processStage++;

        // Upload file
        const uploadTx = await this.uploadSignDoc();
        if (!uploadTx) return;
        this.document.uploadTxHash = uploadTx.transactionHash;
        this.processStage++;

        // Upload signature
        if (this.signers.length != 0) {
            const signatureUploadTx = await this.uploadStorage.uploadSignatureForDocSigning(
                this.signature.signatureImg,
                this.document.documentAcc.publicAccount
            ).catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err.status;
                this.document.status = 'Failed to upload signature: ' + err.message;
                return null;
            });
            if (!signatureUploadTx) return;
            this.document.signatureUploadTxHash = signatureUploadTx.transactionHash;
        }
        else {
            this.document.signatureUploadTxHash = '';
        }
        console.log(this.document);
        this.processStage++;

        // Sign transaction
        let signatureInfo = this.signers.filter(signer => signer.publicKey == this.account.publicKey);
        // ********************************Temp for 1 instance of signature
        if (signatureInfo.length == 0) {
            const noSigantureInfo: SignatureInfo = {
                publicKey: this.account.publicKey,
                name: '',
                signaturePositions: [{
                    name: '',
                    page: 0,
                    x: -1,
                    y: -1,
                    namePosition: NamePosition.NONE
                }]
            }
            signatureInfo.push(noSigantureInfo);
        }
        const signedDocSigningTx = this.signingWithoutMultisig.createDocSigningTransaction(this.document, true, signatureInfo[0]);
        this.document.signTxHash = signedDocSigningTx.hash;
        const docSigningTx = await this.monitor.waitForTransactionConfirmed(this.account.address, signedDocSigningTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedDocSigningTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = err;
            });
        if (!docSigningTx) return;

        this.transactionStatus.isDone = true;
        this.transactionStatus.status = 'confirmed';
        this.document.status = 'Confirmed';
    }

    /**
     * Run multi sign task
     * @param fnFinish
     */
    async incMultiSign(fnFinish?) {
        console.log('Multi Sign');
        // Increase progress percentage
        this.runNewProgress(fnFinish);

        // Create document account
        this.document.documentAcc = this.signingWithoutMultisig.createDocumentAccount();

        // Set Restriction
        const signedRestrictionTx = this.signingWithoutMultisig.createRestriction(this.document);
        const restrictTx: Transaction = await this.monitor.waitForTransactionConfirmed(
            this.document.documentAcc.address,
            signedRestrictionTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedRestrictionTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = err;
                return null;
            });
        if (!restrictTx) return;
        this.processStage++;

        // Upload file
        const uploadTx = await this.uploadSignDoc();
        this.document.uploadTxHash = uploadTx.transactionHash;
        if (!uploadTx) return;

        // Upload signature
        if (this.signers.map(signatureInfo => signatureInfo.publicKey).includes(this.account.publicKey)) {
            const signatureUploadTx = await this.uploadStorage.uploadSignatureForDocSigning(
                this.signature.signatureImg,
                this.document.documentAcc.publicAccount
            ).catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err.status;
                this.document.status = 'Failed to upload signature: ' + err.status;
                return null;
            });
            if (!signatureUploadTx) return;
            this.document.signatureUploadTxHash = signatureUploadTx.transactionHash;
        }
        else {
            this.document.signatureUploadTxHash = '';
        }
        this.processStage++;

        // Sign transaction
        let signatureInfo = this.signers.filter(signer => signer.publicKey == this.account.publicKey);
        // ********************************Temp for 1 instance of signature
        if (signatureInfo.length == 0) {
            const noSigantureInfo: SignatureInfo = {
                publicKey: this.account.publicKey,
                name: '',
                signaturePositions: [{
                    name: '',
                    page: 0,
                    x: -1,
                    y: -1,
                    namePosition: NamePosition.NONE
                }]
            }
            signatureInfo.push(noSigantureInfo);
        }
        const signedDocSigningTx = this.signingWithoutMultisig.createDocSigningTransaction(this.document, true, signatureInfo[0]);
        this.document.signTxHash = signedDocSigningTx.hash;

        const docSigningTx: Transaction = await this.monitor.waitForTransactionConfirmed(this.account.address, signedDocSigningTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedDocSigningTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = err;
                return null;
            });
        if (!docSigningTx) return;
        this.processStage++;

        // Send notification
        let signers = this.signers;
        const noSignatureSigners = this.document.cosigners
            .filter(cosigner => !signers.map(signer => signer.publicKey).includes(cosigner));
        noSignatureSigners.forEach(signer => {
            const noSigantureInfo: SignatureInfo = {
                publicKey: signer,
                name: '',
                signaturePositions: [{
                    name: '',
                    page: 0,
                    x: -1,
                    y: -1,
                    namePosition: NamePosition.NONE
                }]
            }
            signers.push(noSigantureInfo);
        });
        const signedNotiTx = this.signingWithoutMultisig.createMultiSigningNotificationTransaction(this.document, signers);
        const aggNotiTx: Transaction = await this.monitor.waitForTransactionConfirmed(this.document.documentAcc.address, signedNotiTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedNotiTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.document.status = 'Failed to send notification to cosigners: ' + err;
                return null;
            });;
        if (!aggNotiTx) return;

        this.transactionStatus.isDone = true;
        this.transactionStatus.status = 'confirmed';
        this.document.status = 'Confirmed';
    }
}

export class SiriusSignCosigningTask extends SiriusSignTask {
    account: Account;
    cosigner: SignatureInfo;
    signatureImg: string;
    selectedDocInfo: SiriusSignDocument;
    selectedDocStatus: string;
    completedFileUint8Array: Uint8Array;
    originalFileUint8Array: Uint8Array

    constructor(
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private documentClassification: DocumentClassificationService,
        private monitor: MonitorService,
        private uploadStorage: UploadStorageService,
        account: Account,
        cosigner: SignatureInfo,
        signatureImg: string,
        selectedDocInfo: SiriusSignDocument,
        completedFileUint8Array: Uint8Array,
        originalFileUint8Array: Uint8Array
    ) {
        super();
        this.account = Object.create(account);
        this.cosigner = {
            publicKey: cosigner.publicKey,
            name: cosigner.name,
            signaturePositions: cosigner.signaturePositions
        };
        this.signatureImg = signatureImg;
        this.selectedDocInfo = SiriusSignDocument.create(
            selectedDocInfo.id,
            selectedDocInfo.name,
            selectedDocInfo.fileHash.sha256Hash,
            selectedDocInfo.uploadDate,
            selectedDocInfo.owner,
            selectedDocInfo.isSigned,
            selectedDocInfo.signDate,
            selectedDocInfo.cosigners,
            selectedDocInfo.cosignatures,
            selectedDocInfo.documentAccount,
            selectedDocInfo.signTxHash,
            selectedDocInfo.uploadTxHash,
            selectedDocInfo.isEncrypt,
            // selectedDocInfo.signatures,
            // selectedDocInfo.signaturesUploadTxHash,
            selectedDocInfo.signerSignatures,
            selectedDocInfo.verifiers,
            selectedDocInfo.verifierSignatures,
            selectedDocInfo.localUrl
        );
        this.name = this.selectedDocInfo.name;
        this.completedFileUint8Array = completedFileUint8Array;
        this.originalFileUint8Array = originalFileUint8Array;
        this.type = 'Cosigning';
    }

    /**
     * Increase progress percentage of new process
     */
    runProgress(fnFinish?) {
        const reGetStatus = setInterval(() => {
            if (this.percent < this.waitVal) this.percent += 1;
            if (this.transactionStatus.isDone) clearInterval(reGetStatus);
        }, 300);

        const toDone = setInterval(() => {
            if (this.transactionStatus.isDone) {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(toDone);
                    if (fnFinish) fnFinish(this);
                }
            }
        }, 100);
    }


    /**
     * Run Cosign task
     */
    async incCosignProgress(fnFinish?) {
        this.runProgress(fnFinish);

        // Upload signature
        let userSignatureIndex = this.selectedDocInfo.signerSignatures
            .map(signature => signature.publicKey)
            .indexOf(this.account.publicKey);
        if (userSignatureIndex < 0) {
            userSignatureIndex = this.selectedDocInfo.signerSignatures
            .map(signature => signature.publicKey)
            .indexOf(this.account.address.plain());
        }

        if (this.cosigner.signaturePositions[0].name != '') {
            const signatureUploadTx = await this.uploadStorage.uploadSignatureForDocSigning(
                this.signatureImg,
                this.selectedDocInfo.documentAccount
            ).catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err.status;
                this.selectedDocStatus = 'Failed to upload signature: ' + err.status;
                return null;
            });
            if (!signatureUploadTx) return;
            // this.selectedDocInfo.signaturesUploadTxHash.push(signatureUploadTx.transactionHash);
            this.selectedDocInfo.signerSignatures[userSignatureIndex].signatureUploadTxHash = signatureUploadTx.transactionHash;
        }
        else {
            this.selectedDocInfo.signerSignatures[userSignatureIndex].signatureUploadTxHash = '';
        }

        // Sign transaction
        let signedDocSigningTx: SignedTransaction;
        try {
            signedDocSigningTx = this.signingWithoutMultisig.createDocSigningTransaction(this.selectedDocInfo, false, this.cosigner);
        }
        catch (err) { // Prevent error 'plain is not a function of address' cause stuck progress
            this.transactionStatus.isDone = true;
            this.transactionStatus.status = 'Failed to get document address';
            this.selectedDocStatus = 'Failed to cosign: Failed to get document address';
            return;
        }
        this.selectedDocInfo.signTxHash = signedDocSigningTx.hash;

        const docSigningTx: Transaction = await this.monitor.waitForTransactionConfirmed(this.account.address, signedDocSigningTx.hash,
            () => { this.signingWithoutMultisig.announceTransaction(signedDocSigningTx); })
            .catch(err => {
                this.transactionStatus.isDone = true;
                this.transactionStatus.status = err;
                this.selectedDocStatus = 'Failed to cosign: ' + err;
                return null;
            });
        if (!docSigningTx) return;

        // Upload completed document
        const allDocumentSigningTxs = await this.documentClassification.fetchAllDocumentSigningTransactionsByDocumentAccount(this.selectedDocInfo.documentAccount.publicKey);
        const sortCondition = (a: TransferTransaction, b: TransferTransaction) => {
            if (a.transactionInfo.id > b.transactionInfo.id) return -1;
            else return 1;
        }
        // allDocumentSigningTxs.sort((tx1, tx2) => sortCondition(tx1, tx2));
        // console.log(allDocumentSigningTxs);
        // if (this.selectedDocInfo.cosignatures.length == this.selectedDocInfo.cosigners.length - 1)
        //     this.uploadStorage.uploadCompletedDoc(this.completedFileUint8Array, this.name, this.selectedDocInfo.documentAccount, this.selectedDocInfo.isEncrypt, this.selectedDocInfo);

        if (
            (allDocumentSigningTxs[0].signer.publicKey == this.account.publicKey) &&
            (this.selectedDocInfo.cosigners.length == allDocumentSigningTxs.length - 1)
        ) {
            const completedFile = await this.getSignedFile();
            this.uploadStorage.uploadCompletedDoc(completedFile, this.name, this.selectedDocInfo.documentAccount, this.selectedDocInfo.isEncrypt, this.selectedDocInfo);
        }

        this.transactionStatus.isDone = true;
        this.transactionStatus.status = 'confirmed';
        this.selectedDocStatus = 'Cosigned';
    }

    /**
     * Embbed signatures to pdf
     */
    async getSignedFile() {
        console.log('[PDF] Embbed signature');
        console.log(this.originalFileUint8Array);
        // Load
        const pdfDoc = await PDF.PDFDocument.load(this.originalFileUint8Array);
        const pages = pdfDoc.getPages();
        // const canvasPngImage = await pdfDoc.embedPng(canvasImageBuffer);
        const dpr = window.devicePixelRatio;

        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(PDF.StandardFonts.Helvetica);

        // Draw
        const embedImg = async () => HelperService.asyncForEach(this.selectedDocInfo.signerSignatures, async (signerSignature: SignerSignature, idxSigner: number) => {
            if (!signerSignature.signatureUploadTxHash || signerSignature.signatureUploadTxHash == '') return;
            const signatureSvgDataUri = await this.uploadStorage.downloadFileToDataUri(signerSignature.signatureUploadTxHash, false)
                .catch(err => {
                    throw new Error('Failed to fetch signature image');
                });
            const signaturePngDataUri = await HelperService.getPngSignature(signatureSvgDataUri);
            const signaturePngImg = await pdfDoc.embedPng(signaturePngDataUri);

            const subEmbedImg = async () => HelperService.asyncForEach(signerSignature.signaturePositions, async (signaturePosition: SignaturePosition, idxSignature: number) => {
                const embedPage = pages[signaturePosition.page - 1];
                const { width, height } = embedPage.getSize();
                embedPage.setFont(helveticaFont);
                embedPage.setFontSize(14);  //px

                const signatureCanvas = new SignatureCanvas('', 0, '');
                const imgElement = document.createElement('img');
                imgElement.src = '';
                imgElement.height = 88;
                imgElement.width = 188;
                signatureCanvas.x = signaturePosition.x;
                signatureCanvas.y = signaturePosition.y;
                signatureCanvas.image = imgElement;

                const isNamePlaced = (signaturePosition.name != '');
                if (isNamePlaced) {
                    const namePosition = signaturePosition.namePosition;
                    const nameRect = signatureCanvas.computeNameRect(namePosition);

                    const isNameCenter = (namePosition == NamePosition.TOP) || (namePosition == NamePosition.BOTTOM);
                    let nameX = nameRect.topLeftX;
                    const nameY = height / dpr - nameRect.bottomRightY + 16;

                    const pdfCanvas = document.createElement('canvas');
                    const pdfCtx = pdfCanvas.getContext('2d');

                    if (isNameCenter) {
                        pdfCtx.font = "14px Helvetica";
                        const textMetric = pdfCtx.measureText(signaturePosition.name);
                        const centerNameX = (nameRect.topLeftX + nameRect.bottomRightX) / 2 - textMetric.width / 2;
                        nameX = centerNameX;
                    }

                    if (namePosition == NamePosition.LEFT) {
                        pdfCtx.font = "14px Helvetica";
                        const textMetric = pdfCtx.measureText(signaturePosition.name);
                        const rightAlignNameX = nameRect.bottomRightX - textMetric.width - 5;
                        nameX = rightAlignNameX;
                    }

                    embedPage.drawText(signaturePosition.name, {
                        x: nameX,
                        y: nameY,
                        color: PDF.rgb(0, 0, 0),
                        size: 14
                    });
                }

                const embedImg = signaturePngImg;
                embedPage.drawImage(embedImg, {
                    x: signaturePosition.x,
                    y: height - signaturePosition.y - signatureCanvas.image.height,
                    height: signatureCanvas.image.height,
                    width: signatureCanvas.image.width
                });
            });
            await subEmbedImg();
        });
        await embedImg();

        // Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save();
        return pdfBytes;
    }
}

export class SiriusSignVerifyTask extends SiriusSignTask {
    account: Account;
    selectedDocInfo: SiriusSignDocument;
    verifyResult = {
        status: '',
        isValid: false
    }
    docResult: SiriusSignDocument;
    isRealVerify: boolean;

    constructor(
        private monitor: MonitorService,
        private verify: VerifyService,
        private signingWithoutMultisig: SigningWithoutMultisigService,
        private documentClassification: DocumentClassificationService,
        account: Account,
        selectedDocInfo: SiriusSignDocument
    ){
        super();
        this.type = 'Verifying';
        this.account = account;
        this.selectedDocInfo = selectedDocInfo;
        this.name = this.selectedDocInfo.name;
    }

    /**
     * Launch verify progress
     */
    async incVerify(fnFinish) {
        const incProgress = setInterval(() => {
            if (this.percent < 75) this.percent += 1;
            if (this.transactionStatus.isDone) clearInterval(incProgress);
        }, 250);

        const toDone = setInterval(() => {
            if (this.transactionStatus.isDone) {
                if (this.percent < 100) this.percent += 1;
                if (this.percent == 100) {
                    clearInterval(toDone);
                    if (fnFinish) fnFinish();
                }
            }
        }, 40);

        const signedVerifyTx = this.verify.createDocVerifyTransaction(this.selectedDocInfo);
        const docVerifyTx: Transaction = await this.monitor.waitForTransactionConfirmed(this.account.address, signedVerifyTx.hash, () => {
            this.signingWithoutMultisig.announceTransaction(signedVerifyTx);
        }).catch(err => {
            this.verifyResult.status = err.message;
            return null;
        });

        const doc = await this.documentClassification.fetchDocumentByDocumentAccount(this.documentClassification.selectedDocInfo.documentAccount.publicKey);
        this.docResult = doc;
        if (docVerifyTx) {
            this.verifyResult.status = 'Verified';
            this.verifyResult.isValid = true;
            this.transactionStatus.status = 'confirmed';
            this.transactionStatus.isDone = true;
        }
        else {
            this.verifyResult.isValid = false;
            this.transactionStatus.isDone = true;
        }
    }
}