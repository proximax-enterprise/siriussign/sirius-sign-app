import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabTasksPage } from './tab-tasks.page';

describe('TabTasksPage', () => {
    let component: TabTasksPage;
    let fixture: ComponentFixture<TabTasksPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TabTasksPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TabTasksPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
