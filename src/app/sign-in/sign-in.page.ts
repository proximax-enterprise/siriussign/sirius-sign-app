import { Component, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

import { SiriusWallet } from '../model/sirius-wallet.model';

import { GlobalService } from './../services/global.service';
import { WalletService } from './../services/wallet.service';
import { AccountStoreService } from '../services/account-store.service';
import { SignatureService } from '../services/signature.service';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.page.html',
    styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage {

    errorMess = '';
    inputDat = {
        email: '',
        pass: ''
    };

    passView = 'password';

    wallets: SiriusWallet[];

    selectedWallet: SiriusWallet;
    isFingerAllow: boolean = false;
    isKeyboardShow: boolean = false;
    isProcessing: boolean = false;

    constructor(
        private route: Router,
        private alertController: AlertController,
        private faio: FingerprintAIO,
        public global: GlobalService,
        private wallet: WalletService,
        private accountStore: AccountStoreService,
        private signature: SignatureService
    ) {
        this.setStyleDefault();
    }

    async ionViewWillEnter() {
        this.setStyleDefault();
        this.wallets = await this.wallet.fetchWallets();
        this.selectedWallet = (this.wallets && this.wallets.length > 0) ? this.wallets[0] : null;
        this.isFingerAllow = this.selectedWallet ? this.selectedWallet.tertiary.length == 64 : false;
        this.inputDat.email = this.selectedWallet ? this.selectedWallet.name : '';
        this.global.isAllowFingerprint = this.isFingerAllow;

        // Observe Keyboard show event
        window.addEventListener('keyboardWillShow', (e) => {
            this.isKeyboardShow = true;
            // this.setStyle('root', '--height-logo', 'fit-content');
            // this.setStyle('root', '--padding-top-logo', '20px');
            // this.setStyle('root', '--height-form', 'fit-content');
        });

        // Observe Keyboard hide event
        window.addEventListener('keyboardWillHide', () => {
            this.isKeyboardShow = false;
            // this.setStyleDefault();
        });
    }

    /**
     * Set style value
     * @param selector 
     * @param property 
     * @param value 
     */
    setStyle(selector: string, property: string, value: string) {
        document.documentElement.style.setProperty(property, value);
    }

    /**
     * Set style to default values
     */
    setStyleDefault() {
        this.setStyle('root', '--height-logo', '30vh');
        this.setStyle('root', '--padding-top-logo', '15vh');
        this.setStyle('root', '--img-width-logo', '90px');
        this.setStyle('root', '--height-welcome', '20vh');
        this.setStyle('root', '--height-form', '40vh');
        this.setStyle('root', '--height-signup', '8vh');
    }

    /**
     * Show/hide password
     */
    togglePassView() {
        if (this.passView == 'password') this.passView = 'text';
        else this.passView = 'password';
    }

    /**
      * Validate email input 
      */
    checkEmail() {
        this.inputDat.email = this.inputDat.email.toLowerCase();
        const emailPattern = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm);
        let isMatch = emailPattern.test(this.inputDat.email.toLowerCase());
        if (isMatch) {
            this.errorMess = '';
            return true;
        }
        else {
            this.errorMess = 'Your email is invalid.';
            return false;
        }
    }

    /**
      * Validate password input
      */
    checkPass() {
        return true;
    }

    /*
     * Validate form input
     */
    checkInput() {
        if (this.checkEmail())
            return this.checkPass();
        return false;
    }

    /**
     * Launch Sign in process
     */
    async onSignIn() {
        this.isProcessing = true;
        let isNoEmail = this.inputDat.email == '';
        let isNoPass = this.inputDat.pass == '';

        if (isNoEmail) this.errorMess = 'Please enter your email.';
        else if (isNoPass) this.errorMess = 'Please enter your password.';
        else if (this.checkInput()) {
            //let isExist = await this.wallet.checkWalletExist(this.inputDat.email);
            try {
                this.errorMess = '';
                // this.selectedWallet = await this.wallet.getWalletByName(this.inputDat.email);
                let toBeSelectedWallet = [];
                if (this.selectedWallet && this.selectedWallet.name != this.inputDat.email) {
                    console.log("wrong wallet")
                    toBeSelectedWallet = this.wallets.filter(wlt => wlt.name == this.inputDat.email);
                    if (toBeSelectedWallet.length == 1) this.selectedWallet = toBeSelectedWallet[0];
                }
                if (!this.selectedWallet || (this.selectedWallet && this.selectedWallet.name != this.inputDat.email)) {
                    const selectedAccount = await this.accountStore.getAccount(this.inputDat.email, this.inputDat.pass);
                    if (!selectedAccount) throw new Error('No account matched');
                    const plan = selectedAccount.uDid == this.global.uDid ? selectedAccount.plan : -1;
                    this.wallet.setInfo(
                        selectedAccount.name,
                        selectedAccount.password,
                        plan,
                        selectedAccount.privateKey
                    );
                    await this.wallet.createWalletFromPrivateKey();
                    await this.signature.storeSignature(selectedAccount.name, selectedAccount.signature, '');
                    this.wallet.clearInfo();
                    this.global.setIsSignedUp(true);

                    this.wallets = await this.wallet.fetchWallets();
                    this.selectedWallet = this.wallets.filter(wlt => wlt.name == this.inputDat.email)[0];
                }

                await this.wallet.signIn(this.selectedWallet, this.inputDat.pass);
                const isDisableFingerprint = this.selectedWallet.tertiary.length < 63;
                if (!this.global.isBrowser) {
                    if (!this.isFingerAllow && !isDisableFingerprint) {
                        const password = this.inputDat.pass;
                        await this.presentAlertFingerprint(() => {
                            console.log(this.inputDat.pass);
                            console.log(password);
                            this.global.isAllowFingerprint = true;
                            this.wallet.allowFingerprint(this.inputDat.email, password);
                        });
                    }
                }
                this.inputDat.pass = '';
                this.isProcessing = false;
                this.goHome();
            }
            catch (err) {
                console.log(err);
                this.errorMess = 'Email or Password is invalid.';
                this.isProcessing = false;
            }
        }
        this.isProcessing = false;
    }

    /**
     * Ask user if using fingerprint or not
     * @param fn 
     */
    async presentAlertFingerprint(fn) {
        const alert = await this.alertController.create({
            header: 'Sign in with one touch?',
            message: 'Do you want to use your fingerprint in the system to sign in this account?',
            inputs: [
                {
                    name: 'remember',
                    type: 'checkbox',
                    label: 'Remember this choice',
                    value: 'remember',
                    checked: true
                },
            ],
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        this.goHome();
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.fingerprint(fn);
                    }
                }
            ]
        });

        alert.present();
        let result = await alert.onDidDismiss();
        if (result.data.values.length == 1) this.wallet.disableFingerprint(this.inputDat.email);
    }


    /**
     * Fingerprint
     */
    fingerprint(fn) {
        this.faio.isAvailable()
            .then(res => {
                console.log(res);
                this.faio.show({
                    // clientId: 'Fingerprint-Demo', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
                    // clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
                    disableBackup: true,  //Only for Android(optional)
                    // localizedFallbackTitle: 'Use Pin', //Only for iOS
                    // localizedReason: 'Please authenticate' //Only for iOS
                })
                    .then((result: any) => { console.log(result); fn(); })
                    .catch((error: any) => console.log(error));
            })
            .catch(err => { console.log('Fingerprint is not available'); console.log(err); });
    }

    /**
     * Trigger fingerprint sign in
     */
    signInWithFingerprint() {
        this.inputDat.email = this.inputDat.email.toLowerCase();
        if (this.isFingerAllow) {
            this.fingerprint(async () => {
                this.isProcessing = true;
                await this.wallet.signInWithFingerprint(this.selectedWallet);
                this.isProcessing = false;
                this.goHome();
            });
        }
    }

    /**
     * Select email
     * @param event 
     */
    onSelectEmail(event) {
        this.inputDat.email = event.target.value;
        const index = this.wallets.map(wallet => wallet.name).indexOf(this.inputDat.email);
        this.selectedWallet = this.wallets[index];
        this.isFingerAllow = this.selectedWallet.tertiary.length == 64;
        this.global.isAllowFingerprint = this.isFingerAllow;
        this.signInWithFingerprint();
    }

    /**
     * Check email in device and account fingerprint
     * @param event 
     */
    checkAccount() {
        const email = this.inputDat.email.toLowerCase();
        const setFalse = () => {
            this.isFingerAllow = false;
            this.global.isAllowFingerprint = this.isFingerAllow;
        }
        if (!this.wallets || this.wallets && this.wallets.length == 0) {
            setFalse();
            return;
        }
        const index = this.wallets.map(wallet => wallet.name).indexOf(email);
        if (index < 0) {
            setFalse();
            return;
        }
        this.selectedWallet = this.wallets[index];
        this.isFingerAllow = this.selectedWallet.tertiary.length == 64;
        this.global.isAllowFingerprint = this.isFingerAllow;
        // this.signInWithFingerprint();
    }

    /*
     * Navigate to App's Home
     */
    goHome() {
        this.route.navigate(['app']);
    }

    /*
     * Navigate to sign-up
     */
    goSignUp() {
        this.route.navigate(['sign-up']);
    }
}
