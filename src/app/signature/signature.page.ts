import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SignaturePad } from '../lib/signature_pad/signature_pad';
import { AlertController, Platform } from '@ionic/angular';

import { Chooser } from '@ionic-native/chooser/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

import { SignatureService } from './../services/signature.service';
import { GlobalService } from '../services/global.service';
import { WalletService } from '../services/wallet.service';
import { HelperService } from './../services/helper.service';
import { AccountStoreService } from '../services/account-store.service';

@Component({
    selector: 'app-signature',
    templateUrl: './signature.page.html',
    styleUrls: ['./signature.page.scss'],
})

export class SignaturePage implements OnInit {

    isDone: boolean = false;
    signatureImg: string;
    isProcessing: boolean = false;

    onChooser: boolean = false;

    dpr: number = window.devicePixelRatio;
    scale: number = 1;
    isLandscape: boolean = false;
    landscapeW: number = 640;
    landscapeH: number = 336;
    portraitW: number = 360;
    portraitH: number = 616;

    padW: number = 376;
    padH: number = 176;

    basePadW: number = 376;
    basePadH: number = 176;

    signaturePad: SignaturePad;

    constructor(
        private router: Router,
        private platform: Platform,
        private domSanitizer: DomSanitizer,
        private alertController: AlertController,
        private chooser: Chooser,
        private screenOrientation: ScreenOrientation,
        private global: GlobalService,
        private wallet: WalletService,
        private signature: SignatureService,
        private accountStore: AccountStoreService
    ) {
        this.onChooser = !this.global.isBrowser;
    }

    ngOnInit() {
    }

    bypassSecurityTrustUrl = this.domSanitizer.bypassSecurityTrustUrl;

    ionViewWillEnter() {
        console.log(this.platform.width());
        console.log(this.platform.height());
        console.log(this.screenOrientation.type);
        this.isLandscape = this.platform.isLandscape();
        this.isDone = false;
        this.setupSigpad();
        this.signaturePad.clear();
        this.resizeSigPadWhenRotateScreen();
    }

    /**
     * resize SigPad when rotate screen
     */
    resizeSigPadWhenRotateScreen() {
        this.screenOrientation.onChange().subscribe(
            () => {
                if (this.platform.is("android"))
                    this.isLandscape = !this.platform.isLandscape(); // Platform is NOT updated yet at this time
                else
                    this.isLandscape = this.platform.isLandscape();
                console.log(this.screenOrientation.type);
                console.log(this.isLandscape);
                console.log(this.platform.width());
                console.log(this.platform.height());
                if (this.isLandscape) {
                    if (this.platform.is("android")) {
                        this.portraitW = this.platform.width();
                        this.portraitH = this.platform.height();
                    }
                    else {
                        this.landscapeW = this.platform.width();
                        this.landscapeH = this.platform.height();
                    }
                    this.padW = (this.landscapeH - 70) * this.basePadW / this.basePadH;
                    this.padH = (this.landscapeH - 70);
                    document.getElementById("sigpad").style.marginTop = '5px';
                }
                else {
                    if (this.platform.is("android")) {
                        this.landscapeW = this.platform.width();
                        this.landscapeH = this.platform.height();
                    }
                    else {
                        this.portraitW = this.platform.width();
                        this.portraitH = this.platform.height();
                    }
                    this.padW = this.portraitW;
                    this.padH = this.portraitW * this.basePadH / this.basePadW;
                    document.getElementById("sigpad").style.marginTop = '1em';
                }
                console.log('Pad: ' + this.padW + ' x ' + this.padH);

                this.scale = this.padW / this.basePadW;
                this.signaturePad.scale = this.scale;
                this.signaturePad._ctx.canvas.width = this.basePadW * this.dpr;
                this.signaturePad._ctx.canvas.height = this.basePadH * this.dpr;
                this.signaturePad._ctx.scale(this.dpr, this.dpr);
                this.signaturePad._ctx.canvas.style.width = this.padW + 'px';

                this.signaturePad.fromData(this.signaturePad.toData());
            });
    }

    setupSigpad() {
        const sigpadWidth = (this.platform.width() < this.basePadW) ? this.platform.width() : this.basePadW;
        const sigpadHeight = (this.platform.width() < this.basePadW) ? this.platform.width() * this.basePadH / this.basePadW : this.basePadH;
        this.scale = sigpadWidth / this.basePadW;
        const canvas = <HTMLCanvasElement>document.getElementById('signature-pad');
        this.signaturePad = new SignaturePad(canvas);
        this.signaturePad.dpr = this.dpr;
        this.signaturePad.scale = this.scale;
        const ctx = this.signaturePad._ctx;

        ctx.canvas.width = this.basePadW * this.dpr;
        ctx.canvas.height = this.basePadH * this.dpr;
        ctx.scale(this.dpr, this.dpr);
        canvas.style.width = sigpadWidth + 'px';

        canvas.onmouseup = (e) => this.drawComplete();
        canvas.ontouchend = (e) => this.drawComplete();
    }

    /**
     * Run when untouch
     */
    drawComplete() {
        // will be notified of szimek/signature_pad's onEnd event
        // console.log(this.signaturePad.toDataURL('image/png'));
        // console.log(this.signaturePad.toDataURL('image/svg+xml'));

        const svgBase64 = this.signaturePad.toDataURL('image/svg+xml');
        // console.log(svgBase64);
        let svg = HelperService.base64toUtf8(svgBase64.split(';')[1].split(',')[1]);
        // let patterns = [RegExp('width=\"([^"]*)\"'), RegExp('height=\"([^"]*)\"'), RegExp('viewBox=\"([^"]*)\"')]
        // patterns.forEach(pattern => { svg = svg.replace(pattern, '') });

        // let pattern = RegExp('width=\"([^"]*)\"');
        // svg = svg.replace(pattern, 'width="376"');
        // pattern = RegExp('height=\"([^"]*)\"');
        // svg = svg.replace(pattern, 'height="176"');
        // pattern = RegExp('viewBox=\"([^"]*)\"');
        // svg = svg.replace(pattern, 'viewBox="0 0 ' + this.padW + ' ' + this.padH + '"');
        const fullSvgBase64 = HelperService.utf8toBase64(svg);
        this.signatureImg = 'data:image/svg+xml;base64,' + fullSvgBase64;
    }

    /**
     * Run when touch
     */
    drawStart() {
        // will be notified of szimek/signature_pad's onBegin event
        console.log('begin drawing');
    }

    /**
     * Clear signautre pad
     */
    onClear() {
        this.signaturePad.clear();
    }

    /**
     * Done draw sigpad
     */
    onDone() {
        this.isDone = true;
        setTimeout(() => this.drawSignature(), 100);
    }

    drawSignature() {
        console.log(this.dpr);
        const imgCanvas = <HTMLCanvasElement>document.getElementById('signature-img');
        imgCanvas.width = this.basePadW * this.dpr;
        imgCanvas.height = this.basePadH * this.dpr;
        imgCanvas.style.width = '376px';
        imgCanvas.style.maxWidth = '90%';

        const ctx = imgCanvas.getContext('2d');
        ctx.scale(this.dpr, this.dpr);
        ctx.clearRect(0, 0, imgCanvas.width, imgCanvas.height);
        console.log(this.signatureImg);
        const signatureImgObj: HTMLImageElement = document.createElement('img');
        signatureImgObj.src = this.signatureImg;
        signatureImgObj.width = this.basePadW;
        signatureImgObj.height = this.basePadH;
        setTimeout(() => {
            ctx.drawImage(signatureImgObj, 0, 0, signatureImgObj.width, signatureImgObj.height);
        }, 100);
    }

    /**
     * Create wallet and goto sign in
     */
    async createWallet() {
        await this.wallet.createWallet();
        // this.wallet.createNotarizationWallet();
        this.wallet.clearInfo();
        this.global.setIsSignedUp(true);
    }

    /**
     * Back to draw signature
     */
    onBack() {
        this.isDone = false;
        this.isLandscape = this.platform.isLandscape();
        console.log(this.isLandscape);
        setTimeout(() => {
            this.setupSigpad();
            this.signaturePad.clear();
            this.resizeSigPadWhenRotateScreen();
        }, 10);
    }

    /**
     * Confirm signature
     */
    async onConfirm() {
        this.isProcessing = true;
        try {
            // await this.signature.assign(this.signatureImg);
            const owner = this.wallet.getInfoName();
            const { siriusWallet, account } = await this.wallet.createWallet();
            // this.wallet.createNotarizationWallet();
            const docref = await this.accountStore.addAccount(
                this.wallet.preparedInfo.name,
                this.wallet.preparedInfo.pass,
                account.privateKey,
                this.global.networkType,
                this.global.uDid ? this.global.uDid : '',
                siriusWallet.plan,
                this.signatureImg
            ).catch(err => {
                console.log(err);
                this.alertMessage('Failed to create account', err.message);
                return null;
            });
            if (!docref) return;
            await this.signature.storeSignature(owner, this.signatureImg, '');
            // await this.signature.fetchFromStorage();
            // await this.createWallet();
            this.wallet.clearInfo();
            this.global.setIsSignedUp(true);

            this.isProcessing = false;
            this.router.navigate(['sign-in']);
        }
        catch (err) {
            this.alertMessage('Failed to register the signature', err.message);
            this.isProcessing = false;
        }
    }

    /**
     * Open file chooser then return file uri, switch view to file list
     * @async
     */
    browseFile() {
        this.chooser.getFile('image/svg+xml')
            .then(file => {
                if (file) {
                    console.log(file);
                    this.signatureImg = file.dataURI;
                    if (file.mediaType != 'image/svg+xml') {
                        this.alertMessage('Invalid File Type', 'SirisuSign supports .svg file only.');
                        return;
                    }
                    this.isDone = true;
                }
            })
            .catch((e: any) => console.log(e));
    }

    /**
     * Read file by browser
     * @param files
     */
    onFile(files) {
        console.log(files);
        const reader = new FileReader();
        reader.onload = (event) => {
            console.log(event.target);
            const target = event.target as any;
            const mediaType = target.result.split(';')[0].split(':')[1];
            if (mediaType != 'image/svg+xml') {
                this.alertMessage('Invalid File Type', 'SirisuSign supports .svg file only.');
                return;
            }
            this.signatureImg = target.result;
            this.isDone = true;
        };
        reader.readAsDataURL(files[0]);
    }

    /**
     * Present Alert
     * @param message
     */
    async alertMessage(header: string, message: string = '') {
        const alert = await this.alertController.create({
            header: header,
            message: message,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });

        await alert.present();
    }
}
