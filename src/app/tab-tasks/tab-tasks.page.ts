
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import {
    SiriusSignTask,
    SiriusSignSigningTask,
    SiriusSignCosigningTask,
    SiriusSignVerifyTask
} from './../model/siriussign-task.model';

import { MultitaskService } from './../services/multitask.service';
import { GlobalService } from './../services/global.service';

@Component({
    selector: 'app-tab-tasks',
    templateUrl: './tab-tasks.page.html',
    styleUrls: ['./tab-tasks.page.scss'],
})
export class TabTasksPage {

    constructor(
        private router: Router,
        private global: GlobalService,
        public multitasks: MultitaskService
    ) { }

    /**
     * Set stroke dash array of progress circle
     * @param task 
     */
    calStrokeDashArray(task) {
        return task.percent + ', 100';
    }

    /**
     * Open task progress
     * @param task 
     */
    onTask(task: SiriusSignTask) {
        if (task.transactionStatus.isDone) {
            this.multitasks.selectedTask = task;
            if (task instanceof SiriusSignSigningTask)
                this.router.navigate(['new-done']);
            else if (task instanceof SiriusSignCosigningTask)
                this.router.navigate(['cosign-done']);
            else if (task instanceof SiriusSignVerifyTask)
                this.router.navigate(['verify-done']);
        }
    }
}
