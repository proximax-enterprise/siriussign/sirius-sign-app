import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CosignInteractivePage } from './cosign-interactive.page';
import { ComponentModule } from '../components/component.module';
import { ProgressGuardService, ProgressGuardCanDeactiveService } from '../services/guards/progress-guard.service';

const routes: Routes = [
    {
        path: '',
        component: CosignInteractivePage
    },
    {
        path: 'cosign-progress',
        loadChildren: '../cosign-progress/cosign-progress.module#CosignProgressPageModule',
        canActivate: [ProgressGuardService],
        canDeactivate: [ProgressGuardCanDeactiveService]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ComponentModule,
        RouterModule.forChild(routes)
    ],
    declarations: [CosignInteractivePage]
})
export class CosignInteractivePageModule { }
