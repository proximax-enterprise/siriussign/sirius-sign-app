import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewWithOthersPage } from './new-with-others.page';

xdescribe('NewWithOthersPage', () => {
    let component: NewWithOthersPage;
    let fixture: ComponentFixture<NewWithOthersPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NewWithOthersPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NewWithOthersPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
