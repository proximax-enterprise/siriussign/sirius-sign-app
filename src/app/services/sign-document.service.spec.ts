import { TestBed } from '@angular/core/testing';

import { SignDocumentService } from './sign-document.service';

describe('SignDocumentService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: SignDocumentService = TestBed.get(SignDocumentService);
        expect(service).toBeTruthy();
    });
});
