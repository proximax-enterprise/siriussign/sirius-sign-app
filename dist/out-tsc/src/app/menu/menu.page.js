import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
var MenuPage = /** @class */ (function () {
    function MenuPage(menu) {
        this.menu = menu;
    }
    MenuPage.prototype.ngOnInit = function () {
    };
    /*
     *
     */
    MenuPage.prototype.onPrivacy = function () {
    };
    /*
     *
     */
    MenuPage.prototype.onAccount = function () {
    };
    /*
     *
     */
    MenuPage.prototype.onHelp = function () {
    };
    /*
     *
     */
    MenuPage.prototype.onAbout = function () {
    };
    /*
     *
     */
    MenuPage.prototype.onSignOut = function () {
    };
    MenuPage = tslib_1.__decorate([
        Component({
            selector: 'app-menu',
            templateUrl: './menu.page.html',
            styleUrls: ['./menu.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuController])
    ], MenuPage);
    return MenuPage;
}());
export { MenuPage };
//# sourceMappingURL=menu.page.js.map