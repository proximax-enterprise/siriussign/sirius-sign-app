import { SimpleWallet, Address, Crypto, WalletAlgorithm, Account } from 'tsjs-xpx-chain-sdk';
import * as CryptoJS from 'crypto-js';
import { Scrambler } from './../lib/scrambler';
import { HelperService } from './../services/helper.service';

export class SiriusWallet {

    primary: string;
    secondary: string;
    tertiary: string;

    name: string;
    address: Address;
    encryptedPrivateKey: {
        encryptedKey: string,
        iv: string
    };
    plan: number = 0;

    constructor(name, address, encryptedPrivateKey, primary, secondary, tertiary, plan) {
        this.name = name;
        this.address = Address.createFromRawAddress(address.address);
        this.encryptedPrivateKey = encryptedPrivateKey;
        this.primary = primary;
        this.secondary = secondary;
        this.tertiary = tertiary;
        this.plan = plan;
    }

    static createFromSimpleWallet(simpleWallet: SimpleWallet, password: string, plan: number) {
        const name = simpleWallet.name;
        const address = simpleWallet.address;
        const encryptedPrivateKey = {
            encryptedKey: simpleWallet.encryptedPrivateKey.encryptedKey,
            iv: simpleWallet.encryptedPrivateKey.iv
        }
        const primary = HelperService.base64toHex(CryptoJS.AES.encrypt('data', 'password').toString()); //CryptoJS.enc.Base64.parse(CryptoJS.AES.encrypt('data', 'password').toString());
        const secondary = HelperService.base64toHex(CryptoJS.AES.encrypt(primary.toString(), 'password').toString()); //CryptoJS.enc.Base64.parse(CryptoJS.AES.encrypt(primary, 'password').toString());
        const tertiary = CryptoJS.SHA256(password).toString().substr(0, 63);
        return new SiriusWallet(name, address, encryptedPrivateKey, primary, secondary, tertiary, plan);
    }

    static createFromRaw(wallet) {
        const name = wallet.name;
        const address = wallet.address;
        const encryptedPrivateKey = wallet.encryptedPrivateKey;
        const primary = wallet.primary;
        const secondary = wallet.secondary;
        const tertiary = wallet.tertiary;
        const plan = wallet.plan;
        return new SiriusWallet(name, address, encryptedPrivateKey, primary, secondary, tertiary, plan);
    }

    generateCode(password: string) {
        const inAppKey = '4TRlCshHCCnrIxdeyvvW7bSwEgKXThMed0iqOOwIpg8Qx0L7Ds9OqCEFkgIy6Xbu';
        this.tertiary = CryptoJS.SHA256(password).toString();

        try {
            const common = {
                password: password,
                privateKey: '',
            }
            const walletKey = {
                encrypted: this.encryptedPrivateKey.encryptedKey,
                iv: this.encryptedPrivateKey.iv,
            };
            Crypto.passwordToPrivateKey(common, walletKey, WalletAlgorithm.Pass_bip32);
            const account = Account.createFromPrivateKey(common.privateKey, this.address.networkType);
            const scrambledPrivKey = Scrambler.scramble(account.privateKey, this.encryptedPrivateKey.iv);
            const encScrambledPrivKey = CryptoJS.AES.encrypt(scrambledPrivKey, inAppKey).toString();
            const encCode = HelperService.base64toHex(CryptoJS.AES.encrypt(encScrambledPrivKey, this.tertiary).toString());
            const encCodeLen = encCode.length / 2;
            this.primary = encCode.substr(0, encCodeLen);
            this.secondary = encCode.substr(encCodeLen, encCodeLen);
        } catch (err) {
            console.log(err);
        }
    }

    getPrivKey() {
        const inAppKey = '4TRlCshHCCnrIxdeyvvW7bSwEgKXThMed0iqOOwIpg8Qx0L7Ds9OqCEFkgIy6Xbu';
        const encCode = this.primary + this.secondary;
        const encScrambledPrivKey = CryptoJS.AES.decrypt(HelperService.hexToBase64(encCode), this.tertiary).toString(CryptoJS.enc.Utf8);
        const scrambledPrivKey = CryptoJS.AES.decrypt(encScrambledPrivKey, inAppKey).toString(CryptoJS.enc.Utf8);
        const privKey = Scrambler.scramble(scrambledPrivKey, this.encryptedPrivateKey.iv);
        return privKey;
    }
}