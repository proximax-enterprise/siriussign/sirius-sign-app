import { Injectable } from '@angular/core';

import {
    PublicAccount,
    QueryParams,
    AccountHttp,
    Transaction,
    TransferTransaction,
    TransactionType,
    AggregateTransaction,
    AccountRestrictionsInfo,
    RestrictionType,
    Address,
    AccountInfo,
    TransactionHttp
} from 'tsjs-xpx-chain-sdk';

import { SiriusSignDocument, SignerSignature } from '../model/siriussign-document.model';
import { SiriusMessageType, SsDocumentSigningMessage, SsNotifyDocumentCosignMessage } from '../model/siriussign-message.model';

import { GlobalService } from './global.service';
import { HelperService } from './helper.service';
import { SignatureInfo } from './draw-signature.service';

export enum DocumentSigningStatus {
    COMPLETED,
    WAITING,
    NEEDSIGN,
    ANY
}

export enum DocumentVerifyingStatus {
    VERIFIED,
    VERIFYING,
    NEEDVERIFY,
    ANY
}

@Injectable({
    providedIn: 'root'
})
export class DocumentClassificationService {

    completed: SiriusSignDocument[] = [];
    waiting: SiriusSignDocument[] = [];
    needSign: SiriusSignDocument[] = [];

    needVerify: SiriusSignDocument[] = [];
    verifying: SiriusSignDocument[] = [];
    verified: SiriusSignDocument[] = [];

    selectedDocInfo: SiriusSignDocument = null;
    selectedDocStatus: string;

    // Flags to prevent fetch method run multiple times
    isCompletedAndWaitingFetching: boolean = false;
    isNeedSignFetching: boolean = false;

    /** Transaction ID get from universal link */
    universalId: string = '';

    constructor(
        private global: GlobalService
    ) { }

    private fetchConfirmedTransactions(account: PublicAccount, lastId: string) {
        console.log('fetchIncomingTransactions: ' + lastId);
        let querryParams = null;
        if (lastId != null) {
            querryParams = new QueryParams(10, lastId);
        }
        const accountHttp = new AccountHttp(this.global.apiNode);
        return accountHttp.transactions(account, querryParams).toPromise();
    }

    private fetchIncomingTransactions(account: PublicAccount, lastId: string) {
        console.log('fetchIncomingTransactions: ' + lastId);
        let querryParams = null;
        if (lastId != null) {
            querryParams = new QueryParams(10, lastId);
        }
        const accountHttp = new AccountHttp(this.global.apiNode);
        return accountHttp.incomingTransactions(account, querryParams).toPromise();
    }

    private fetchOutgoingTransactions(account: PublicAccount, lastId: string) {
        console.log('fetchOutgoingTransactions: ' + lastId);
        let querryParams = null;
        if (lastId != null) {
            querryParams = new QueryParams(10, lastId);
        }
        const accountHttp = new AccountHttp(this.global.apiNode);
        return accountHttp.outgoingTransactions(account, querryParams).toPromise();
    }

    /**
     * Fetch all incoming transations of an account
     * @param publicAccount
     */
    async fetchAllIncomingTransactions(publicAccount: PublicAccount) {
        const allIncomingTxs = await this.fetchAll(publicAccount, true,
            (publicAccount: PublicAccount, lastId: string) => this.fetchIncomingTransactions(publicAccount, lastId),
            txs => { });
        return allIncomingTxs;
    }

    /**
     * Fetch all outgoing transations of an account
     * @param publicAccount
     */
    async fetchAllOutgoingTransactions(publicAccount: PublicAccount) {
        const allOutgoingTxs = await this.fetchAll(publicAccount, true,
            (publicAccount: PublicAccount, lastId: string) => this.fetchOutgoingTransactions(publicAccount, lastId),
            txs => { });
        return allOutgoingTxs;
    }

    /**
     * Fetch all aggregate complete transaction of an account
     * @param publicAccount
     */
    private async fetchAllAggregateCommpleteTransactions(publicAccount: PublicAccount) {
        let allAggCompleteTxs: AggregateTransaction[] = [];
        await this.fetchAll(publicAccount, false,
            (publicAccount: PublicAccount, lastId: string) => this.fetchConfirmedTransactions(publicAccount, lastId),
            (txs: Transaction[]) => {
                txs.forEach(tx => {
                    if (tx.type == TransactionType.AGGREGATE_COMPLETE)
                        allAggCompleteTxs = [...allAggCompleteTxs, <AggregateTransaction>tx];
                });
            });
        return allAggCompleteTxs;
    }

    /**
     * Fetch all transactions of a kind od transation according to fnFetch
     * @param publicAccount
     * @param isReturnAll return all transactions fetched
     * @param fnFetch
     * @param fnEachFetch
     */
    private async fetchAll(publicAccount: PublicAccount, isReturnAll: boolean, fnFetch, fnEachFetch) {
        let allTxs: Transaction[] = [];
        let lastId = null;
        let isLast = false;
        const loadAll = async () => {
            if (!isLast) {
                const txs: Transaction[] = await fnFetch(publicAccount, lastId)
                    .catch(err => { console.log(err); return null; });
                if (txs) {
                    let last;
                    if (txs.length > 0) last = txs[txs.length - 1].transactionInfo.id;
                    else last = null;
                    isLast = (txs.length < 10) || (last == lastId);
                    lastId = last;

                    fnEachFetch(txs);
                    if (isReturnAll) allTxs = [...allTxs, ...txs];
                }
                else {
                    isLast = true;
                }

                await HelperService.sleep(100);
                await loadAll();
            }
        }
        await loadAll();
        if (isReturnAll) return allTxs;
    }

    /**
     * Fetch transaction by hash
     * @param hash
     */
    fetchTransaction(hash: string) {
        const transactionHttp = new TransactionHttp(this.global.apiNode);
        return transactionHttp.getTransaction(hash).toPromise();
    }

    /**
     * Filter Document Signing Notifications from an array of Aggregate Commplete Transactions
     * @param aggCompleteTxs
     */
    filterDocumentSigningNotifications(aggCompleteTxs: AggregateTransaction[]) {
        let documentSigningNotifications: AggregateTransaction[] = [];
        aggCompleteTxs.forEach(aggTx => {
            const checkTx = aggTx.innerTransactions[0];
            let isNotification = false;
            if (checkTx.type == TransactionType.TRANSFER) {
                const info = HelperService.parseSsMessage((<TransferTransaction>checkTx).message.payload);
                if (info && (info.header.appCode == this.global.appCodeName) && (info.header.messageType == SiriusMessageType.SIGN_NOTIFY))
                    isNotification = true;
            }

            if (isNotification) {
                documentSigningNotifications.push(aggTx);
            }
        });
        return documentSigningNotifications;
    }

    /**
     * Fetch all document signing notifications
     */
    async fetchDocumentSigningNotifications() {
        // Fetch all incomming transaction of user account
        let allAggCompleteTxs: AggregateTransaction[] = await this.fetchAllAggregateCommpleteTransactions(this.global.loggedAccount.publicAccount);
        // Filter document signing notificaion transaction - Aggregate Complete Tx with NOTIFY TransferTx inner
        const documentSigningNotifications = this.filterDocumentSigningNotifications(allAggCompleteTxs);
        return documentSigningNotifications;
    }

    /**
     * Fetch signer public key, address
     * @param address document address
     */
    async fetchDocumentSignersInfoFromDocumentAccount(address: Address) {
        const accountHttp = new AccountHttp(this.global.apiNode);
        const docAccRestriction: AccountRestrictionsInfo = await accountHttp.getAccountRestrictions(address).toPromise()
            .catch(err => { console.log(err); return null; });
        const docAccAllow = docAccRestriction.accountRestrictions.restrictions
            .filter(restriction => restriction.restrictionType == RestrictionType.AllowAddress)[0];
        const doccAccAllowAddresses = docAccAllow.values;

        // TODO: distinguish signers and verifiers

        const signersInfo = await accountHttp.getAccountsInfo(<Address[]>doccAccAllowAddresses).toPromise();
        const signers = signersInfo.map(signerInfo => { return { publicKey: signerInfo.publicKey, address: signerInfo.address }; });

        return signers;
    }

    /**
     * Fetch signer public key, address of cosigners and verifiers from agg noti tx
     * @param aggNotiTx
     */
    async fetchDoucmentSignersAndVerifiersFromNotiTx(aggNotiTx: AggregateTransaction) {
        const accountHttp = new AccountHttp(this.global.apiNode);
        const notiTxs = aggNotiTx.innerTransactions.map(tx => <TransferTransaction>tx);

        const recvAddresses = notiTxs.map(tx => tx.recipient);
        const recvInfos = await accountHttp.getAccountsInfo(<Address[]>recvAddresses).toPromise();
        const recvs = recvInfos.map(info => { return { publicKey: info.publicKey, address: info.address }; })

        let cosignerAddresses: Address[] = [];
        let verifierAddresses: Address[] = [];
        notiTxs.forEach(tx => {
            const ssMsg = HelperService.parseSsMessage(tx.message.payload);
            if (ssMsg.header.messageType == SiriusMessageType.SIGN_NOTIFY) cosignerAddresses.push(<Address>tx.recipient);
            if (ssMsg.header.messageType == SiriusMessageType.VERYFY_NOTIFY) verifierAddresses.push(<Address>tx.recipient);
        });

        const cosigners = recvs.filter(recv => cosignerAddresses.map(addr => addr.plain()).includes(recv.address.plain()));
        const verifiers = recvs.filter(recv => verifierAddresses.map(addr => addr.plain()).includes(recv.address.plain()));

        return [cosigners, verifiers]
    }

    /**
     * Fetch document public account of address
     * @param address
     */
    async fetchDocumentPublicAccount(address: Address) {
        const accountHttp = new AccountHttp(this.global.apiNode);
        const docAccInfo: AccountInfo = await accountHttp.getAccountInfo(address).toPromise()
            .catch(err => { console.log(err); return null; });
        const docAccPublicKey = docAccInfo.publicKey;
        const docPublicAcc = PublicAccount.createFromPublicKey(docAccPublicKey, this.global.networkType);
        return docPublicAcc;
    }


    /**
     * Get signatures info
     * @param notiAggTx
     * @param signersInfo
     * @param ownerPublicKey
     * @param ownerSigningMsg
     */
    getSignaturesInfo(
        notiAggTx: AggregateTransaction,
        signersInfo: { publicKey: string, address: Address }[],
        ownerPublicKey: string,
        ownerSigningMsg: SsDocumentSigningMessage
    ) {
        let signaturesInfo: SignatureInfo[] = [];
        if (notiAggTx) {
            signaturesInfo = notiAggTx.innerTransactions.filter(innerTx => {
                const notiTx = <TransferTransaction>innerTx;
                const notiMsg = HelperService.parseSsMessage(notiTx.message.payload);
                return notiMsg.header.messageType == SiriusMessageType.SIGN_NOTIFY;
            }).map(innerTx => {
                const notiTx = <TransferTransaction>innerTx;
                const notiMsg = <SsNotifyDocumentCosignMessage>HelperService.parseSsMessage(notiTx.message.payload);
                let publicKey = signersInfo.filter(signer => signer.address.equals(<Address>notiTx.recipient))[0].publicKey;
                if (publicKey == '0000000000000000000000000000000000000000000000000000000000000000'){
                    publicKey = signersInfo.filter(signer => signer.address.equals(<Address>notiTx.recipient))[0].address.plain();
                    publicKey = (publicKey == this.global.loggedAccount.address.plain()) ? this.global.loggedAccount.publicKey : publicKey;
                }
                const signatureInfo: SignatureInfo = {
                    publicKey: publicKey,
                    name: notiMsg.body.signaturePositions[0].name,
                    signaturePositions: notiMsg.body.signaturePositions
                    // {
                    //     name: notiMsg.body.signaturePosition.name,
                    //     page: notiMsg.body.signaturePosition.page,
                    //     x: notiMsg.body.signaturePosition.x,
                    //     y: notiMsg.body.signaturePosition.y,
                    //     namePosition: notiMsg.body.signaturePosition.namePosition
                    // }
                }
                return signatureInfo;
            });
        }
        const ownerSignatureInfo: SignatureInfo = {
            publicKey: ownerPublicKey,
            name: ownerSigningMsg.body.signaturePositions[0].name,
            signaturePositions: ownerSigningMsg.body.signaturePositions
            // {
            //     name: ownerSigningMsg.body.signaturePosition.name,
            //     page: ownerSigningMsg.body.signaturePosition.page,
            //     x: ownerSigningMsg.body.signaturePosition.x,
            //     y: ownerSigningMsg.body.signaturePosition.y,
            //     namePosition: ownerSigningMsg.body.signaturePosition.namePosition
            // }
        }
        signaturesInfo.push(ownerSignatureInfo);
        return signaturesInfo;
    }

    /**
     * Get signed info: signer pulickey, verified publickey, owner info, signature upload tx hash
     * from incoming transactions of a document account
     * @param incomingTxs
     */
    getSignedInfo(incomingTxs: Transaction[]) {
        // Filter SIGN transactions to get signatures
        // let signatures: string[] = [];       // Signed publickey
        let verifierSignatures: string[] = [];  // Verified publickey
        let owner: string;                      // Owner public key
        let ownerSigningMsg: SsDocumentSigningMessage;
        let ownerSigningTx: TransferTransaction;
        // let signaturesUploadTxHash: string[] = [];
        let userSigningTx: TransferTransaction = null;
        let signatureUploadTxHashesInfo: {
            publicKey: string,
            signatureUploadTxHash: string
        }[] = [];
        incomingTxs.forEach(tx => {
            if (tx.type == TransactionType.TRANSFER) {
                const transferTx = <TransferTransaction>tx;
                const txMsg = <SsDocumentSigningMessage>HelperService.parseSsMessage(transferTx.message.payload);
                if (txMsg && (txMsg.header.messageType == SiriusMessageType.SIGN)) {
                    // signatures.push(transferTx.signer.publicKey);
                    // signaturesUploadTxHash.push(txMsg.body.signatureUploadTxHash);
                    signatureUploadTxHashesInfo.push({
                        publicKey: transferTx.signer.publicKey,
                        signatureUploadTxHash: txMsg.body.signatureUploadTxHash
                    });
                    if (txMsg.body.isOwner) {
                        owner = transferTx.signer.publicKey;
                        ownerSigningMsg = txMsg;
                        ownerSigningTx = transferTx;
                    }
                    if (transferTx.signer.publicKey == this.global.loggedAccount.publicKey)
                        userSigningTx = transferTx;
                }
                if (txMsg && (txMsg.header.messageType == SiriusMessageType.VERIFY)) {
                    verifierSignatures.push(transferTx.signer.publicKey);
                }
            }
        });
        return {
            // signatures: signatures,
            verifierSignatures: verifierSignatures,
            owner: owner,
            ownerSigningMsg: ownerSigningMsg,
            ownerSigningTx: ownerSigningTx,
            // signaturesUploadTxHash: signaturesUploadTxHash,
            userSigningTx: userSigningTx,
            signatureUploadTxHashesInfo: signatureUploadTxHashesInfo
        }
    }

    /**
     * Fetch all document that need user to sign
     */
    async fetchNeedSignDocs() {
        if (this.isNeedSignFetching) return;
        this.isNeedSignFetching = true;
        let needSign: SiriusSignDocument[] = [];
        let needVerify: SiriusSignDocument[] = [];
        const notiAggTxs: AggregateTransaction[] = await this.fetchDocumentSigningNotifications().catch(err => { 
            console.log(err);
            this.isNeedSignFetching = false;
            return []; 
        });
        const fetchNeedSign = async () => HelperService.asyncForEach(notiAggTxs, async (notiAggTx: AggregateTransaction, index: number) => {
            // notiAggTx.forEach(async notiAggTx => {
            //Fetch all incomming transactions of document account
            const docPublicAcc = await this.fetchDocumentPublicAccount(notiAggTx.signer.address);
            const incomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);

            // Filter SIGN transactions to get signatures
            const signedInfo                  = this.getSignedInfo(incomingTxs);
            const verifierSignatures          = signedInfo.verifierSignatures;
            const owner                       = signedInfo.owner;
            const ownerSigningMsg             = signedInfo.ownerSigningMsg;
            const signatureUploadTxHashesInfo = signedInfo.signatureUploadTxHashesInfo;
            const signatures                  = signatureUploadTxHashesInfo.map(info => info.publicKey);
            // const signaturesUploadTxHash      = signatureUploadTxHashesInfo.map(info => info.signaturesUploadTxHash);

            // const userSignature = signatures.filter(signature => signature == this.global.loggedAccount.publicKey);
            // const isSigned = userSignature.length > 0;
            const isSigned = [owner, ...signatures, ...verifierSignatures].includes(this.global.loggedAccount.publicKey);

            if (!isSigned) {
                // Check cosigners
                // const signersInfo = await this.fetchDocumentSignersInfo(notiAggTx.signer.address)
                // const cosigners = signersInfo.map(signer => signer.publicKey).filter(cosigner => cosigner != owner);
                let cosignerPublicKeys = [];
                let verifierPublicKeys = [];
                let cosignerAddresses = [];
                let verifierAddresses = [];
                let cosignerInfos = [];
                let verifierInfos = [];
                if (notiAggTx) {
                    [cosignerInfos, verifierInfos] = await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx)
                        .catch(err => {
                            console.log(err);
                            return [[], []];
                        });
                    if ((cosignerInfos.length == 0) && (verifierInfos.length == 0)) return;
                    cosignerPublicKeys = cosignerInfos.map((info: { publicKey: string, address: Address }) => info.publicKey);
                    verifierPublicKeys = verifierInfos.map((info: { publicKey: string, address: Address }) => info.publicKey);
                    cosignerAddresses  = cosignerInfos.map((info: { publicKey: string, address: Address }) => info.address.plain());
                    verifierAddresses  = verifierInfos.map((info: { publicKey: string, address: Address }) => info.address.plain());
                }
                const cosigners = cosignerPublicKeys.map((key, index) => {
                    let cosigner = key;
                    if (key != '0000000000000000000000000000000000000000000000000000000000000000') return key;
                    else cosigner = cosignerAddresses[index];
                    if (cosigner == this.global.loggedAccount.address.plain()) cosigner = this.global.loggedAccount.publicKey;
                    return cosigner;
                });
                const verifiers = verifierPublicKeys.map((key, index) => {
                    let verifier = key;
                    if (key != '0000000000000000000000000000000000000000000000000000000000000000') return key;
                    else verifier = verifierAddresses[index];
                    if (verifier == this.global.loggedAccount.address.plain()) verifier = this.global.loggedAccount.publicKey;
                    return verifier;
                });
                
                const cosignatures = signatures.filter(cosignature => cosignature != owner);

                const signaturesInfo = this.getSignaturesInfo(notiAggTx, cosignerInfos, owner, ownerSigningMsg);
                const signerSignatures = signaturesInfo.map(info => {
                    const indexOfSignatureHash = signatures.indexOf(info.publicKey);
                    const signerSignature: SignerSignature = {
                        publicKey: info.publicKey,
                        signaturePositions: info.signaturePositions,
                        signatureUploadTxHash: (indexOfSignatureHash != -1) ? signatureUploadTxHashesInfo[indexOfSignatureHash].signatureUploadTxHash : ''
                    }
                    return signerSignature;
                });

                const uploadDate = HelperService.getDateFromLocalTime(notiAggTx.deadline.value, -2);

                const doc = SiriusSignDocument.create(
                    notiAggTx.transactionInfo.id,
                    ownerSigningMsg.body.fileName,
                    ownerSigningMsg.body.fileHash,
                    uploadDate,
                    owner,
                    isSigned,
                    null,
                    cosigners,
                    cosignatures,
                    notiAggTx.signer,
                    null,
                    ownerSigningMsg.body.uploadTxHash,
                    ownerSigningMsg.body.isEncrypt,
                    // signaturesInfo,
                    // signaturesUploadTxHash,
                    signerSignatures,
                    verifiers,
                    verifierSignatures,
                    null
                );

                const isNeedSign   = cosignerAddresses.includes(this.global.loggedAccount.address.plain());
                const isNeedVerify = verifierAddresses.includes(this.global.loggedAccount.address.plain());
                if (isNeedSign) needSign.push(doc);
                if (isNeedVerify) needVerify.push(doc);
            }

            await HelperService.sleep(200);
            if (index > 0 && (index % 2) == 0) {
                await HelperService.sleep(200);
            }
        }, () => !this.global.isLoggedIn);
        await fetchNeedSign().catch(err => {
            console.log(err);
            this.isNeedSignFetching = false;
        });
        if (!this.global.isLoggedIn) return;
        // this.needSign = needSign;
        // this.needSign = [];
        // needSign.forEach((doc, i) => setTimeout(() => this.needSign.push(doc), 200 * i));
        this.isNeedSignFetching = false;
        return [needSign, needVerify];
    }

    async fetchAllUserDocumentSigningTransactions() {
        let allDocSigningTxs: TransferTransaction[] = [];
        await this.fetchAll(this.global.loggedAccount.publicAccount, false,
            (publicAccount: PublicAccount, lastId: string) => this.fetchOutgoingTransactions(publicAccount, lastId),
            (txs: Transaction[]) => {
                txs.forEach(tx => {
                    if (tx.type == TransactionType.TRANSFER) {
                        const info = HelperService.parseSsMessage((<TransferTransaction>tx).message.payload);
                        if (info && (info.header.appCode == this.global.appCodeName) &&
                            ((info.header.messageType == SiriusMessageType.SIGN) || (info.header.messageType == SiriusMessageType.VERIFY))) {
                            allDocSigningTxs = [...allDocSigningTxs, <TransferTransaction>tx];
                        }
                    }
                });
            })
        return allDocSigningTxs;
    }

    async fetchAllDocumentSigningTransactionsByDocumentAccount(documentPublicKey: string) {
        const documentPublicAccount = PublicAccount.createFromPublicKey(documentPublicKey, this.global.networkType);
        let allDocSigningTxs: TransferTransaction[] = [];
        await this.fetchAll(documentPublicAccount, false,
            (publicAccount: PublicAccount, lastId: string) => this.fetchIncomingTransactions(publicAccount, lastId),
            (txs: Transaction[]) => {
                txs.forEach(tx => {
                    if (tx.type == TransactionType.TRANSFER) {
                        const info = HelperService.parseSsMessage((<TransferTransaction>tx).message.payload);
                        if (info && (info.header.appCode == this.global.appCodeName) &&
                            ((info.header.messageType == SiriusMessageType.SIGN))) {
                            allDocSigningTxs = [...allDocSigningTxs, <TransferTransaction>tx];
                        }
                    }
                });
            })
        return allDocSigningTxs;
    }

    /**
     * Fetch all compeleted and waiting documents
     */
    async fetchCompletedAndWaiting() {
        if (this.isCompletedAndWaitingFetching) return;
        this.isCompletedAndWaitingFetching = true;
        let waiting: SiriusSignDocument[] = [];
        let completed: SiriusSignDocument[] = [];
        let verifying: SiriusSignDocument[] = [];
        let verified: SiriusSignDocument[] = [];
        const signTxs = await this.fetchAllUserDocumentSigningTransactions();
        const fetchDocuments = async () => {
            await HelperService.asyncForEach(signTxs, async (tx: TransferTransaction, index: number) => {
                const info = <SsDocumentSigningMessage>HelperService.parseSsMessage((<TransferTransaction>tx).message.payload);
                const docPublicAcc = await this.fetchDocumentPublicAccount(<Address>tx.recipient);

                // Fetch noti agg transaction
                const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
                const docAggTxs = docOutgoingTxs
                    .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
                    .map(aggTx => <AggregateTransaction>aggTx);
                const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);

                // Fetch cosigners and verifiers
                // const signersInfo = await this.fetchDocumentSignersInfoFromDocumentAccount(<Address>tx.recipient);
                const notiAggTx = notiAggTxs[0];
                let cosigners = [];
                let verifiers = [];
                let cosignerInfos = [];
                let verifierInfos = [];
                if (notiAggTx) {
                    [cosignerInfos, verifierInfos] = await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx);
                    cosigners = cosignerInfos.map(info => info.publicKey);
                    verifiers = verifierInfos.map(info => info.publicKey);
                }

                //Fetch all incomming transactions of document account
                const incomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);

                // Filter SIGN and VERIFY transactions to get signatures
                const signedInfo                  = this.getSignedInfo(incomingTxs);
                const verifierSignatures          = signedInfo.verifierSignatures;
                const owner                       = signedInfo.owner;
                const ownerSigningTx              = signedInfo.ownerSigningTx;
                const ownerSigningMsg             = signedInfo.ownerSigningMsg;
                const signatureUploadTxHashesInfo = signedInfo.signatureUploadTxHashesInfo;
                const signatures                  = signatureUploadTxHashesInfo.map(info => info.publicKey);
                // const signaturesUploadTxHash      = signatureUploadTxHashesInfo.map(info => info.signaturesUploadTxHash);

                // const cosigners = signersInfo.map(signer => signer.publicKey).filter(cosigner => cosigner != owner);
                const cosignatures = signatures.filter(cosignature => cosignature != owner);
                // const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
                // const docAggTxs = docOutgoingTxs
                //     .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
                //     .map(aggTx => <AggregateTransaction>aggTx);
                // const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);
                // if (notiAggTxs.length != 1) return;

                // Get signatures info
                // const notiAggTx = notiAggTxs[0];
                const signaturesInfo = this.getSignaturesInfo(notiAggTx, cosignerInfos, owner, ownerSigningMsg);

                const uploadDate = HelperService.getDateFromLocalTime(ownerSigningTx.deadline.value, -2, ownerSigningMsg.meta['timestamp']);
                const signDate = HelperService.getDateFromLocalTime(tx.deadline.value, -2, info.meta['timestamp']);

                const isSigned = [owner, ...cosignatures, ...verifierSignatures].includes(this.global.loggedAccount.publicKey);
                const signerSignatures = signaturesInfo.map(info => {
                    const indexOfSignatureHash = signatures.indexOf(info.publicKey);
                    const signerSignature: SignerSignature = {
                        publicKey: info.publicKey,
                        signaturePositions: info.signaturePositions,
                        signatureUploadTxHash: (indexOfSignatureHash != -1) ? signatureUploadTxHashesInfo[indexOfSignatureHash].signatureUploadTxHash : ''
                    }
                    return signerSignature;
                });

                const doc = SiriusSignDocument.create(
                    tx.transactionInfo.id,
                    ownerSigningMsg.body.fileName,
                    ownerSigningMsg.body.fileHash,
                    uploadDate,
                    owner,
                    isSigned,
                    signDate,
                    cosigners,
                    cosignatures,
                    docPublicAcc,
                    tx.transactionInfo.hash,
                    ownerSigningMsg.body.uploadTxHash,
                    ownerSigningMsg.body.isEncrypt,
                    // signaturesInfo,
                    // signaturesUploadTxHash,
                    signerSignatures,
                    verifiers,
                    verifierSignatures,
                    null
                );
                const isSigner = [owner, ...cosigners].includes(this.global.loggedAccount.publicKey);
                const isVerifier = verifiers.includes(this.global.loggedAccount.publicKey);
                if (isSigner) {
                    if (cosignatures.length == cosigners.length) completed.push(doc);
                    else waiting.push(doc);
                }
                if (isVerifier) {
                    if (verifierSignatures.length == verifiers.length) verified.push(doc);
                    else verifying.push(doc);
                }

                await HelperService.sleep(200);
                if (index > 0 && (index % 2) == 0) {
                    await HelperService.sleep(200);
                }
            }, () => !this.global.isLoggedIn);
        }
        await fetchDocuments().catch(err => {
            console.log(err);
            this.isCompletedAndWaitingFetching = false;
        });
        if (!this.global.isLoggedIn) return;
        this.waiting = waiting;
        this.completed = completed;
        this.verifying = verifying;
        this.verified = verified;
        // this.waiting = [];
        // this.completed = [];
        // waiting.forEach((doc, i) => setTimeout(() => this.waiting.push(doc), 200 * i));
        // completed.forEach((doc, i) => setTimeout(() => this.completed.push(doc), 200 * i));
        this.isCompletedAndWaitingFetching = false;
    }

    /**
     * Fetch a Sirius Sign document by signing transction hash
     * @param hash
     */
    async fetchDocumentBySigningTxHash(hash: string) {
        const signTx: Transaction = await this.fetchTransaction(hash).catch(err => { return null; });
        if (!signTx) return null;
        if (signTx.type != TransactionType.TRANSFER) return null;
        const tx = <TransferTransaction>signTx;
        const info = <SsDocumentSigningMessage>HelperService.parseSsMessage(tx.message.payload);
        const docPublicAcc = await this.fetchDocumentPublicAccount(<Address>tx.recipient);

        // Fetch all outgoing transactions of document account and filter NOTI transaction to get signers and verifiers
        const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
        const docAggTxs = docOutgoingTxs
            .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
            .map(aggTx => <AggregateTransaction>aggTx);
        const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);
        // const signersInfo = await this.fetchDocumentSignersInfo(<Address>tx.recipient);
        console.log(notiAggTxs);
        const notiAggTx = notiAggTxs[0];
        let cosigners = [];
        let verifiers = [];
        let cosignerInfos = [];
        let verifierInfos = [];
        if (notiAggTx) {
            [cosignerInfos, verifierInfos] = await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx);
            cosigners = cosignerInfos.map(info => info.publicKey);
            verifiers = verifierInfos.map(info => info.publicKey);
        }
        // Fetch all incomming transactions of document account and filter SIGN and VERIFY transactions to get signatures
        const docIncomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);
        const signedInfo = this.getSignedInfo(docIncomingTxs);

        const verifierSignatures          = signedInfo.verifierSignatures;
        const owner                       = signedInfo.owner;
        const ownerSigningTx              = signedInfo.ownerSigningTx;
        const ownerSigningMsg             = signedInfo.ownerSigningMsg;
        const signatureUploadTxHashesInfo = signedInfo.signatureUploadTxHashesInfo;
        const signatures                  = signatureUploadTxHashesInfo.map(info => info.publicKey);
        // const signaturesUploadTxHash      = signatureUploadTxHashesInfo.map(info => info.signaturesUploadTxHash);

        // const cosigners = signersInfo.map(signer => signer.publicKey).filter(cosigner => cosigner != owner);
        const cosignatures = signatures.filter(cosignature => cosignature != owner);

        // Get signatures info
        const signaturesInfo = this.getSignaturesInfo(notiAggTx, cosignerInfos, owner, ownerSigningMsg);

        const uploadDate = HelperService.getDateFromLocalTime(ownerSigningTx.deadline.value, -2, ownerSigningMsg.meta['timestamp']);
        const signDate = HelperService.getDateFromLocalTime(tx.deadline.value, -2, info.meta['timestamp']);

        const isSigned = [owner, ...cosignatures, ...verifierSignatures].includes(this.global.loggedAccount.publicKey);
        const signerSignatures = signaturesInfo.map(info => {
            const indexOfSignatureHash = signatures.indexOf(info.publicKey);
            const signerSignature: SignerSignature = {
                publicKey: info.publicKey,
                signaturePositions: info.signaturePositions,
                signatureUploadTxHash: (indexOfSignatureHash != -1) ? signatureUploadTxHashesInfo[indexOfSignatureHash].signatureUploadTxHash : ''
            }
            return signerSignature;
        });

        const doc = SiriusSignDocument.create(
            tx.transactionInfo.id,
            ownerSigningMsg.body.fileName,
            ownerSigningMsg.body.fileHash,
            uploadDate,
            owner,
            isSigned,
            signDate,
            cosigners,
            cosignatures,
            docPublicAcc,
            tx.transactionInfo.hash,
            ownerSigningMsg.body.uploadTxHash,
            ownerSigningMsg.body.isEncrypt,
            // signaturesInfo,
            // signaturesUploadTxHash,
            signerSignatures,
            verifiers,
            verifierSignatures,
            null
        );
        return doc;
    }

    /**
     * Fetch a Sirius Sign document by signing transction hash
     * @param hash
     */
    async fetchDocumentByDocumentAccount(documentPublicKey: string) {
        const docPublicAcc = PublicAccount.createFromPublicKey(documentPublicKey, this.global.networkType);

        // Fetch all outgoing transactions of document account and filter NOTI transaction to get signers and verifiers
        const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
        const docAggTxs = docOutgoingTxs
            .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
            .map(aggTx => <AggregateTransaction>aggTx);
        const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);
        // const signersInfo = await this.fetchDocumentSignersInfo(<Address>tx.recipient);
        console.log(notiAggTxs);
        const notiAggTx = notiAggTxs[0];
        let cosigners = [];
        let verifiers = [];
        let cosignerInfos = [];
        let verifierInfos = [];
        if (notiAggTx) {
            [cosignerInfos, verifierInfos] = await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx);
            cosigners = cosignerInfos.map(info => info.publicKey);
            verifiers = verifierInfos.map(info => info.publicKey);
        }
        // Fetch all incomming transactions of document account and filter SIGN and VERIFY transactions to get signatures
        const docIncomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);
        const signedInfo = this.getSignedInfo(docIncomingTxs);

        const verifierSignatures          = signedInfo.verifierSignatures;
        const owner                       = signedInfo.owner;
        const ownerSigningTx              = signedInfo.ownerSigningTx;
        const ownerSigningMsg             = signedInfo.ownerSigningMsg;
        const userSigningTx               = signedInfo.userSigningTx;
        const signatureUploadTxHashesInfo = signedInfo.signatureUploadTxHashesInfo;
        const signatures                  = signatureUploadTxHashesInfo.map(info => info.publicKey);
        // const signaturesUploadTxHash      = signatureUploadTxHashesInfo.map(info => info.signaturesUploadTxHash);

        // const cosigners = signersInfo.map(signer => signer.publicKey).filter(cosigner => cosigner != owner);
        const cosignatures = signatures.filter(cosignature => cosignature != owner);

        // Get signatures info
        const signaturesInfo = this.getSignaturesInfo(notiAggTx, cosignerInfos, owner, ownerSigningMsg);

        const uploadDate = HelperService.getDateFromLocalTime(ownerSigningTx.deadline.value, -2, ownerSigningMsg.meta['timestamp']);
        const signDate = HelperService.getDateFromLocalTime(ownerSigningTx.deadline.value, -2, ownerSigningMsg.meta['timestamp']);

        const isSigned = [owner, ...cosignatures, ...verifierSignatures].includes(this.global.loggedAccount.publicKey);
        const signerSignatures = signaturesInfo.map(info => {
            const indexOfSignatureHash = signatures.indexOf(info.publicKey);
            const signerSignature: SignerSignature = {
                publicKey: info.publicKey,
                signaturePositions: info.signaturePositions,
                signatureUploadTxHash: (indexOfSignatureHash != -1) ? signatureUploadTxHashesInfo[indexOfSignatureHash].signatureUploadTxHash : ''
            }
            return signerSignature;
        });

        const doc = SiriusSignDocument.create(
            ownerSigningTx.transactionInfo.id,
            ownerSigningMsg.body.fileName,
            ownerSigningMsg.body.fileHash,
            uploadDate,
            owner,
            isSigned,
            signDate,
            cosigners,
            cosignatures,
            docPublicAcc,
            (userSigningTx) ? userSigningTx.transactionInfo.hash : null,
            ownerSigningMsg.body.uploadTxHash,
            ownerSigningMsg.body.isEncrypt,
            // signaturesInfo,
            // signaturesUploadTxHash,
            signerSignatures,
            verifiers,
            verifierSignatures,
            null
        );
        return doc;
    }

    /**
     * Fetch documents that currently is in waiting list
     */
    async fetchWaitingToUpdate() {
        let updateDocs = [];
        const fetchWaiting = async () => HelperService.asyncForEach(this.waiting, async (doc: SiriusSignDocument) => {
            const updateDoc = await this.fetchDocumentByDocumentAccount(doc.documentAccount.publicKey)
                .catch(err => null);
            updateDocs.push(updateDoc);
        });
        await fetchWaiting();
        return updateDocs;
    }

    /**
     * Fetch the completely signed document transaction upload hash
     * @param documentPublicKey
     */
    async fetchCompletedFileTxUploadHash(documentPublicKey: string) {
        const documentPublicAccount = PublicAccount.createFromPublicKey(documentPublicKey, this.global.networkType);
        const incomingTxs = await this.fetchAllIncomingTransactions(documentPublicAccount);
        let uploadTxHash: string = '';
        let checkDone: boolean = false;
        incomingTxs.filter(tx => tx.type == TransactionType.TRANSFER).forEach((tx: TransferTransaction) => {
            if (checkDone) return;
            let parsedMsg;
            try {
                parsedMsg = JSON.parse(tx.message.payload);
                if (parsedMsg.data && parsedMsg.data.description && parsedMsg.data.description == 'SiriusSign Signed Document') {
                    uploadTxHash = tx.transactionInfo.hash;
                    checkDone = true;
                    return;
                }
            }
            catch (err) {
                return;
            }
        });
        if (uploadTxHash == '') throw new Error('No completely signed document uploaded');
        return uploadTxHash;
    }

    /**
     * Classify a fetched Sirius Sign Document
     * @param document
     */
    classify(document: SiriusSignDocument) {
        if (!document) return;
        const isVerifier = document.verifiers.includes(this.global.loggedAccount.publicKey);
        if (isVerifier) {
            if (!document.isSigned) this.needVerify.push(document);
            else if (document.verifierSignatures.length < document.verifiers.length) this.verifying.push(document);
            if (document.verifierSignatures.length == document.verifiers.length) this.verified.push(document);
        }
        else {
            if (!document.isSigned) this.needSign.push(document);
            else if (document.cosignatures.length < document.cosigners.length) this.waiting.push(document);
            if (document.cosignatures.length == document.cosigners.length) this.completed.push(document);
        }
    }

    /**
     * Update info of a document on waiting list
     */
    updateWaiting(document: SiriusSignDocument) {
        const oldDocIndex = this.waiting.map(doc => doc.documentAccount.publicKey).indexOf(document.documentAccount.publicKey)
        if (oldDocIndex == -1) throw new Error('Document not exist');
        const isCompleted = (document.cosignatures.length == document.cosigners.length);
        if (isCompleted) {
            this.waiting.splice(oldDocIndex, 1);
            this.completed.push(document);
        }
        else {
            this.waiting[oldDocIndex] = document;
        }
    }

    /**
     * Get document by Id
     * @param id
     */
    getDocById(id: string, flag: DocumentSigningStatus) {
        const documents =
            (flag == DocumentSigningStatus.COMPLETED) ? this.completed :
                (flag == DocumentSigningStatus.WAITING) ? this.waiting :
                    (flag == DocumentSigningStatus.NEEDSIGN) ? this.needSign :
                        [...this.completed, ...this.waiting, ...this.needSign];
        let ids = documents.map(doc => doc.id);
        let index = ids.indexOf(id);
        if (index != -1) return documents[index];
        else throw new Error("No documents have the given id.")
    }

    /**
     * Get document by index
     * @param index
     */
    getDocByIndex(index: number, flag: DocumentSigningStatus) {
        const documents =
            (flag == DocumentSigningStatus.COMPLETED) ? this.completed :
                (flag == DocumentSigningStatus.WAITING) ? this.waiting :
                    (flag == DocumentSigningStatus.NEEDSIGN) ? this.needSign :
                        [...this.completed, ...this.waiting, ...this.needSign];
        if (index <= documents.length) return documents[index];
        else throw new Error("Out of range index.")
    }

    /**
     * Get document by Document account public key
     * @param publicKey
     */
    getDocByDocumentAccount(publicKey: string, flag: DocumentSigningStatus) {
        const documents =
            (flag == DocumentSigningStatus.COMPLETED) ? this.completed :
                (flag == DocumentSigningStatus.WAITING) ? this.waiting :
                    (flag == DocumentSigningStatus.NEEDSIGN) ? this.needSign :
                        [...this.completed, ...this.waiting, ...this.needSign];
        let publicKeys = documents.map(doc => doc.documentAccount.publicKey);
        let index = publicKeys.indexOf(publicKey);
        if (index != -1) return documents[index];
        else throw new Error("No documents have the given id.")
    }

    /**
     * Get document by index
     * @param index
     */
    getVerifyDocByIndex(index: number, flag: DocumentVerifyingStatus) {
        const documents =
            (flag == DocumentVerifyingStatus.VERIFIED) ? this.verified :
                (flag == DocumentVerifyingStatus.VERIFYING) ? this.verifying :
                    (flag == DocumentVerifyingStatus.NEEDVERIFY) ? this.needVerify :
                        [...this.verified, ...this.verifying, ...this.needVerify];
        if (index <= documents.length) return documents[index];
        else throw new Error("Out of range index.")
    }

    /**
     * Sort document in descending order of id/time
     */
    sortDocs(flag: 'completed' | 'waiting' | 'needSign' | 'all' | 'compledAndWaiting') {
        const sortCondition = (a: SiriusSignDocument, b: SiriusSignDocument) => {
            if (a.id > b.id) return -1;
            else return 1;
        }
        if (flag == 'all' || flag == 'needSign')
            this.needSign.sort((a, b) => sortCondition(a, b));
        if (flag == 'all' || flag == 'waiting' || flag == 'compledAndWaiting')
            this.waiting.sort((a, b) => sortCondition(a, b));
        if (flag == 'all' || flag == 'completed' || flag == 'compledAndWaiting')
            this.completed.sort((a, b) => sortCondition(a, b));
    }

    /**
     * Clear document lists
     */
    clearDocs() {
        this.completed = [];
        this.waiting = [];
        this.needSign = [];
        this.verified = [];
        this.verifying = [];
        this.needVerify = [];
    }
}
