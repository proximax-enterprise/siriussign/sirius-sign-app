import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import * as CryptoJS from 'crypto-js';

@Injectable({
    providedIn: 'root'
})
export class AccountStoreService {

    collectionName: string;

    constructor(private firestore: AngularFirestore) { }

    /**
     * Setup db according to app. MUST be called before any other methods
     * @param collectionName
     */
    setup(collectionName: string) {
        this.collectionName = collectionName;
    }

    private create(name, hashedPassword, encryptedPrivateKey, network, did, plan, signature) {
        return this.firestore.firestore.collection(this.collectionName).add({
            name: name,
            password: hashedPassword,
            key: encryptedPrivateKey,
            network: network,
            did: did,
            plan: plan,
            signature: signature
        });
    }

    private update(id, keyValue) {
        return this.firestore.firestore.collection(this.collectionName).doc(id).update(keyValue);
    }

    private read(name) {
        return this.firestore.collection(this.collectionName, ref => ref.where('name', '==', name)).get().toPromise();
    }

    addAccount(name, password, privateKey, network, did, plan, signature) {
        const encryptedPrivateKey = CryptoJS.AES.encrypt(privateKey, password).toString();
        const hashedPassword = CryptoJS.MD5(password).toString();
        return this.create(name, hashedPassword, encryptedPrivateKey, network, did, plan, signature);
    }

    async checkAccountExist(name) {
        const querrySnapshot = await this.read(name).catch(err => { throw err });
        const accounts = querrySnapshot.docs;
        if (accounts.length > 0) return true;
        else return false;
    }

    async readUser(name) {
        const querrySnapshot = await this.read(name).catch(err => { throw err });
        const accounts = querrySnapshot.docs;
        const toBeSelectedAccount = accounts.map(acc => acc.data()).filter(acc => acc.name == name);
        if (toBeSelectedAccount.length == 1) return toBeSelectedAccount[0];
        else return null;
    }

    async getAccount(name, password) {
        const toBeSelectedAccount = await this.readUser(name);
        if (!toBeSelectedAccount) return null;

        const hashedPassword = CryptoJS.MD5(password).toString();
        if (hashedPassword == toBeSelectedAccount.password) {
            const privateKey = CryptoJS.AES.decrypt(toBeSelectedAccount.key, password).toString(CryptoJS.enc.Utf8);
            const account = {
                name: name,
                password: password,
                privateKey: privateKey,
                network: toBeSelectedAccount.network,
                uDid: toBeSelectedAccount.did,
                plan: toBeSelectedAccount.plan,
                signature: toBeSelectedAccount.signature
            }
            return account;
        }
        return null;
    }

    async updateAccount(name, keyValue) {
        const querrySnapshot = await this.read(name).catch(err => { throw err });
        const accounts = querrySnapshot.docs;
        const toBeSelectedIndex = accounts.map(acc => acc.data().name).indexOf(name);
        const id = accounts[toBeSelectedIndex].id;
        return this.update(id, keyValue);
    }
}