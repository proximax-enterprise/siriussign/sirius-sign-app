import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/observable/forkJoin';
// import 'rxjs/add/operator/first';
import { forkJoin } from 'rxjs';
import { first } from 'rxjs/operators';

import {
    PlainMessage,
    TransferTransaction,
    Deadline,
    TransactionHttp,
    Account,
    PublicAccount,
    ModifyMultisigAccountTransaction,
    MultisigCosignatoryModification,
    MultisigCosignatoryModificationType,
    AggregateTransaction,
    AccountHttp,
    Address,
    QueryParams,
    HashLockTransaction,
    NetworkCurrencyMosaic,
    UInt64,
    Mosaic,
    CosignatureTransaction,
    CosignatureSignedTransaction,
    MultisigAccountInfo,
    TransactionType
} from 'tsjs-xpx-chain-sdk';

import { SiriusDocument } from './../model/sirius-document.model';

import { HelperService } from './helper.service';
import { GlobalService } from './global.service';
import { MonitorService } from './monitor.service';

@Injectable({
    providedIn: 'root'
})
export class SigningService {

    /** Document info of a selected document on any screen */
    selectedDocInfo: SiriusDocument = {
        id: '',
        name: '',
        uploadDate: new Date(),
        fileHash: {
            sha256Hash: '',
            md5Hash: ''
        },
        status: '',
        owner: '',
        isSigned: 0,
        cosigners: [],
        cosignatures: [],
        multisigAccount: null,
        documentAccount: null,
        signTxHash: '',
        uploadTxHash: '',
        isEncrypt: false
    };

    /** Document info of all documents unconfirmed sign */
    documentsInfo: SiriusDocument[] = [];

    /** Cosignature Signed Transaction to cosign the selected document */
    toBeCosignTx: CosignatureSignedTransaction;

    /** Multisig address that the logged in account is a cosigner */
    multisigAccs: string[];

    /** Transaction ID get from universal link */
    universalId: string = '';

    constructor(
        private global: GlobalService,
        private monitor: MonitorService
    ) { }

    /**
     * Set document info for viewing
     * @param id
     */
    setSelectedDocInfo(doc: SiriusDocument) {
        this.selectedDocInfo = doc;
    }

    /**
     * Set document info for viewing by ID
     * @param id 
     */
    setSelectedDocInfoById(id) {
        this.selectedDocInfo = this.getDocById(id);
    }

    /**
     * Set document info for viewing from universal link
     */
    setSelectedDocInfoFromUniversal() {
        this.selectedDocInfo = this.getDocById(this.universalId);
    }

    /**
     * Add a document
     * @param doc
     */
    addDocInfo(doc: SiriusDocument) {
        this.documentsInfo.push(doc);
    }

    /**
     * Get document by Id
     * @param id
     */
    getDocById(id) {
        let ids = this.documentsInfo.map(doc => doc.id);
        let index = ids.indexOf(id);
        if (index != -1) return this.documentsInfo[index];
        else throw new Error("No documents have the given id.")
    }

    /**
     * Create message from sign document.
     * 
     * Message format:
     * 
     * [AppIdCode/AppName],[AppVersion]|[FileHash],[FileName],[UploadTxHash],[isEncrypt]|[MetaInfo]
     */
    createMessage(document) {
        let now = new Date(Date.now());
        const isEncrypt = document.isEncrypt ? '1' : '0';
        return PlainMessage.create(
            this.global.appCodeName + ',' + this.global.appVersion + ',1|'
            + document.fileHash + ','
            + document.file.name + ','
            + document.uploadTxHash + ','
            + isEncrypt + '|'
            + now.toString()
        );
    }

    /**
     * Create message to invate joinning group
     * @param name 
     */
    createMessageJoinGroup(name) {
        let now = new Date(Date.now());
        return PlainMessage.create(
            this.global.appCodeName + ',' + this.global.appVersion + ',2|'
            + 'join_group,'
            + name + '|'
            + now.toString()
        );
    }

    /**
     * Sign document without cosignatures
     * @param document
     */
    signOnlyMe(document) {
        // Prepare info
        const senderAccount = this.global.loggedAccount;
        const recipientAddress = null; //this.global.notarizationAddress;
        // const recipientAddress = Address.createFromRawAddress('SCQALPC7AQHA6OEDCSOF45HEMLKIKFTSFISN2LVG');
        const message = this.createMessage(document);

        // Create transaction
        const notarizationTransaction = TransferTransaction.create(
            Deadline.create(),
            recipientAddress,
            [],
            message,
            this.global.networkType
        );
        console.log(notarizationTransaction);

        // Sign transaction
        const networkGenerationHash = this.global.networkGenerationHash;
        const signedNotarization = senderAccount.sign(notarizationTransaction, networkGenerationHash);  //v~0.4.0
        // const signedNotarization = senderAccount.sign(notarizationTransaction);     //v^0.2.0
        console.log(signedNotarization);

        return signedNotarization;
    }

    /**
     * Create multisig account
     * @param cosignerPublicKeys
     */
    createMultisigAccount(cosignerPublicKeys: string[], name) {
        const senderAcc = this.global.loggedAccount;

        // Generate an account
        const preMultisigAcc = Account.generateNewAccount(this.global.networkType);
        console.log('[Debug] Generated preMultisigAcc');
        console.log(preMultisigAcc);

        // Create Public Accounts from cosigner public keys
        let cosigners = cosignerPublicKeys.map(cosignerPublicKey => {
            return PublicAccount.createFromPublicKey(cosignerPublicKey, this.global.networkType);
        });
        console.log('[Debug] Generated cosigners');
        console.log(cosigners);

        // Create MultisigCosignatoryModifications arrays
        let multisigMods = cosigners.map(cosigner => {
            return new MultisigCosignatoryModification(
                MultisigCosignatoryModificationType.Add,
                cosigner,
            )
        });
        console.log('[Debug] Generated multisigMods');
        console.log(multisigMods);

        // Create a Modify Multisig Account Transaction, a Convert to Multisig transaction
        const convertIntoMultisigTransaction = ModifyMultisigAccountTransaction.create(
            Deadline.create(23),
            cosigners.length,
            cosigners.length,
            multisigMods,
            this.global.networkType
        );
        console.log('[Debug] Generated Convert Transaction');
        console.log(convertIntoMultisigTransaction);

        // Create transaction
        const cosignersWithoutMe = cosigners.filter(cosigner => cosigner.address.plain() != senderAcc.address.plain());
        const message = this.createMessageJoinGroup(name);
        const notifyTransactions = cosignersWithoutMe.map(cosigner => {
            return TransferTransaction.create(
                Deadline.create(23),
                cosigner.address,
                [],
                message,

                this.global.networkType
            );
        });
        console.log('[Debug] Generated notify transaction')
        console.log(cosignersWithoutMe);
        console.log(notifyTransactions);


        //The first block hash from this.global.apiNode/block/1 meta.generationHash value
        const networkGenerationHash = this.global.networkGenerationHash;
        // const signedTransaction = preMultisigAcc.sign(convertIntoMultisigTransaction, networkGenerationHash);   //v~0.4.0
        // const signedTransaction = preMultisigAcc.sign(convertIntoMultisigTransaction);  //v^0.2.0

        // Wrap Modify Multisig Account Transaction into the Aggregate Bonded Transaction
        const notifyInnnerAggTx = notifyTransactions.map((notifyTx, index) => {
            return notifyTx.toAggregate(cosignersWithoutMe[index])
        });
        console.log('[Debug] Created inner transaction to notify join group');
        console.log(notifyInnnerAggTx);

        const aggregateBondedTransaction = AggregateTransaction.createBonded(
            Deadline.create(23),
            [convertIntoMultisigTransaction.toAggregate(preMultisigAcc.publicAccount),
            ...notifyInnnerAggTx],
            this.global.networkType
        );
        console.log('[Debug] Created Aggregate Bonded Transaction');
        console.log(aggregateBondedTransaction);

        // Sign the Aggregate Bonded Transaction
        const signedTransaction = senderAcc.sign(aggregateBondedTransaction, networkGenerationHash);
        console.log('[Debug] Signed Aggregate Bonded Transaction');
        console.log(signedTransaction);

        // Create hash lock transaction to avoid network spamming
        const hashLockTransaction = HashLockTransaction.create(
            Deadline.create(),
            new Mosaic(
                this.global.mosaicId,
                // new NamespaceId('bffb42a19116bdf6'),
                UInt64.fromUint(10 * Math.pow(10, NetworkCurrencyMosaic.DIVISIBILITY))
            ),
            // NetworkCurrencyMosaic.createRelative(10),
            UInt64.fromUint(4800),
            signedTransaction,
            this.global.networkType
        );
        console.log('[Debug] Created Hask Lock Transaction');
        console.log(hashLockTransaction);

        // Sign hash lock transation
        const hashLockTransactionSigned = senderAcc.sign(hashLockTransaction, networkGenerationHash);   //v~0.4.0
        console.log('[Debug] Signed Hash Lock');
        console.log(hashLockTransactionSigned);

        // Listen hash lock and announce aggregate bonded transaction
        this.monitor.listenTransaction(senderAcc, hashLockTransactionSigned.hash,
            () => {
                // Announce create multisig account transaction
                console.log('[Debug] Announce hashLock');
                this.announceTransaction(hashLockTransactionSigned);
            },
            async tx => {
                const aggBonddedTxListen = await this.monitor.listenAggregateBondedTransaction(
                    senderAcc.address,
                    () => {
                        console.log('[Debug] hashLock is on network. Announce aggregateBondedTx.');
                        this.announceAggregateTransaction(signedTransaction)
                            .subscribe(res => console.log(res));
                    },
                    tx => {
                        // Sign the aggTx by to-be-multisig account
                        console.log('Get an AggTx');
                        console.log(tx);
                        const cosignTx = this.cosignMultisigTransaction(tx, preMultisigAcc);
                        this.announceAggregateBondedCosignature(cosignTx);
                        aggBonddedTxListen.unsubscribe();
                    });
            }, err => { console.log(err); });

        return { multisigAccount: preMultisigAcc, creationTxHash: signedTransaction.hash };
    }

    /**
     * Find the multisig address that only cosigners are cosignatories
     * @param cosingers
     */
    findMultisigAcc(cosingers: string[]) {
        const accountHttp = new AccountHttp(this.global.apiNode);

        let cosignersMultisigAddrs = [];
        let seletedGroup = this.multisigAccs;
        let seletedAcc: PublicAccount = null;

        // Fetch all multisig accounts of cosigners
        let task = [];
        cosingers.forEach((cosigner, index) => {
            const cosignerAddr = Address.createFromPublicKey(cosigner, this.global.networkType);
            task.push(accountHttp.getMultisigAccountInfo(cosignerAddr).pipe(first()));
        });

        return new Promise(resolve => {
            // Waiting for fetching done
            forkJoin(...task).pipe(first()).subscribe(multisigAccInfos => {
                console.log('Fetching done');
                multisigAccInfos.forEach((multisigAccInfo, index) => {
                    cosignersMultisigAddrs[index] = multisigAccInfo.multisigAccounts
                        .map(mutilsigAcc => mutilsigAcc.address.plain());
                })

                // Get the intersection of cosigners multisig accounts
                cosignersMultisigAddrs.forEach(cosignerMultisigAddrs => {
                    seletedGroup = cosignerMultisigAddrs.filter(addr => seletedGroup.includes(addr));
                })
                console.log(seletedGroup);

                if (seletedGroup.length == 0) resolve(seletedAcc);

                // Check if any account have cosignatories that include cosgners only
                task = [];
                seletedGroup.forEach(addr => {
                    const address = Address.createFromRawAddress(addr);
                    task.push(accountHttp.getMultisigAccountInfo(address).pipe(first()));
                });

                forkJoin(...task).subscribe(multisigAccInfos => {
                    console.log(multisigAccInfos);
                    multisigAccInfos.forEach((multisigAccInfo: MultisigAccountInfo) => {
                        if (multisigAccInfo.cosignatories.length == cosingers.length + 1)
                            seletedAcc = multisigAccInfo.account;
                    });

                    console.log('[Debug] Final result')
                    console.log(seletedAcc);
                    resolve(seletedAcc);
                });
            });
        });
    }

    /**
     * Sign docment with cosignatures
     * @param document
     * @async
     */
    async signWithOthers(document, multisigAcc) {
        // Find multisig account
        // let multisigPublicAcc = <PublicAccount>await this.findMultisigAcc(document.cosigners);
        // if (!multisigPublicAcc) {
        //     console.log('[Debug] No multisig account found. Create one.')
        //     // Add me to cosigners list
        //     const cosigners = [this.global.loggedAccount.publicKey, ...document.cosigners];
        //     // Create a multisig account
        //     const multisigAcc = this.createMultisigAccount(cosigners);
        //     multisigPublicAcc = multisigAcc.publicAccount;
        // }

        const multisigPublicAcc = multisigAcc;

        // Prepare info
        const myAccount = this.global.loggedAccount;
        const recipientAddress = null; // this.global.notarizationAddress;
        const message = this.createMessage(document);

        // Create transfer transaction
        const notarizationTransaction = TransferTransaction.create(
            Deadline.create(),
            recipientAddress,
            [],
            message,
            recipientAddress.networkType
        );
        console.log(notarizationTransaction.deadline.value);

        // Wrap the Transfer Transaction in an Aggregate Bonded Transaction
        const aggregateTransaction = AggregateTransaction.createBonded(
            Deadline.create(23),
            [notarizationTransaction.toAggregate(multisigPublicAcc)],
            this.global.networkType
        );
        console.log('[Debug] Generated Aggregate Bonded Transaction');
        console.log(aggregateTransaction);

        // Sign aggregate bonded transaction by logged account
        const networkGenerationHash = this.global.networkGenerationHash;
        const signedNotarization = myAccount.sign(aggregateTransaction, networkGenerationHash); //v~0.4.0
        // const signedNotarization = myAccount.sign(aggregateTransaction);    //v^0.2.0
        console.log('[Debug] signedNotarization');
        console.log(signedNotarization);

        // Create hash lock transaction to avoid network spamming
        const hashLockTransaction = HashLockTransaction.create(
            Deadline.create(),
            new Mosaic(
                this.global.mosaicId,
                // new NamespaceId('bffb42a19116bdf6'),
                UInt64.fromUint(10 * Math.pow(10, NetworkCurrencyMosaic.DIVISIBILITY))
            ),
            // NetworkCurrencyMosaic.createRelative(10),
            UInt64.fromUint(4800),
            signedNotarization,
            this.global.networkType);

        // Sign hash lock transation
        const hashLockTransactionSigned = myAccount.sign(hashLockTransaction, networkGenerationHash);   //v~0.4.0
        // const hashLockTransactionSigned = myAccount.sign(hashLockTransaction);  //v^0.2.0
        console.log('[Debug] hashLockTransactionSigned');
        console.log(hashLockTransactionSigned);

        return { signedTx: signedNotarization, hashLockTx: hashLockTransactionSigned };
    }

    /**
     * Get multisig account info wrapper
     * @param address
     */
    getMultisigAccountInfo(address: Address) {
        let addr = address;
        if (typeof address == 'string') addr = Address.createFromRawAddress(address);
        const accountHttp = new AccountHttp(this.global.apiNode);
        return accountHttp.getMultisigAccountInfo(addr);
    }

    /**
     * Announce a transaction to network
     * @param recipient
     * @param signedTx
     */
    announceTransaction(signedTx) {
        const transactionHttp = new TransactionHttp(this.global.apiNode);
        console.log('[Debug] transactionHttp announce');
        transactionHttp
            .announce(signedTx)
            .subscribe(announcedTransaction => {
                console.log('[TransactionHttp] Announced Successfully');
                console.log(signedTx.hash);
            },
                err => console.log('[TransactionHttp] Announce Error' + err),
                () => console.log('[TransactionHttp] Announce Done')
            );
    }

    /**
     * Announce Aggregate Bonded Transaction
     * @param signedTx 
     */
    announceAggregateTransaction(signedTx) {
        const transactionHttp = new TransactionHttp(this.global.apiNode);
        return transactionHttp.announceAggregateBonded(signedTx);
    }

    /**
     * Get transaction status by hash
     * @param hash
     */
    getTransactionStatus(hash: string) {
        const transactionHttp = new TransactionHttp(this.global.apiNode);
        return transactionHttp.getTransactionStatus(hash);
    }

    /**
     * Get transaction by hash
     * @param hash
     */
    getTransaction(hash: string) {
        const transactionHttp = new TransactionHttp(this.global.apiNode);
        return transactionHttp.getTransaction(hash);
    }

    /**
     * Fetch aggregate bonded transactions
     * @param lastId 
     * @param fn
     */
    async fetchAggregateBondedTransactions(account, lastId, fn?) {
        console.log('fetchAggregateBondedTransactions: ' + lastId);
        const accountHttp = new AccountHttp(this.global.apiNode);
        let querryParams = null;
        if (lastId != null) {
            querryParams = new QueryParams(10, lastId);
        }
        return new Promise((resolve, reject) => {
            accountHttp.aggregateBondedTransactions(account, querryParams)
                .subscribe(transactions => {
                    fn(transactions);
                    resolve();
                }, err => {
                    console.log(err);
                    reject();
                });
        });
    }

    /**
     * Fetch multisig account info for creating sirius document to add to multisigDocs
     * @param multisigAcc 
     * @param aggregateTransaction 
     * @param info 
     * @param fn 
     */
    private createMitisigSiriusDoc(aggregateTransaction, info, fn?) {
        const transaction = <TransferTransaction>aggregateTransaction.innerTransactions[0];
        const multisigAcc = transaction.signer;
        this.getMultisigAccountInfo(multisigAcc.address)
            .subscribe(multisigAccInfo => {
                const cosigners = multisigAccInfo.cosignatories.map(publicAcc => publicAcc.address.plain());
                const cosignatures = aggregateTransaction.cosignatures
                    .map(aggTxCosignature => aggTxCosignature.signer.address.plain());
                const isOwner = aggregateTransaction.signer.address.plain() == this.global.loggedAccount.address.plain();
                const isSignedByMe = isOwner || cosignatures.includes(this.global.loggedAccount.address.plain());

                const doc = SiriusDocument.create(
                    aggregateTransaction.transactionInfo.id,
                    info.name,
                    info.fileHash,
                    new Date(transaction.deadline.value.plusHours(-23).toString()),
                    // aggregateTransaction.isConfirmed ? 'Confirmed' : 'Waiting for cosignatures',
                    'Waiting for cosignatures',
                    aggregateTransaction.signer.address.plain(),
                    isSignedByMe ? 1 : 0,
                    cosigners,
                    cosignatures,
                    multisigAcc,
                    null,
                    aggregateTransaction.transactionInfo.hash,
                    info.uploadTxHash,
                    false
                );
                const isHistory = isSignedByMe && !isOwner;
                this.addDocInfo(doc);
                fn(doc);
            });
    }

    /**
     * Create SiriusDocument from Aggregate Transaction and add to docInfo list
     * @param aggregateTransaction
     * @param fn Callback function for multisig document after add to signing docs info
     */
    addMultisigDocInfo(aggregateTransaction: AggregateTransaction, fn?) {
        const transaction = <TransferTransaction>aggregateTransaction.innerTransactions[0];
        const message = transaction.message.payload;
        const info = HelperService.parseMessage(message);

        if (info.uploadTxHash != '') {
            if (info.appCodeName == this.global.appCodeName) {
                // Get upload transaction to check file hash
                this.getTransaction(info.uploadTxHash)
                    .subscribe((uploadTx: TransferTransaction) => {
                        const uploadTxMessage = uploadTx.message.payload;
                        const uploadInfo = JSON.parse(uploadTxMessage);
                        const fileHashFromUpload = uploadInfo.data.metadata.SFH;
                        if (fileHashFromUpload == info.fileHash) {
                            this.createMitisigSiriusDoc(aggregateTransaction, info, fn);
                        }
                    });
            }
        }
        else {  //Temporory no Upload for debug
            this.createMitisigSiriusDoc(aggregateTransaction, info, fn);
        }
    }

    /**
     * Cosign an announced aggregate transaction
     * @param transaction 
     */
    cosignMultisigTransaction(transaction, account) {
        const cosignTx = CosignatureTransaction.create(transaction);
        const cosignSignedTx = account.signCosignatureTransaction(cosignTx);
        return cosignSignedTx;
    }

    /**
     * announceAggregateBondedCosignature wrapper
     * @param cosignSignedTx 
     */
    announceAggregateBondedCosignature(cosignSignedTx) {
        const transactionHttp = new TransactionHttp(this.global.apiNode);
        return transactionHttp.announceAggregateBondedCosignature(cosignSignedTx);
    }

    /**
     * Check if an aggregate bonnded transaction is a multisig account converting tx for logged account
     * @param aggTx 
     */
    checkJoinMultisigAcc(aggTx: AggregateTransaction) {
        if (aggTx.innerTransactions.length <= 1) return false;
        if (aggTx.innerTransactions[0].type != TransactionType.MODIFY_MULTISIG_ACCOUNT) return false;
        return true;
    }
}
