import { MenuController, Platform, AlertController } from '@ionic/angular';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Device } from '@ionic-native/device/ngx';
import { Chooser, ChooserResult } from '@ionic-native/chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { IOSFilePicker } from '@ionic-native/file-picker/ngx';
import { File } from '@ionic-native/file/ngx';

import { SignDocumentService } from './../services/sign-document.service';
import { HelperService } from './../services/helper.service';
import { GlobalService } from '../services/global.service';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage {
    onChooser: boolean = false;
    isLoading: boolean = false;

    constructor(
        private menu: MenuController,
        private alertController: AlertController,
        private router: Router,
        private chooser: Chooser,
        private platform: Platform,
        private device: Device,
        private filePath: FilePath,
        private fileChooser: FileChooser,
        private filePicker: IOSFilePicker,
        private file: File,
        private signDoc: SignDocumentService,
        private global: GlobalService
    ) {
        console.log(this.platform);
        this.onChooser = this.device.platform != 'browser' && this.device.platform != null;
        //!this.platform.is('desktop') || !this.platform.is('mobileweb');
        console.log(this.onChooser);
    }

    /**
     * Close the side menu
     */
    closeMenu() {
        this.menu.close("menu");
    }

    /**
     *  Navigate to new doc signing page
     */
    onCreate() {
        if (this.global.isPayment) {
            if (this.global.loggedWallet.plan == -1) {
                this.alertMessage('Missing permission', 'Your account has no license on this device');
                return;
            }
        }
        if (this.onChooser)
            this.browseFile();
        else {
            document.getElementById('browseNew').click();
        }
    }

    /**
     * Navigate to new file view
     */
    goNewFile() {
        this.router.navigate(['new-file-view']);
    }

    /**
     * Open file chooser then return file uri, switch view to file list
     * @async
     */
    async browseFileByCommonChooser() {
        this.isLoading = true;
        const file: ChooserResult = await this.chooser.getFile('application/pdf')
            .catch((e: any) => {
                console.log(e);
                this.isLoading = false;
                return null;
            });

        if (file) {
            console.log(file);

            if (file.mediaType != 'application/pdf') {
                this.alertInvalidFileType();
                this.isLoading = false;
                return;
            }

            let fileSize = file.data.length;
            if (this.platform.is('android'))
                file.uri = await this.filePath.resolveNativePath(file.uri);
            this.signDoc.setDocument(file, fileSize);
            this.isLoading = false;
            this.goNewFile();
        }
    }

    /**
     * Open file chooser then return file uri, switch view to file list
     * @async
     */
    async browseFile() {
        this.isLoading = true;
        let fileUri: string;
        if (this.platform.is('android')) {
            fileUri = await this.fileChooser.open()
                .catch(err => {
                    console.log(err);
                    this.isLoading = false;
                    return null;
                });
        }
        else if (this.platform.is('ios')) {
            fileUri = 'file://' + await this.filePicker.pickFile()
                .catch(err => {
                    console.log(err);
                    this.isLoading = false;
                    return null;
                });
        }

        if (fileUri) {
            console.log(fileUri);
            let nativeFileUri = fileUri;
            if (this.platform.is('android'))
                nativeFileUri = await this.filePath.resolveNativePath(fileUri);
            console.log(nativeFileUri)
            const seperatorIdx = nativeFileUri.lastIndexOf('/');
            const filePath = nativeFileUri.substring(0, seperatorIdx + 1);
            const fileName = nativeFileUri.substring(seperatorIdx + 1);
            const fileDataUri = await this.file.readAsDataURL(filePath, fileName)
                .catch(err => {
                    console.log(err);
                    this.isLoading = false;
                    return null;
                });
            if (!fileDataUri) return;

            const fileType = fileDataUri.split(';')[0].split(':')[1];
            if (fileType != 'application/pdf') {
                this.alertInvalidFileType();
                this.isLoading = false;
                return;
            }

            const fileData = HelperService.convertDataURIToBinary(fileDataUri);
            const fileSize = fileData.length;
            const file: ChooserResult = {
                data: fileData,
                dataURI: fileDataUri,
                mediaType: fileType,
                name: fileName,
                uri: nativeFileUri
            }
            this.signDoc.setDocument(file, fileSize);
            this.isLoading = false;
            this.goNewFile();
        }
    }

    /**
     * Read file by browser
     * @param files 
     */
    onFile(files) {
        this.isLoading = true;
        console.log(files);
        const reader = new FileReader();
        reader.onload = (event) => {
            console.log(event.target);
            const target = <any>event.target;
            const file = {
                data: HelperService.convertDataURIToBinary(target.result),
                dataURI: target.result,
                mediaType: target.result.split(';')[0].split(':')[1],
                name: files[0].name,
                uri: ''
            }
            console.log(file);
            if (file.mediaType != 'application/pdf') {
                this.alertInvalidFileType();
                this.isLoading = false;
                return;
            }
            this.signDoc.setDocument(file, files[0].size);
            let input = <any>document.getElementById("browseNew");
            input.value = '';
            this.isLoading = false;
            this.goNewFile();
        }
        reader.readAsDataURL(files[0]);
    }

    /**
     * Alert invalid file type
     */
    async alertInvalidFileType() {
        this.alertMessage('Invalid File Type', 'SirisuSign supports .pdf file only.');
    }

    /**
     * Alert message
     */
    async alertMessage(header: string, message: string) {
        const alert = await this.alertController.create({
            header: header,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
    }
}
