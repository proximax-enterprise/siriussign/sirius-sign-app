import { TestBed, async } from '@angular/core/testing';

import { HistoryService } from './history.service';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device/ngx';

import { GlobalService } from './global.service';
import { Account, NetworkType, Transaction, LockFundsTransaction, Deadline, UInt64, Mosaic, AccountHttp, QueryParams, TransactionHttp } from 'tsjs-xpx-chain-sdk';


describe('HistoryService', () => {
    let service:HistoryService;
    let global:GlobalService
    let account:Account

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                IonicStorageModule.forRoot()
            ],
            providers: [
                Storage,
                Device
            ]
        });

        // Setup Account
        // This account has been used to create Sirius Document before.
        service = TestBed.get(HistoryService);
        global = TestBed.get(GlobalService);
        account = Account.createFromPrivateKey('EC994CC2F78603D554809FC06CC587357CDE40E6577B4B069E881ECA6D901E04', NetworkType.TEST_NET);
        global.setLoggedAccount(account);
    });

    it('should be created', () => {
        // const service: HistoryService = TestBed.get(HistoryService);
        expect(service).toBeTruthy();
    });



    /**
     * Need to setup an account with at least 1 Sirius Sign transaction.
     * Uncomment expect to see what happen
     */
    it('Fetch multisig account info for creating sirius document to add to history', async ()=>{
        
        await (()=>{
            return new Promise((resolve, reject)=>{
                service.fetchTransactions(()=>{
                    resolve(true)
                })
            })
        })();

        // Check list transaction (unsigned)
        // expect(service.docs.length).toBeGreaterThan(0);

        // Check list transaction (signed)
        // expect(service.documentInfo.length).toBeGreaterThan(0);

    });


    describe('Check if a transaction is Sirius Sign transaction', ()=> {
        it('check TRANSFER transaction', async ()=>{
            const txId = '93531EEF3767A83C5AA5C8ADDFF49D4B2B965736BD2B407A520D369EA635AF3F';
            
            const transactionHttp = new TransactionHttp(global.API_NODE_1);
            const transaction = await transactionHttp.getTransaction(txId).toPromise();
            
            // Check this transaction is real
            expect(transaction.transactionInfo.hash).toEqual(txId);

            // Check function <service.checkSiriusTransactionType()>
            expect(service.checkSiriusTransactionType(transaction)).toEqual(true);
            
        })
    })

    
});
