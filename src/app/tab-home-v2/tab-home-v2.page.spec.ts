import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabHomeV2Page } from './tab-home-v2.page';

describe('TabHomeV2Page', () => {
    let component: TabHomeV2Page;
    let fixture: ComponentFixture<TabHomeV2Page>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TabHomeV2Page],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TabHomeV2Page);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
