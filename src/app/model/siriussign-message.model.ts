import { SignaturePosition } from './../services/draw-signature.service';

export enum SiriusMessageType {
    SIGN = 1,
    SIGN_NOTIFY = 2,
    VERIFY = 3,
    VERYFY_NOTIFY = 4
}

/**
 * SiriusMessage is the model of Sirius Sign message that be contained in each Sirius Sign trsaction
 */
export abstract class SiriusSignMessage {

    header: {
        appCode: string;
        appVersion: string;
        messageType: SiriusMessageType;
    }
    abstract body: Object;
    meta: Object;

    constructor(appCode: string, appVersion: string, messageType: SiriusMessageType, meta?: Object) {
        this.header = {
            appCode: appCode,
            appVersion: appVersion,
            messageType: messageType
        }
        this.meta = meta ? meta : { timestamp: Date.now() };
    }

    toMessage() {
        const header = this.header.appCode + ',' + this.header.appVersion + ',' + this.header.messageType;
        const body = JSON.stringify(this.body);
        const meta = JSON.stringify(this.meta);
        return header + '|' + body + '|' + meta;
    }
}

export class SsDocumentSigningMessage extends SiriusSignMessage {
    body: {
        fileHash: string,
        fileName: string,
        isOwner: boolean,
        uploadTxHash: string,
        isEncrypt: boolean,
        signatureUploadTxHash: string,
        signaturePositions: SignaturePosition[],
    }

    constructor(
        appCode: string,
        appVerion: string,
        fileHash: string,
        fileName: string,
        isOwner: boolean,
        uploadTxHash: string,
        isEncrypt: boolean,
        signatureUploadTxHash: string,
        // signatureX: number,
        // signatureY: number,
        // signaturePage: number,
        // signatureName: string,
        // namePosition: NamePosition,
        signaturePositions: SignaturePosition[],
        meta?: Object
    ) {
        // if (!(appCode && appVerion && fileHash && fileName && uploadTxHash &&
        //     !isNaN(signatureX) && !isNaN(signatureY) && (signaturePage >= 0) && signatureName && [0, 1, 2, 3, 4].includes(namePosition))) throw new Error('Undefined parameters');
        super(appCode, appVerion, SiriusMessageType.SIGN, meta);
        this.body = {
            fileHash: fileHash,
            fileName: fileName,
            isOwner: isOwner,
            uploadTxHash: uploadTxHash,
            isEncrypt: isEncrypt,
            signatureUploadTxHash: signatureUploadTxHash,
            signaturePositions: signaturePositions
            // {
            //     page: signaturePage,
            //     x: signatureX,
            //     y: signatureY,
            //     name: signatureName,
            //     namePosition: namePosition
            // }
        }
    }

    static createFromString(appCode: string, appVersion: string, bodyString: string, metaString: string) {
        try {
            const body = JSON.parse(bodyString);
            const meta = JSON.parse(metaString);
            const fileHash = body.fileHash;
            const fileName = body.fileName;
            const isOwner = body.isOwner;
            const uploadTxHash = body.uploadTxHash;
            const isEncrypt = body.isEncrypt;
            const signatureUploadTxHash = body.signatureUploadTxHash;
            // const signatureX = body.signaturePosition.x;
            // const signatureY = body.signaturePosition.y;
            // const signaturePage = body.signaturePosition.page;
            // const signatureName = body.signaturePosition.name;
            // const namePosition = body.signaturePosition.namePosition;
            const signaturePositions = body.signaturePositions;
            return new SsDocumentSigningMessage(
                appCode,
                appVersion,
                fileHash,
                fileName,
                isOwner,
                uploadTxHash,
                isEncrypt,
                signatureUploadTxHash,
                // signatureX,
                // signatureY,
                // signaturePage,
                // signatureName,
                // namePosition,
                signaturePositions,
                meta
            );
        }
        catch (err) {
            throw err;
        }
    }
}

export class SsNotifyDocumentCosignMessage extends SiriusSignMessage {
    body: {
        uploadTxHash: string,
        signaturePositions: SignaturePosition[];
    }

    constructor(
        appCode: string,
        appVerion: string,
        uploadTxHash: string,
        // signatureX: number,
        // signatureY: number,
        // signaturePage: number,
        // signatureName: string,
        // namePosition: NamePosition,
        signaturePositions: SignaturePosition[],
        meta?: Object
    ) {
        // if (!(appCode && appVerion && uploadTxHash && !isNaN(signatureX) && !isNaN(signatureY) && signaturePage && signatureName && [0, 1, 2, 3, 4].includes(namePosition)))
        //     throw new Error('Undefined parameters');
        super(appCode, appVerion, SiriusMessageType.SIGN_NOTIFY, meta);
        this.body = {
            uploadTxHash: uploadTxHash,
            signaturePositions: signaturePositions
            // {
            //     name: signatureName,
            //     page: signaturePage,
            //     x: signatureX,
            //     y: signatureY,
            //     namePosition: namePosition
            // }
        }
    }

    static createFromString(appCode: string, appVersion: string, bodyString: string, metaString: string) {
        try {
            const body = JSON.parse(bodyString);
            const meta = JSON.parse(metaString);
            const uploadTxHash = body.uploadTxHash;
            // const signatureX = body.signaturePosition.x;
            // const signatureY = body.signaturePosition.y;
            // const signaturePage = body.signaturePosition.page;
            // const signatureName = body.signaturePosition.name;
            // const namePosition = body.signaturePosition.namePosition;
            const signaturePositions = body.signaturePositions
            const metaObj = meta;
            return new SsNotifyDocumentCosignMessage(
                appCode,
                appVersion,
                uploadTxHash,
                // signatureX,
                // signatureY,
                // signaturePage,
                // signatureName,
                // namePosition,
                signaturePositions,
                metaObj
            );
        }
        catch (err) { throw err; }
    }
}

/////////////////////////////////////////////////////////////////////

export class SsDocumentVerifyMessage extends SiriusSignMessage {
    body: {
        fileHash: string,
        fileName: string,
        isOwner: boolean,
        uploadTxHash: string,
        isEncrypt: boolean
    }

    constructor(
        appCode: string,
        appVerion: string,
        fileHash: string,
        fileName: string,
        isOwner: boolean,
        uploadTxHash: string,
        isEncrypt: boolean,
        meta?: Object
    ) {
        super(appCode, appVerion, SiriusMessageType.VERIFY, meta);
        this.body = {
            fileHash: fileHash,
            fileName: fileName,
            isOwner: isOwner,
            uploadTxHash: uploadTxHash,
            isEncrypt: isEncrypt,
        };
    }

    static createFromString(appCode: string, appVersion: string, bodyString: string, metaString: string) {
        try {
            const body = JSON.parse(bodyString);
            const meta = JSON.parse(metaString);
            const fileHash = body.fileHash;
            const fileName = body.fileName;
            const isOwner = body.isOwner;
            const uploadTxHash = body.uploadTxHash;
            const isEncrypt = body.isEncrypt;

            return new SsDocumentVerifyMessage(
                appCode,
                appVersion,
                fileHash,
                fileName,
                isOwner,
                uploadTxHash,
                isEncrypt,
                meta
            );
        } catch (err) {
            throw err;
        }
    }
}

export class SsNotifyDocumentVerifyMessage extends SiriusSignMessage {
    body: {
        uploadTxHash: string,
    };

    constructor(
        appCode: string,
        appVerion: string,
        uploadTxHash: string,
        meta?: Object
    ) {
        super(appCode, appVerion, SiriusMessageType.VERYFY_NOTIFY, meta);
        this.body = {
            uploadTxHash: uploadTxHash,
        };
    }

    static createFromString(appCode: string, appVersion: string, bodyString: string, metaString: string) {
        try {
            const body = JSON.parse(bodyString);
            const meta = JSON.parse(metaString);
            const uploadTxHash = body.uploadTxHash;
            const metaObj = meta;
            return new SsNotifyDocumentVerifyMessage(
                appCode,
                appVersion,
                uploadTxHash,
                metaObj
            );
        }
        catch (err) { throw err; }
    }
}
