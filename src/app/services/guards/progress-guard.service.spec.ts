import { TestBed } from '@angular/core/testing';

import { ProgressGuardService } from './progress-guard.service';
import { IonicStorageModule } from '@ionic/storage';
import { GlobalService } from '../global.service';
import { Device } from '@ionic-native/device/ngx';

describe('ProgressGuardService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            IonicStorageModule.forRoot()
        ],
        providers: [
            Storage,
            GlobalService,
            Device
        ]
    }));

    it('should be created', () => {
        const service: ProgressGuardService = TestBed.get(ProgressGuardService);
        expect(service).toBeTruthy();
    });
});
