import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DrawSignatureService } from './../services/draw-signature.service';

@Component({
    selector: 'app-select-signer',
    templateUrl: './select-signer.page.html',
    styleUrls: ['./select-signer.page.scss'],
})
export class SelectSignerPage implements OnInit {

    signers: {
        publicKey: string,
        name: string
    }[];
    selectedIndex: number = 0;

    constructor(
        private router: Router,
        private drawSignature: DrawSignatureService
    ) {
        this.signers = this.drawSignature.signers.map(signer => { return { publicKey: signer.publicKey, name: signer.signaturePositions[0].name } });
    }

    ngOnInit() {
        this.selectedIndex = this.drawSignature.selectedSignerIndex;
    }

    onChoose(index: number) {
        this.selectedIndex = index;
        this.drawSignature.selectedSignerIndex = this.selectedIndex;
        this.router.navigateByUrl('new-interactive');
    }
}
