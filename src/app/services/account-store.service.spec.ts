import { TestBed } from '@angular/core/testing';

import { AccountStoreService } from './account-store.service';

fdescribe('AccountStoreService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: AccountStoreService = TestBed.get(AccountStoreService);
        expect(service).toBeTruthy();
    });

    it('add and get account', async () => {
        const service: AccountStoreService = TestBed.get(AccountStoreService);
        const docRef = await service.addAccount('name0', 'pass0', 'key0', 176, 'did0', 0, 'signature0');
        console.log(docRef);
        const docChangeAction = await service.getAccount('name0');
        console.log(docChangeAction);

        expect(0).toBe(0);
    });
});
