import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { LoadingOptions } from '@ionic/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingProcessService {

  constructor(
    private loadingController: LoadingController
  ) { }
  async present() {
    // Dismiss all pending loaders before creating the new one
    await this.dismiss();

    let options: LoadingOptions = {
      message:
      `<img src="assets/loading.gif" />`,
      cssClass: 'custom-loading',
      // translucent: true,
      // showBackdrop: true,
      spinner: null,
      mode: 'md',
      keyboardClose: true
    };

    await this.loadingController
      .create(options)
      .then(res => {
        res.present();
      });
  }

  async dismiss() {
    while (await this.loadingController.getTop() !== undefined) {
      await this.loadingController.dismiss();
    }
  }
}
