import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/guards/auth-guard.service';
import { ProgressGuardService, ProgressGuardCanDeactiveService, NewDoneGuardService } from './services/guards/progress-guard.service';

const routes: Routes = [
    { path: '', redirectTo: 'sign-up', pathMatch: 'full' },
    {
        path: 'app', loadChildren: './tabs/tabs.module#TabsPageModule'
    },
    {
        path: 'sign-in', loadChildren: './sign-in/sign-in.module#SignInPageModule',
        canActivate: [AuthGuardService]
    },
    {
        path: 'sign-up', loadChildren: './sign-up/sign-up.module#SignUpPageModule',
        canActivate: [AuthGuardService]
    },
    {
        path: 'code-verify', loadChildren: './code-verify/code-verify.module#CodeVerifyPageModule',
        canActivate: [AuthGuardService]
    },
    //{ path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
    //{ path: 'need-sign', loadChildren: './need-sign/need-sign.module#NeedSignPageModule' },
    //{ path: 'wait-others', loadChildren: './wait-others/wait-others.module#WaitOthersPageModule' },
    { path: 'new-file-view', loadChildren: './new-file-view/new-file-view.module#NewFileViewPageModule' },
    { path: 'new-only-me', loadChildren: './new-only-me/new-only-me.module#NewOnlyMePageModule' },
    { path: 'new-with-others', loadChildren: './new-with-others/new-with-others.module#NewWithOthersPageModule' },
    {
        path: 'new-progress', loadChildren: './new-progress/new-progress.module#NewProgressPageModule',
        canActivate: [ProgressGuardService],
        canDeactivate: [ProgressGuardCanDeactiveService]
    },
    {
        path: 'new-done', 
        loadChildren: './new-done/new-done.module#NewDonePageModule',
        canActivate: [NewDoneGuardService]
    },
    { path: 'verify-file-view', loadChildren: './verify-file-view/verify-file-view.module#VerifyFileViewPageModule' },
    {
        path: 'verify-progress', loadChildren: './verify-progress/verify-progress.module#VerifyProgressPageModule',
        canActivate: [ProgressGuardService],
        canDeactivate: [ProgressGuardCanDeactiveService]
    },
    { path: 'verify-done', loadChildren: './verify-done/verify-done.module#VerifyDonePageModule' },
    { path: 'sign-info', loadChildren: './sign-info/sign-info.module#SignInfoPageModule' },
    {
        path: 'cosign-progress', loadChildren: './cosign-progress/cosign-progress.module#CosignProgressPageModule',
        canActivate: [ProgressGuardService],
        canDeactivate: [ProgressGuardCanDeactiveService]
    },
    { path: 'cosign-done', loadChildren: './cosign-done/cosign-done.module#CosignDonePageModule' },
    { path: 'group-info', loadChildren: './group-info/group-info.module#GroupInfoPageModule' },
    { path: 'help', loadChildren: './help/help.module#HelpPageModule' },
    { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
    { path: 'privacy', loadChildren: './privacy/privacy.module#PrivacyPageModule' },
    { path: 'backup', loadChildren: './backup/backup.module#BackupPageModule' },
    { path: 'recovery', loadChildren: './recovery/recovery.module#RecoveryPageModule' },
    { path: 'signature', loadChildren: './signature/signature.module#SignaturePageModule' },
    { path: 'new-interactive', loadChildren: './new-interactive/new-interactive.module#NewInteractivePageModule' },
    { path: 'select-signer', loadChildren: './select-signer/select-signer.module#SelectSignerPageModule' },
    { path: 'new-multi-sign', loadChildren: './new-multi-sign/new-multi-sign.module#NewMultiSignPageModule' },
    { path: 'tab-home-v2', loadChildren: './tab-home-v2/tab-home-v2.module#TabHomeV2PageModule' },
    { path: 'cosign-interactive', loadChildren: './cosign-interactive/cosign-interactive.module#CosignInteractivePageModule' },
    { path: 'share-info', loadChildren: './share-info/share-info.module#ShareInfoPageModule' },
    { path: 'signed-doc-view', loadChildren: './signed-doc-view/signed-doc-view.module#SignedDocViewPageModule' },
    { path: 'tab-tasks', loadChildren: './tab-tasks/tab-tasks.module#TabTasksPageModule' },
  { path: 'upgrade-choose', loadChildren: './upgrade-choose/upgrade-choose.module#UpgradeChoosePageModule' },
  { path: 'card-info', loadChildren: './card-info/card-info.module#CardInfoPageModule' },
  { path: 'transaction-success', loadChildren: './transaction-success/transaction-success.module#TransactionSuccessPageModule' },
  { path: 'transaction-fail', loadChildren: './transaction-fail/transaction-fail.module#TransactionFailPageModule' },
  { path: 'upgrade-alert', loadChildren: './upgrade-alert/upgrade-alert.module#UpgradeAlertPageModule' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})

export class AppRoutingModule { }
