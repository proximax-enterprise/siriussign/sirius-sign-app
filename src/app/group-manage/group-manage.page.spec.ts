import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupManagePage } from './group-manage.page';

xdescribe('GroupManagePage', () => {
  let component: GroupManagePage;
  let fixture: ComponentFixture<GroupManagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupManagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupManagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
