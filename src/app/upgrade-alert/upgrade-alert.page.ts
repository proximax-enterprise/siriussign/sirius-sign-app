import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upgrade-alert',
  templateUrl: './upgrade-alert.page.html',
  styleUrls: ['./upgrade-alert.page.scss'],
})
export class UpgradeAlertPage implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  navigate() {
    this.router.navigateByUrl('upgrade-choose');
  }

}
