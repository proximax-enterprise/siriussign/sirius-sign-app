import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { Chooser } from '@ionic-native/chooser/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { GlobalService } from './../services/global.service';
import { HelperService } from './../services/helper.service';
import { BackupService, BackupInfo } from './../services/backup.service';
import { WalletService } from './../services/wallet.service';
// import { GroupService } from './../services/group.service';
// import { UploadStorageService } from './../services/upload-storage.service';
import { SignatureService } from './../services/signature.service';
import { DocumentStorageService } from './../services/document-strorage.service';

@Component({
    selector: 'app-recovery',
    templateUrl: './recovery.page.html',
    styleUrls: ['./recovery.page.scss'],
})
export class RecoveryPage implements OnInit {

    onChooser: boolean = false;
    backupInfo: BackupInfo;
    isSelected: boolean = false;

    showForm: boolean = false;
    errorMess: string = '';
    inputDat = {
        inputPass: '',
        verifPass: ''
    }

    isProcessing: boolean = false;

    constructor(
        private router: Router,
        private alertController: AlertController,
        private chooser: Chooser,
        private barcodeScanner: BarcodeScanner,
        private global: GlobalService,
        private backup: BackupService,
        private wallet: WalletService,
        // private group: GroupService,
        // private uploadStorage: UploadStorageService,
        private signature: SignatureService,
        private documentStorage: DocumentStorageService
    ) {
        this.onChooser = !this.global.isBrowser;
    }

    ngOnInit() {
        this.isProcessing = false;
    }

    /**
     * Navigate to sign-in page
     */
    goSignIn() {
        this.router.navigate(['sign-in']);
    }


    /**
     * Navigate to sign-up page
     */
    goSignUp() {
        this.router.navigate(['sign-up']);
    }

    /**
     * Open file chooser then return file uri, switch view to file list
     * @async
     */
    async browseFile() {
        await this.chooser.getFile('*')
            .then(file => {
                if (file) {
                    const base64Data = file.dataURI.split(';')[1].split(',')[1];
                    const backupString = HelperService.base64toUtf8(base64Data);
                    this.retriveBackupInfo(backupString);
                }
            })
            .catch((e: any) => console.log(e))
    }

    /**
     * Read file by browser
     * @param files 
     */
    onFile(files) {
        console.log(files);
        const reader = new FileReader();
        reader.onload = (event) => {
            console.log(event.target);
            const target = <any>event.target;
            const file = {
                data: HelperService.convertDataURIToBinary(target.result),
                dataURI: target.result,
                mediaType: target.result.split(';')[0].split(':')[1],
                name: files[0].name,
                uri: ''
            }
            const base64Data = file.dataURI.split(';')[1].split(',')[1];
            const backupString = HelperService.base64toUtf8(base64Data);
            this.retriveBackupInfo(backupString);

            let input = <any>document.getElementById("browseBackup");
            input.value = '';

        }
        reader.readAsDataURL(files[0]);
    }

    /**
     * Get backup info from backup string
     * @param compressedBackupString
     */
    async retriveBackupInfo(compressedBackupString: string) {
        try {
            this.backupInfo = await this.backup.revert(compressedBackupString);
            this.isSelected = true;
        }
        catch (err) {
            this.alertMessage('Invalid Backup');
        }
    }

    async onNext() {
        //Check if there is a wallet created with the email
        let isExist = await this.wallet.checkWalletExist(this.backupInfo.name);
        if (isExist) {
            // this.errorMess = 'This email is registered on this device.';
            this.alertOverride();
            return;
        }

        this.isSelected = false;
        this.showForm = true;
    }

    /**
     * Validate password input
     * @returns {boolean}
     */
    checkPass() {
        let passLen = this.inputDat.inputPass.length;
        if ((passLen < 8) && (passLen > 0)) {
            this.errorMess = 'Password must be at least 8 charaters.';
            return false;
        }
        else {
            this.errorMess = '';
        }
        let isMatch = (this.inputDat.verifPass === this.inputDat.inputPass) || (this.inputDat.verifPass === '');
        if (isMatch) {
            this.errorMess = '';
            return true;
        }
        else {
            this.errorMess = 'Your password and confirm password do not match.'
            return false;
        }
    }

    /**
     * Create wallet and goto sign in
     */
    async createWallet() {
        await this.wallet.createWalletFromPrivateKey();
        // await this.wallet.createNotarizationWalletFromPrivateKey();
        this.wallet.clearInfo();
        this.global.setIsSignedUp(true);
    }

    async onRecover() {
        this.isProcessing = true;

        //Validate info
        let isNoInputPass = this.inputDat.inputPass == '';
        let isNoVerifPass = this.inputDat.verifPass == '';

        if (isNoInputPass) {
            this.errorMess = 'Password is required.';
            this.isProcessing = false;
            return;
        }

        if (isNoVerifPass) {
            this.errorMess = 'Confirm password is required.';
            this.isProcessing = false;
            return;
        }

        if (!this.checkPass()) {
            this.isProcessing = false;
            return;
        }

        // Validated, temporarily store info
        this.wallet.setInfo(
            this.backupInfo.name,
            this.inputDat.inputPass,
            this.backupInfo.plan,
            this.backupInfo.privateKey,
            // this.backupInfo.notaPrivateKey
        );

        // Restore groups
        // if (this.backupInfo.groupsShortInfo.length != 0) {
        //     console.log(this.backupInfo);
        //     // await this.group.restore(this.backupInfo.name, this.backupInfo.groups);
        //     const pubAcc = Account.createFromPrivateKey(this.backupInfo.privateKey, this.global.networkType).publicAccount;
        //     await this.group.fetchAndRestore(this.backupInfo.name, pubAcc, this.backupInfo.groupsShortInfo)
        //         .catch(err => {
        //             this.alertMessage('Failed to backup Groups', err.message);
        //             this.isProcessing = false;
        //         });
        // }

        // Restore signature
        // if (this.backupInfo.signatureUploadTx != '') {
        // const signatureUploadTxHash = this.backupInfo.signatureUploadTx;
        // const downloadResult: DownloadResult = await this.uploadStorage.downloadFile(signatureUploadTxHash)
        //     .catch(err => {
        //         console.log(err);
        //         this.alertMessage('Failed to backup signature', err.message);
        //         this.isProcessing = false;
        //         return null;
        //     });

        // const buffer: Buffer = await downloadResult.data.getContentAsBuffer()
        //     .catch(err => {
        //         console.log(err);
        //         this.alertMessage('Failed to backup signature', err.message)
        //         this.isProcessing = false;
        //         return null;
        //     });
        // const signatureImg = 'data:image/png;base64,' + buffer.toString('base64');
        const signatureImg = this.backupInfo.signatureImg;
        const signatureUploadTxHash = '';
        await this.signature.storeSignature(this.backupInfo.name, signatureImg, signatureUploadTxHash)
            .catch(err => {
                this.signature.override(this.backupInfo.name, '', signatureImg, signatureUploadTxHash);
            })
            .catch(err => {
                console.log(err);
                this.alertMessage('Failed to backup signature', err.message);
                this.isProcessing = false;
                return;
            });
        // }

        // Create wallets
        await this.createWallet();

        // Delete stored documents if this is an account replacing
        this.documentStorage.deleteDocuments(this.backupInfo.name);

        // Clear password
        this.inputDat.inputPass = '';
        this.inputDat.verifPass = '';

        this.isProcessing = false;

        // Navigate to OTP verify page
        this.goSignIn();
    }

    /**
     * Open barcode sacnner to scan backup qrcode
     */
    scanQRCode() {
        this.barcodeScanner.scan().then(barcodeData => {
            this.retriveBackupInfo(barcodeData.text);
        }).catch(err => {
            console.log('Error', err);
        });
    }

    /**
     * Present Alert
     * @param message 
     */
    async alertMessage(header: string, message: string = '') {
        const alert = await this.alertController.create({
            header: header,
            message: message,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });

        await alert.present();
    }

    /**
     * Present Alert
     * @param message 
     */
    async alertOverride() {
        const alert = await this.alertController.create({
            header: 'Email existed!',
            subHeader: 'Override account',
            message: 'There is an existing account with this email on this device. Do you want to override it by the backup account?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel'
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.isSelected = false;
                        this.showForm = true;
                    }
                }
            ]
        });

        await alert.present();
    }
}
