import * as tslib_1 from "tslib";
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabVerifyPage } from './tab-verify.page';
var TabVerifyPageModule = /** @class */ (function () {
    function TabVerifyPageModule() {
    }
    TabVerifyPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                IonicModule,
                CommonModule,
                FormsModule,
                RouterModule.forChild([{ path: '', component: TabVerifyPage }])
            ],
            declarations: [TabVerifyPage]
        })
    ], TabVerifyPageModule);
    return TabVerifyPageModule;
}());
export { TabVerifyPageModule };
//# sourceMappingURL=tab-verify.module.js.map