import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../services/global.service';

@Component({
    selector: 'app-about',
    templateUrl: './about.page.html',
    styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
    version: string;
    build: string;
    
    constructor(private global: GlobalService) { 
        this.version = this.global.appVersion;
        this.build = this.global.build;
    }

    ngOnInit() {
    }

}
