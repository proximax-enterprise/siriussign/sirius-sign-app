import { Component, OnInit } from '@angular/core';
import { HelperService } from '../services/helper.service';
import { Router } from '@angular/router';

import { GlobalService } from './../services/global.service';
import { DocumentSigningStatus, DocumentClassificationService } from '../services/document-classification.service';

@Component({
    selector: 'app-wait-others',
    templateUrl: './wait-others.page.html',
    styleUrls: ['./wait-others.page.scss'],
})

export class WaitOthersPage implements OnInit {

    ownerPublicKey: string;

    constructor(
        private router: Router,
        private global: GlobalService,
        public documentClassifcation: DocumentClassificationService
    ) {
        this.ownerPublicKey = this.global.loggedAccount.publicKey;
    }

    ngOnInit() {
    }

    dateToShortString = HelperService.dateToShortString;

    /**
     * View document info
     */
    onDocument(index: number) {
        this.documentClassifcation.selectedDocInfo = this.documentClassifcation.getDocByIndex(index, DocumentSigningStatus.WAITING);
        this.router.navigate(['sign-info']);
    }
}
