import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionSuccessPage } from './transaction-success.page';

describe('TransactionSuccessPage', () => {
  let component: TransactionSuccessPage;
  let fixture: ComponentFixture<TransactionSuccessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionSuccessPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionSuccessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
