import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
    selector: 'app-common-header',
    templateUrl: './common-header.component.html',
    styleUrls: ['./common-header.component.scss'],
})

export class CommonHeaderComponent implements OnInit{

    @Input() name: string = 'Page Name';
    @Input() btnBackLink: string = '/app';
    @Input() btnForwardLink: string = '';
    @Input() forwardText: string = '';
    @Output() forwardFunction: EventEmitter<any> = new EventEmitter();

    isIcon: boolean;
    ionIcon: string;

    constructor() {
        this.isIcon = (this.forwardText.substr(0, 3) == 'ion');
    }

    ngOnInit() {
        this.isIcon = (this.forwardText.substr(0, 3) == 'ion');
        if (this.isIcon) this.ionIcon = this.forwardText.substr(4);
    }

    /**
     * Function to call forward function when forward button is clicked
     */
    onForward() {
        this.forwardFunction.emit('');
    }
}
